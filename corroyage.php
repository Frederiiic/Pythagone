<!DOCTYPE html>
<html>
	<head>
        <meta charset="utf-8" />
        <title>Pythagone : Angles de corroyages</title>
        <meta name="viewport" content="width=800, user-scalable=no" />
        <meta name="description" content="Arêtier / Corroyage : Calcule automatique des angles de corroyage sur arêtier ou noue. Ce calculateur donne le réglage machine pour les usinages le long de d'un arêtier en fonction de l'angle sur sablière et des deux pentes, par Frederic Pavageau." />
        <meta name="keywords" content="frédéric Pavageau, Angle de corroyage, Corroyage, Toiture, charpente, Menuiserie, Arêtier, Noue, Angle, Angle de coupe, Pente, Coupe, Construction, Construire, Pythagore, Euclide, Trigonométrie, Cosinus, Sinus, Tangeante, plan, calcule, géometrie, tracé, Trait de charpente, Trait d'ébéniste" />
        <meta property="og:site_name" content="Pythagone" /> 
        <meta property="og:title" content="Pythagone : Arêtier / Corroyage" />
        <meta property="og:type" content="website" /> 
        <meta property="og:url" content="https://www.pythagone.net/pyramide.php" />
        <meta property="og:description" content="Calcule automatique des angles de corroyage sur arêtier ou noue. Ce calculateur donne le réglage machine pour les usinages le long de d'un arêtier en fonction de l'angle sur sablière et des deux pentes, par Frederic Pavageau." />
        <meta property="og:image" content="https://pythagone.fredericpavageau.net/img/Pythagone.svg" />
        <meta name="twitter:card" content="summary_large_image" />
        <link rel="stylesheet" href="style.css" />
		<link rel="stylesheet" href="corroyage.css" />
		<link rel="icon" type="image/svg+xml" href="img/Pythagone.svg" sizes="any"/>
		<link rel="icon" type="image/png" href="img/16-flavico.png" sizes="16x16"/>
		<link rel="icon" type="image/png" href="img/32-flavico.png" sizes="32x32"/>
		<link rel="icon" type="image/png" href="img/64-flavico.png" sizes="64x64"/>
		<link rel="icon" type="image/png" href="img/96-flavico.png" sizes="96x96"/>
		<link rel="icon" type="image/png" href="img/256-flavico.png" sizes="256x256"/>
	</head>

	<body>

		<h1>Pythagone : Angles de corroyages</h1>
		<h2>Arêtier / Corroyage : Calcule automatique des angles de corroyage sur arêtier ou noue. Ce calculateur donne le réglage machine pour les usinages le long de d'un arêtier en fonction de l'angle sur sablière et des deux pentes, par Frederic Pavageau.</h2>

		<div id="backdrop"></div>

		<div id="sidenav">
			<?php require "menu.php"; ?>
			<script>
				mdf = document.getElementById("corroyage");
				mdf.className = "active";
			</script>
		</div>
	
		<div id="content">
		
			<header>
				<div id="menu-toggle">
					<img id="menu" src="img/Menu.svg" alt="Bouton d'ouverture du menu" />
				</div>
			</header>
			
			<svg id="fond" viewBox="0 0 200 100" xml:lang="fr"
			xmlns="http://www.w3.org/2000/svg"
			xmlns:xlink="http://www.w3.org/1999/xlink">
				<title>Corroyage Double Pente</title>
				<circle id="PointOrtho" stroke="transparent" fill="#036" r="0.5"/>
				<polygon id="Sabliere" class="Grid" fill="transparent" stroke="#ddd" stroke-width="0.3" points=""/>
				<polyline id="Rem1" class="FaceCache" fill="transparent" stroke="#69c" stroke-width="0.3" stroke-dasharray="1, 1" points=""/>
				<polygon id="CoupeAlpha" class="CoupeAlpha" fill="rgba(68, 191, 68, 0.5)" stroke="#4b4" stroke-width="0.3" opacity="0" points=""/>
				<polygon id="CoupeBeta" class="CoupeBeta" fill="rgba(68, 191, 68, 0.5)" stroke="#4b4" stroke-width="0.3" opacity="0" points=""/>
				<polygon id="CoupeBetaRond" class="CoupeBeta" fill="#4b4" stroke="#4b4" stroke-width="0.3" opacity="0" points=""/>
				<polygon id="CoupeGamma" class="CoupeGamma" fill="rgba(68, 191, 68, 0.5)" stroke="#4b4" stroke-width="0.3"  opacity="0" points=""/>
				<polygon id="CoupeGammaRond" class="CoupeGamma" fill="#4b4" stroke="#4b4" stroke-width="0.3" opacity="0" points=""/>
				<polyline id="Rem2" class="Face" fill="transparent" stroke="#036" stroke-width="0.5" points=""/>
				<polyline id="Rem3" class="Face" fill="transparent" stroke="#036" stroke-width="0.5" points=""/>
				<polyline id="Rem4" class="Face" fill="transparent" stroke="#036" stroke-width="0.5" points=""/>
				<polyline id="SabliereDotted" class="FaceCache" fill="transparent" stroke="#69c" stroke-width="0.3" stroke-dasharray="1, 1" opacity="0" points=""/>
				<polyline id="SabliereDessin" class="Face" fill="transparent" stroke="#036" stroke-width="0.5" opacity="0" points=""/>
				<polygon id="FaceG" class="Face" fill="transparent" stroke="#036" stroke-width="0.5" opacity="0" points=""/>
				<polygon id="FaceD" class="Face" fill="transparent" stroke="#036" stroke-width="0.5" opacity="0" points=""/>
				<polygon id="FaceDDotted" class="FaceCache" fill="transparent" stroke="#69c" stroke-width="0.3" stroke-dasharray="1, 1" opacity="0" points=""/>
			</svg>
		
			
			<form id="Form">
				<span id="spanfAlpha" class="cont">Angle de la sabliere &#945; : <input name="fAlpha" class="incorrect" onfocus="Show('CoupeAlpha')" onblur="unShow('CoupeAlpha')" id="fAlpha" type="number"/>
				</span><br/>
				<span id="spanfBeta" class="cont">Angle de la pente &#946; : <input name="fBeta" class="incorrect" onfocus="Show('CoupeBeta')" onblur="unShow('CoupeBeta')" id="fBeta" type="number"/><br/>
				</span>
				<span id="spanfGamma" class="cont">Angle de la pente &#947; : <input name="fGamma" class="incorrect" onfocus="Show('CoupeGamma')" onblur="unShow('CoupeGamma')" id="fGamma" type="number"/><br/>
				</span>
				<span id="spanCor" class="cont">Bissectrice sur arêtier : </span>
				<input name="rCor" id="rCor" type="number" disabled="disabled"/><br/>
				<span id="spanAADBeta" class="cont">Face d'aplomb délardé &#946; : </span>
				<input name="rArApDelBeta" id="rArApDelBeta" type="number" disabled="disabled"/><br/>
				<span id="spanAADGamma" class="cont">Face d'aplomb délardé &#947; : </span>
				<input name="rArApDelGamma" id="rArApDelGamma" type="number" disabled="disabled"/><br/>
				<span id="spanreset"><input id="reset" type="button" onmousedown="EmptyAll()" value="Reinitialiser" /></span>
				
			</form>
		
		</div>
		
		<script src="sidenav.min.js"></script>
		
		<script src="Menu.js"></script>

		<script>

			// Fonction arrondis
			function arrondir(Val) {
				result = Math.round(Val * 100) / 100;

				return result;
			}

			// Fonction de calcule des cotes adjascents par tangeante.
			function CalcAdjTan(Angle, Opp) {
				var Angle = Math.radians(Number(Angle)),
					Adj = 0;

				Adj = Opp / Math.tan(Angle);

				return Adj;
			}

			// Fonction de calcule du cote oppose si adj = 1
			function CalcOppTan(Angle, Adj) {
				var Angle = Math.radians(Number(Angle)),
					Opp = 0;

				Opp = Adj * Math.tan(Angle);

				return Opp;
			}

			// Fonction de calcule de cote adjacten par cos
			function CalcAdjCos(Angle,Hyp) {
				var Angle = Math.radians(Number(Angle)),
					Adj = 0;

				Adj = Math.cos(Angle) * Hyp;

				return Adj;
			}

			// Fonction de calcule du cote oppose avec la fonction Sinus.
			function CalcOppSin(Angle, Hyp) {
				var Angle = Math.radians(Number(Angle)),
					Opp = 0;

				Opp = Hyp * Math.sin(Angle);

				return Opp;
			}

			// Fonction de calcule de l'hypothenuse avec pythagore.
			function CalcPythyp(Cote1,Cote2) {
				Hyp = Math.sqrt(Math.pow(Cote1,2) + Math.pow(Cote2,2));

				return Hyp;
			}

			// Fonction pour Determiner l'angle oppose a Gamma (si alpha est > 90)
			function CalcAngleOpp(Angle) {
				var Angle = Number(Angle),
					AngleOpp = 0;

				AngleOpp = 180 - Angle;

				return AngleOpp;
			}

			// Function de calcule de l'hypothenuse des triangles ABF et BCG (si l'angle est > 90) avec l'angle gamma ou gamma' (opp).
			function CalcHypCos(Angle,Adj) {
				var Adj = Number(Adj),
					Angle = Math.radians(Number(Angle)),
					Hyp = 0;

				Hyp = Adj / Math.cos(Angle);

				return Hyp;
			}

			// Fonction de calcule de l'hypothenuse par sinus.
			function CalcHypSin(Angle,Opp) {
				var Angle = Math.radians(Number(Angle)),
					Opp = Number(Opp),
					Hyp = 0;
				
				Hyp = Opp / Math.sin(Angle);

				return Hyp;
			}

			// Fonction de calcule de l'angle avec sinus.
			function CalcAngleSin(Opp,Hyp) {
				var Opp = Number(Opp),
					Hyp = Number(Hyp),
					Angle = 0;

				Angle = Math.asin(Opp / Hyp);

				Angle = Math.degrees(Number(Angle));

				return Angle;
			}

			// Fonction de calcule Al-Kachi calcule GF
			function CalcAlKachiOpp(Adj1,Adj2,Angle) {
				var Adj1 = Number(Adj1),
					Adj2 = Number(Adj2),
					Angle = Math.radians(Number(Angle)),
					CoteOpp = 0;

				CoteOpp = Math.sqrt(Math.pow(Adj1,2) + Math.pow(Adj2,2) - 2 * Adj1 * Adj2 * Math.cos(Angle));

				return CoteOpp;
			}

			// Function de calcule Al-Kachi Angle
			function CalcAlKachiAngle(Adj1,Adj2,Opp) {
				var Adj1 = Number(Adj1),
					Adj2 = Number(Adj2),
					Opp = Number(Opp),
					Angle = 0;

				Angle = Math.acos((Math.pow(Opp,2) - Math.pow(Adj1,2) - Math.pow(Adj2,2)) / (-2 * Adj1 * Adj2));

				Angle = Math.degrees(Number(Angle));

				return Angle;
			}
			
			// Calcule Aire d'un triangle (Formule de Heron) Calcule GF
			function CalcHeron(Cote1,Cote2,Cote3) {
				var Cote1 = Number(Cote1),
					Cote2 = Number(Cote2),
					Cote3 = Number(Cote3),
					Aire = 0;
					p = 0;

				p = (Cote1 + Cote2 + Cote3) / 2;

				Aire = Math.sqrt(p*(p - Cote1)*(p - Cote2)*(p - Cote3));

				return Aire;
			}

			// Calcule de la hauteur issue d'un cote
			function CalcHauteur(Aire,Cote) {
				var Cote = Number(Cote),
					Aire = Number(Cote),
					Hauteur = 0;

				Hauteur = 2 * (Aire / Cote);

				return Hauteur;
			}

			// Calcule de l'angle EBC avec acos
			function CalcAngleCos(Hauteur,Hyp) {
				var Hauteur = Number(Hauteur),
					Hyp = Number(Hyp),
					AngleCos = 0;

				AngleCos = Math.acos(Hauteur / Hyp);

				return AngleCos;
			}
		
			// Calcule d'angle par Tangeante
			function CalcAngleTan(Adj,Opp) {
				var Adj = Number(Adj),
					Opp = Number(Opp),
					Angle = 0;

				Angle = Math.atan(Opp / Adj);

				Angle = Math.degrees(Number(Angle));

				return Angle;
			}

			// Corroyage : Fonction de calcule.
			function CalcComplet(Alpha,Beta,Gamma) {
				var Alpha = Number(Alpha),
					Beta = Number(Beta),
					Gamma = Number(Gamma),
					CoupeAB = 0,
					CoupeBC = 0,
					ProlBF = 0,
					OB = 0,
					OD = 0,
					AD = 0,
					CD = 0,
					AOD = 0,
					COD = 0,
					ED = 0,
					DF = 0,
					OE = 0,
					OF = 0,
					EF = 0,
					EDF = 0,
					EG = 0, // G intersection de l'arêtier sur sablière avec EF.
					Resultat = [0,0];	// Resultat
										// en 0 : Angle de corroyage de la bissectrice EDF,
										// en 1 : Angle sur la sabliere AOB,
										// en 2 : L'angle de corroyage sur chevron d'aplomb délardées pour le pan beta,
										// en 3 : L'angle de corroyage sur chevron d'aplomb délardées pour le pab Gamma.  

				// Calcule du pietement AB et BC des coupes perpendiculaire a l'arrete de la sabliere a une Hauteur de 1.
					CoupeAB = CalcAdjTan(Beta,1);
					CoupeBC = CalcAdjTan(Gamma,1);

				// Calcule de l'angle de l'arretier sur la sabliere en prolongeant une coupe sur un cote.
					ProlBF = CalcHypCos(Alpha,CoupeBC);
					Resultat[1] = CalcAdjTan(Alpha, (CoupeAB + ProlBF));
					Resultat[1] = Math.degrees(Math.atan(CoupeAB / Resultat[1]));
					if(Math.sign(Resultat[1]) == -1) {
						Resultat[1] = 180 + Resultat[1];
					}

				// Calcule de OB a partir de la coupe AB et l'angle de sabliere dans ADB.
					OB = CalcHypSin(Resultat[1],CoupeAB);

				// Calcule de OD dans ODB.
					OD = CalcPythyp(OB,1);

				// Calcule de AD dans ADB.
					AD = CalcHypSin(Beta,1);

				// Calcule de CD dans CBD.
					CD = CalcHypSin(Gamma,1);

				// Calcule de AOD dans AOD.
					if (Resultat[1] <= 90) {
						AOD = CalcAngleSin(AD,OD);
					}
					else {
						AOD = 180 - CalcAngleSin(AD,OD);
					}

				// Calcule de COD dans COD.
					if ((Alpha - Resultat[1]) <= 90) {
						COD = CalcAngleSin(CD,OD);
					}
					else {
						COD = 180 - CalcAngleSin(CD,OD);
					}

				// Calcule de ED dans EDO.
					ED = CalcOppTan(AOD,OD);

				// Calcule de DF dans DFO.
					DF = CalcOppTan(COD,OD);

				// Calcule de OE dans EDO.
					OE = CalcHypSin(AOD,ED);

				// Calcule de OF dans ODF.
					OF = CalcHypSin(COD,DF);

				// Calcule de EF + Angle EDF. Alkachi avec OF, OE et Alpha.
					if (Alpha == Number(180)) {
						EF = 0;
						EDF = 180;
					}
					// Noue
					else if (Resultat[1] > 90 && Alpha - Resultat[1] > 90) {
						EF = CalcAlKachiOpp(OE,OF,(360-Alpha));
						EDF = CalcAlKachiAngle(ED,DF,EF);
						EDF = 360 - EDF;
					}
					// Noue
					else if (Resultat[1] > 90 && Alpha - Resultat[1] <= 90 || Resultat[1] <= 90 && Alpha - Resultat[1] > 90) {
						EF = CalcAlKachiOpp(Math.abs(OE),Math.abs(OF),(180-Alpha));
						EDF = CalcAlKachiAngle(ED,DF,EF);
						EDF = 360 - EDF;
					}
					// arêtier
					else {
						EF = CalcAlKachiOpp(OE,OF,Alpha);
						EDF = CalcAlKachiAngle(ED,DF,EF);
					}

				// Angle de corroyage sur la bissectrice : Division par deux, et transformation en angle de reglage machine.
					if (Resultat[1] > 90 && Alpha - Resultat[1] <= 90 || Resultat[1] <= 90 && Alpha - Resultat[1] > 90) {
						Resultat[0] = EDF / 2 - 90;
					}
					else {
						Resultat[0] = 90 - EDF / 2;
					}

				// Arrêtier d'aplomb délardées.
					EG = CalcOppSin(Resultat[1],OE);
					GF = CalcOppSin((Alpha - Resultat[1]),OF);
					if (Resultat[1] <= 90) {
						Resultat[2] = 90 - CalcAngleSin(EG,ED);
					}
					else {
						Resultat[2] = CalcAngleSin(EG,ED) - 90;
					}
					if ((Alpha - Resultat[1]) <= 90) {
						Resultat[3] = 90 - CalcAngleSin(GF,DF);
					}
					else {
						Resultat[3] = CalcAngleSin(GF,DF) - 90;
					}

				return Resultat;
			}

			// fonction apparition des details de calcule
			
			function Show(x) {
				var ellst = document.getElementsByClassName(x);
				for (i = 0; i < ellst.length; i++) {
				ellst[i].style.opacity = 1;
				}
			}

			function unShow(x) {
				var ellst = document.getElementsByClassName(x);
				for (i = 0; i < ellst.length; i++) {
				ellst[i].style.opacity = 0;
				}
			}

		// Fonction de conversion deg radian

		Math.radians = function(degrees) {
			return degrees * Math.PI / 180;
		};

		Math.degrees = function(radians) {
			return radians * 180 / Math.PI;
		};
		
		// Fonction de suppresion de dessin selon ClassName
		function SupprSVGb(ClassName) {
			Poubelle = document.getElementsByClassName(ClassName);
			while (Poubelle.length > 0) {
				Poubelle[0].remove();
			}
		}

		// fonction d'invisibilisation de polygon selon ClassName
		function InvSVGb(ClassName) {
			for (let n = 0; n < document.getElementsByClassName(ClassName).length; n++) {
				document.getElementsByClassName(ClassName)[n].setAttribute("points", "");
			}
		}

		// fonction d'invisibilisation de polygon selon id
		function InvSVGId(id) {
			document.getElementById(id).setAttribute("points", "");
			document.getElementById(id).setAttribute("opacity", 0);
		}

		// Fonction de suppresion de dessin selon id
		function SupprSVGid(id) {
			document.getElementById(id).remove();
		}
		
		// Fonction de transformation des coordonnees absolue en iso.
		function AbstoIso(Abs) {
			IsoX = -0.35;
			IsoY = 0.35;
			IsoZ = -1;
			if (isNaN(Abs[2])) { Abs[2]=0; }
			Iso = [];
			Iso[0] = Abs[0] + Abs[1] * IsoX;
			Iso[1] = Abs[1] * IsoY + Abs[2] * IsoZ;
			return Iso;
		}

		// Declage autour origine Ortho
		function ToOrtho(Point, OriginOrtho) {
			Point[0] = Point[0] + OriginOrtho[0];
			Point[1] = Point[1] + OriginOrtho[1];
			return Point;
		}

		// Corroyage : fonction de dessin toiture double pente.
		function Dtdp(Alpha, Beta, Gamma) {
			var SVG = document.getElementById('fond'),
				Alpha = Number(Alpha),
				Beta = Number(Beta),
				Gamma = Number(Gamma),
				OriginOrtho = [75, 80],
				Point0 = [-50, 0, 0],
				Point1 = [-50, -50, 0],
				Point2 = [50, -50, 0],
				Point3 = [50, 50, 0],
				Point4 = [-50, 50, 0],
				Pointx = [], // Point Absolue d'arrive sur le perimetre exterieur.
				Pointf = [], // Point Absolue fetier sur le perimetre de sabliere.
				Pointf1 = [], // Point Absolue fetier au point 1
				Pointf2 = [], // Point Absolue fetier au point 2
				Pointf3 = [], // Point Absolue fetier au point 3
				Pointf4 = [], // Point Absolue fetier au point 4
				Pisof1 = [],
				Pisof2 = [],
				Pisof3 = [],
				Pisof4 = [],
				Piso0 = AbstoIso(Point0), // Point 0 Isometrique.
				Piso1 = AbstoIso(Point1), // Point 1 Isometrique.
				Piso2 = AbstoIso(Point2), // Point 2 Isometrique.
				Piso3 = AbstoIso(Point3), // Point 3 Isometrique.
				Piso4 = AbstoIso(Point4); // Point 4 Isometrique.
					
				// Placement des points absolue a l'origine.
				Piso0 = ToOrtho(Piso0,OriginOrtho); // Mise a l'origine du point 0
				Piso1 = ToOrtho(Piso1,OriginOrtho); // Mise a l'origine du point 1
				Piso2 = ToOrtho(Piso2,OriginOrtho); // Mise a l'origine du point 2
				Piso3 = ToOrtho(Piso3,OriginOrtho); // Mise a l'origine du point 3
				Piso4 = ToOrtho(Piso4,OriginOrtho); // Mise a l'origine du point 4

				// Execution du calcule
				Resultat = CalcComplet(Alpha,Beta,Gamma);

				// Suppression du trace precedent
				InvSVGb("Face");
				InvSVGb("FaceCache");
				InvSVGb("CoupeAlpha");
				InvSVGb("CoupeBeta");
				InvSVGb("CoupeGamma");

				// Placement du point de centre ortho
				NewPoint = document.getElementById("PointOrtho");
				NewPoint.setAttribute("cx", OriginOrtho[0]);
				NewPoint.setAttribute("cy", OriginOrtho[1]);

				// Dessin de sabliere
				SvgString = OriginOrtho[0]+","+OriginOrtho[1]+" "+Piso0[0]+","+Piso0[1]+" "+Piso1[0]+","+Piso1[1]+" "+Piso2[0]+","+Piso2[1]+" "+Piso3[0]+","+Piso3[1]+" "+Piso4[0]+","+Piso4[1]+","+Piso0[0]+","+Piso0[1];
				
				document.getElementById("Sabliere").setAttribute("points", SvgString);

				// Dessin du perimetre de la sabliere.
				if (Alpha >= 0 && Alpha <= 45) {
					Pointx[0] = Point0[0];
					Pointx[1] = CalcOppTan(Alpha, Point1[0]);
					Pisox = AbstoIso(Pointx);
					Pisox = ToOrtho(Pisox,OriginOrtho);
					SvgStringDotted = Piso0[0]+","+Piso0[1]+" "+Pisox[0]+","+Pisox[1];
					SvgString = OriginOrtho[0]+","+OriginOrtho[1]+" "+Piso0[0]+","+Piso0[1];
				}
				else if (Alpha > 45 && Alpha <= 135) {
					Pointx[0] = CalcAdjTan(Alpha,1) * Point1[1];
					Pointx[1] = Point1[1];
					Pisox = AbstoIso(Pointx);
					Pisox = ToOrtho(Pisox,OriginOrtho);
					SvgStringDotted = Piso0[0]+","+Piso0[1]+" "+Piso1[0]+","+Piso1[1]+" "+Pisox[0]+","+Pisox[1];
					SvgString = Pisox[0]+","+Pisox[1]+" "+OriginOrtho[0]+","+OriginOrtho[1]+" "+Piso0[0]+","+Piso0[1];
					
				}
				else if (Alpha > 135 && Alpha <= 225) {
					Pointx[0] = Point2[0];
					Pointx[1] = CalcOppTan(Alpha, Point2[0]);
					Pisox = AbstoIso(Pointx);
					Pisox = ToOrtho(Pisox,OriginOrtho);
					SvgStringDotted = Piso0[0]+","+Piso0[1]+" "+Piso1[0]+","+Piso1[1]+" "+Piso2[0]+","+Piso2[1];
					SvgString = Piso2[0]+","+Piso2[1]+" "+Pisox[0]+","+Pisox[1]+" "+OriginOrtho[0]+","+OriginOrtho[1]+" "+Piso0[0]+","+Piso0[1];
				}
				else if (Alpha > 225 && Alpha <= 315) {
					Pointx[0] = CalcAdjTan(Alpha,1) * Point3[1];
					Pointx[1] = Point3[1];
					Pisox = AbstoIso(Pointx);
					Pisox = ToOrtho(Pisox,OriginOrtho);
					SvgStringDotted = Piso0[0]+","+Piso0[1]+" "+Piso1[0]+","+Piso1[1]+" "+Piso2[0]+","+Piso2[1];
					SvgString = Piso2[0]+","+Piso2[1]+" "+Piso3[0]+","+Piso3[1]+" "+Pisox[0]+","+Pisox[1]+" "+OriginOrtho[0]+","+OriginOrtho[1]+" "+Piso0[0]+","+Piso0[1];
				}
				else if (Alpha > 315 && Alpha <= 360) {
					Pointx[0] = Point4[0];
					Pointx[1] = CalcOppTan(Alpha, Point4[0]);
					Pisox = AbstoIso(Pointx);
					Pisox = ToOrtho(Pisox,OriginOrtho);
					SvgStringDotted = Piso0[0]+","+Piso0[1]+" "+Piso1[0]+","+Piso1[1]+" "+Piso2[0]+","+Piso2[1];
					SvgString = Piso2[0]+","+Piso2[1]+" "+Piso3[0]+","+Piso3[1]+" "+Piso4[0]+","+Piso4[1]+" "+Pisox[0]+","+Pisox[1]+" "+OriginOrtho[0]+","+OriginOrtho[1]+" "+Piso0[0]+","+Piso0[1];
				}
				document.getElementById("SabliereDotted").setAttribute("points", SvgStringDotted);
				document.getElementById("SabliereDotted").setAttribute("opacity", 1);
				document.getElementById("SabliereDessin").setAttribute("points", SvgString);
				document.getElementById("SabliereDessin").setAttribute("opacity", 1);

				// Dessin de pente de toit de gauche & droite.
				if (Math.abs(Resultat[1]) >= 0 && Math.abs(Resultat[1]) <= 45) {

					Pointf[0] = Point0[0];
					Pointf[1] = CalcOppTan(Resultat[1], Point0[0]);
					Pointf[2] = CalcOppTan(Beta, Math.abs(Pointf[1]));
					Pisof = AbstoIso(Pointf);
					Pisof = ToOrtho(Pisof,OriginOrtho);

					SvgString = OriginOrtho[0]+","+OriginOrtho[1]+" "+Piso0[0]+","+Piso0[1]+" "+Pisof[0]+","+Pisof[1];

					if (Alpha > 45 && Alpha <= 135) {
						Pointf1 = Point1;
						Pointf1[2] = CalcOppTan(Gamma, CalcOppSin((Alpha - 45), CalcPythyp(Point1[0],Point1[1])));
						Pisof1 = AbstoIso(Pointf1);
						Pisof1 = ToOrtho(Pisof1,OriginOrtho);

						SvgString2 = OriginOrtho[0]+","+OriginOrtho[1]+" "+Pisox[0]+","+Pisox[1]+" "+Pisof1[0]+","+Pisof1[1]+" "+Pisof[0]+","+Pisof[1];
					}
					else if (Alpha > 135 && Alpha <= 225) {
						Pointf1 = Point1;
						Pointf1[2] = CalcOppTan(Gamma, CalcOppSin((Alpha - 45), CalcPythyp(Point1[0],Point1[1])));
						Pisof1 = AbstoIso(Pointf1);
						Pisof1 = ToOrtho(Pisof1,OriginOrtho);

						Pointf2 = Point2;
						Pointf2[2] = CalcOppTan(Gamma, CalcOppSin((Alpha - 135), CalcPythyp(Point2[0],Point2[1])));
						Pisof2 = AbstoIso(Pointf2);
						Pisof2 = ToOrtho(Pisof2,OriginOrtho);

						SvgString2 = OriginOrtho[0]+","+OriginOrtho[1]+" "+Pisox[0]+","+Pisox[1]+" "+Pisof2[0]+","+Pisof2[1]+" "+Pisof1[0]+","+Pisof1[1]+" "+Pisof[0]+","+Pisof[1];
					}
					else {
						SvgString2 = OriginOrtho[0]+","+OriginOrtho[1]+" "+Pisox[0]+","+Pisox[1]+" "+Pisof[0]+","+Pisof[1];
					}
				}
				else if (Math.abs(Resultat[1]) > 45 && Math.abs(Resultat[1]) <= 135) {

					Pointf1 = Point1;
					Pointf1[2] = CalcOppTan(Beta, Math.abs(Point1[1]));
					Pisof1 = AbstoIso(Pointf1);
					Pisof1 = ToOrtho(Pisof1,OriginOrtho);

					if (Math.abs(Resultat[1]) < 90) { Pointf[0] = - CalcOppTan((90 - Resultat[1]), Math.abs(Point1[1])); }
					if (Math.abs(Resultat[1]) == 90) { Pointf[0] = 0; }
					if (Math.abs(Resultat[1]) > 90) {Pointf[0] = CalcOppTan((Resultat[1] - 90), Math.abs(Point1[1]));
					}
					Pointf[1] = Point1[1];
					Pointf[2] = Pointf1[2];
					Pisof = AbstoIso(Pointf);
					Pisof = ToOrtho(Pisof,OriginOrtho);

					SvgString = OriginOrtho[0]+","+OriginOrtho[1]+" "+Piso0[0]+","+Piso0[1]+" "+Pisof1[0]+","+Pisof1[1]+" "+Pisof[0]+","+Pisof[1];
					if (Alpha > 135 && Alpha <= 225) {
						Pointf2 = Point2;
						Pointf2[2] = CalcOppTan(Gamma, CalcOppSin((Alpha - 135), CalcPythyp(Point2[0],Point2[1])));
						Pisof2 = AbstoIso(Pointf2);
						Pisof2 = ToOrtho(Pisof2,OriginOrtho);

						SvgString2 = OriginOrtho[0]+","+OriginOrtho[1]+" "+Pisox[0]+","+Pisox[1]+" "+Pisof2[0]+","+Pisof2[1]+" "+Pisof[0]+","+Pisof[1];
					}
					else if (Alpha > 225 && Alpha <= 315) {
						Pointf2 = Point2;
						Pointf2[2] = CalcOppTan(Gamma, CalcOppSin((Alpha - 135), CalcPythyp(Point2[0],Point2[1])));
						Pisof2 = AbstoIso(Pointf2);
						Pisof2 = ToOrtho(Pisof2,OriginOrtho);

						Pointf3 = Point3;
						Pointf3[2] = CalcOppTan(Gamma, CalcOppSin((Alpha - 225), CalcPythyp(Point3[0],Point3[1])));
						Pisof3 = AbstoIso(Pointf3);
						Pisof3 = ToOrtho(Pisof3,OriginOrtho);

						SvgString2 = OriginOrtho[0]+","+OriginOrtho[1]+" "+Pisox[0]+","+Pisox[1]+" "+Pisof3[0]+","+Pisof3[1]+" "+Pisof2[0]+","+Pisof2[1]+" "+Pisof[0]+","+Pisof[1];
					}
					else {
						SvgString2 = OriginOrtho[0]+","+OriginOrtho[1]+" "+Pisox[0]+","+Pisox[1]+" "+Pisof[0]+","+Pisof[1];
					}
				}
				else if (Math.abs(Resultat[1]) > 135 && Math.abs(Resultat[1]) <= 180) {

					Pointf1 = Point1;
					Pointf1[2] = CalcOppTan(Beta, Math.abs(Point1[1]));
					Pisof1 = AbstoIso(Pointf1);
					Pisof1 = ToOrtho(Pisof1,OriginOrtho);

					Pointf2 = Point2;
					Pointf2[2] = Pointf1[2];
					Pisof2 = AbstoIso(Pointf2);
					Pisof2 = ToOrtho(Pisof2,OriginOrtho);					

					Pointf[0] = Point2[0];
					if (Math.abs(Resultat[1]) < 180) { Pointf[1] = - CalcOppTan((180 - Resultat[1]), Math.abs(Point2[0])); }
					if (Math.abs(Resultat[1]) == 180) { Pointf[1] = 0; }
					if (Math.abs(Resultat[1]) > 180) { Pointf[1] = CalcOppTan((Resultat[1] - 180), Math.abs(Point2[0])); }
					Pointf[2] = CalcOppTan(Beta, Math.abs(Pointf[1]));
					Pisof = AbstoIso(Pointf);
					Pisof = ToOrtho(Pisof,OriginOrtho);

					SvgString = OriginOrtho[0]+","+OriginOrtho[1]+" "+Piso0[0]+","+Piso0[1]+" "+Pisof1[0]+","+Pisof1[1]+" "+Pisof2[0]+","+Pisof2[1]+" "+Pisof[0]+","+Pisof[1];
					if (Alpha > 225 && Alpha <= 315) {
						Pointf3 = Point3;
						Pointf3[2] = CalcOppTan(Gamma, CalcOppSin((Alpha - 225), CalcPythyp(Point3[0],Point3[1])));
						Pisof3 = AbstoIso(Pointf3);
						Pisof3 = ToOrtho(Pisof3,OriginOrtho);

						SvgString2 = OriginOrtho[0]+","+OriginOrtho[1]+" "+Pisof[0]+","+Pisof[1]+" "+Pisof3[0]+","+Pisof3[1]+" "+Pisox[0]+","+Pisox[1];
					}
					else if (Alpha > 315) {
						Pointf3 = Point3;
						Pointf3[2] = CalcOppTan(Gamma, CalcOppSin((Alpha - 225), CalcPythyp(Point3[0],Point3[1])));
						Pisof3 = AbstoIso(Pointf3);
						Pisof3 = ToOrtho(Pisof3,OriginOrtho);

						Pointf4 = Point4;
						Pointf4[2] = CalcOppTan(Gamma, CalcOppSin((Alpha - 315), CalcPythyp(Point4[0],Point4[1])));
						Pisof4 = AbstoIso(Pointf4);
						Pisof4 = ToOrtho(Pisof4,OriginOrtho);

						SvgString2 = OriginOrtho[0]+","+OriginOrtho[1]+" "+Pisof[0]+","+Pisof[1]+" "+Pisof3[0]+","+Pisof3[1]+" "+Pisof4[0]+","+Pisof4[1]+" "+Pisox[0]+","+Pisox[1];
					}
					else {

						SvgString2 = OriginOrtho[0]+","+OriginOrtho[1]+" "+Pisof[0]+","+Pisof[1]+" "+Pisox[0]+","+Pisox[1];
					}

				}

				// Calcule si le point x sur sabliere est devant ou derriere.
				CoefDir = (Pisof[1] - OriginOrtho[1]) / (Pisof[0] - OriginOrtho[0]);
				b = OriginOrtho[1] - CoefDir * OriginOrtho[0];
				if (Pisox[1] >= CoefDir * Pisox[0] + b && Alpha >= 0 && Alpha <= 71) {
					document.getElementById("FaceDDotted").setAttribute("points", SvgString2);
					document.getElementById("FaceDDotted").setAttribute("opacity", 1);
					InvSVGId("FaceD");
				}
				else {
					document.getElementById("FaceD").setAttribute("points", SvgString2);
					document.getElementById("FaceD").setAttribute("opacity", 1);
					InvSVGId("FaceDDotted");
				}
				document.getElementById("FaceG").setAttribute("points", SvgString);
				document.getElementById("FaceG").setAttribute("opacity", 1);

				// Dessin des remontees verticales

				lPointf = [Pointf1,Pointf2,Pointf3,Pointf4];

				for (let n = 0; n < lPointf.length; n++) {
					if (lPointf[n].length == 3) {
						fp = [lPointf[n][0], lPointf[n][1], 0];
						fpiso = AbstoIso(fp);
						fpiso = ToOrtho(fpiso,OriginOrtho);
						fpf = [lPointf[n][0], lPointf[n][1], lPointf[n][2]];
						fpfiso = AbstoIso(fpf);
						fpfiso = ToOrtho(fpfiso,OriginOrtho);
						document.getElementById("Rem"+(n + 1)).setAttribute("points", fpiso[0]+","+fpiso[1]+" "+fpfiso[0]+","+fpfiso[1]);
					}
				}

				// Dessin des coupes

				var DimAlpha = 15, // Rayon du cercle pour Alpha.
					PrecisionA = 10, // Angle de coupe du rond Alpha.
					DimBG = 2/3, // Fraction de cote, pour les coupes.
					RondCoupeProp = 1/3; // Fraction de cote pour les rond des coupes.
					
				// Coupe alpha
				var CoupeA1 = [],
					CoupeA2 = [],
					CoupeA3 = [],
					A1iso = [],
					A2iso = [],
					a3iso = [];

				CoupeA1 = [-DimAlpha, Point0[1], Point0[2]];
				CA1iso = AbstoIso(CoupeA1);
				CA1iso = ToOrtho(CA1iso,OriginOrtho);

				CoupeA2 = [- CalcAdjCos(Alpha,DimAlpha),- CalcOppSin(Alpha,DimAlpha), Point0[2]];

				CA2iso = AbstoIso(CoupeA2);
				CA2iso = ToOrtho(CA2iso,OriginOrtho);

				let SvgStringAlpha = CA1iso[0]+" "+CA1iso[1];
				let pt = [];
				for (let n = PrecisionA; n < Alpha; n = n + PrecisionA) {
					pt[0] = - CalcAdjCos(n,DimAlpha);
					pt[1] = - CalcOppSin(n,DimAlpha);
					pt = AbstoIso(pt);
					pt = ToOrtho(pt,OriginOrtho);
					SvgStringAlpha = SvgStringAlpha+", "+pt[0]+" "+pt[1];
				}
				SvgStringAlpha = SvgStringAlpha+", "+CA2iso[0]+" "+CA2iso[1]+", "+OriginOrtho[0]+" "+OriginOrtho[1];

				document.getElementById("CoupeAlpha").setAttribute("points", SvgStringAlpha);

				// Coupe Beta
				var CoupeB1 = [],
					CoupeB2 = [],
					CoupeB3 = [],
					B1iso = [],
					B2iso = [],
					B3iso = [],
					CRondB1 = [],
					CRondB2 = [],
					CRondB3 = [],
					BR1iso = [],
					BR2iso = [],
					BR3iso = [];

				CoupeB1 = [Point0[0] * DimBG, Point0[1] * DimBG, Point0[2] * DimBG];
				B1iso = AbstoIso(CoupeB1);
				B1iso = ToOrtho(B1iso,OriginOrtho);

				if (Math.abs(CalcOppTan(Resultat[1], CoupeB1[0])) < Math.abs(Point1[1]) && Alpha < 180) { CoupeB2 = [CoupeB1[0], - Math.abs(CalcOppTan(Resultat[1], CoupeB1[0])), Point0[2]]; }
				else { CoupeB2 = [CoupeB1[0], Point1[1], Point0[2]]; }
				B2iso = AbstoIso(CoupeB2);
				B2iso = ToOrtho(B2iso,OriginOrtho);

				CoupeB3 = [CoupeB2[0], - Math.abs(CoupeB2[1]), CalcOppTan(Beta,Math.abs(CoupeB2[1]))];
				B3iso = AbstoIso(CoupeB3);
				B3iso = ToOrtho(B3iso,OriginOrtho);

				SvgStringBeta = B1iso[0]+" "+B1iso[1]+", "+B2iso[0]+" "+B2iso[1]+", "+B3iso[0]+" "+B3iso[1];

				document.getElementById("CoupeBeta").setAttribute("points", SvgStringBeta);

				CRondB2 = [(CoupeB2[0] - CoupeB1[0]) * RondCoupeProp + CoupeB1[0], (CoupeB2[1] - CoupeB1[1]) * RondCoupeProp + CoupeB1[1], (CoupeB2[2] - CoupeB1[2]) * RondCoupeProp + CoupeB1[2]];
				BR2iso = AbstoIso(CRondB2);
				BR2iso = ToOrtho(BR2iso,OriginOrtho);

				CRondB3 = [(CoupeB3[0] - CoupeB1[0]) * RondCoupeProp + CoupeB1[0], (CoupeB3[1] - CoupeB1[1]) * RondCoupeProp + CoupeB1[1], (CoupeB3[2] - CoupeB1[2]) * RondCoupeProp + CoupeB1[2]];
				BR3iso = AbstoIso(CRondB3);
				BR3iso = ToOrtho(BR3iso,OriginOrtho);

				SvgStringGamma = B1iso[0]+" "+B1iso[1]+", "+BR2iso[0]+" "+BR2iso[1]+", "+BR3iso[0]+" "+BR3iso[1];
				document.getElementById("CoupeBetaRond").setAttribute("points", SvgStringGamma);

				// Coupe Gamma.
				var CoupeG1 = [],
					CoupeG2 = [],
					CoupeG3 = [],
					G1iso = [],
					G2iso = [],
					G3iso = [],
					CRondG1 = [],
					CRondG2 = [],
					CRondG3 = [],
					GR1iso = [],
					GR2iso = [],
					GR3iso = [],
					ArSurSab = 0;

				CoupeG1 = [CalcAdjCos(Alpha,Point0[0] * DimBG),CalcOppSin(Alpha,Point0[0] * DimBG), Point0[2] * DimBG];
				G1iso = AbstoIso(CoupeG1);
				G1iso = ToOrtho(G1iso,OriginOrtho);

				ArSurSab = CalcHypCos((Alpha-Resultat[1]),Math.abs(Point0[0] * DimBG));

				if (Alpha >= 0 && Alpha <= 45) {
					CoupeG2[0] = - CalcAdjCos(Resultat[1],ArSurSab);
					CoupeG2[1] = - CalcOppSin(Resultat[1],ArSurSab);
				}
				else if (Alpha > 45) {
					var IntArCote2X = - CalcOppTan(90 - Resultat[1], Point1[1]); // Absysse du point d'intersection entre l'arretier sur sabliere et le cote 2.
						IntArCote3Y = - CalcOppTan(180 - Resultat[1], Point2[0]); // Ordonne du point d'intersection entre l'arretier sur sabliere et le cote 3.

						CoupeSurCote1 = (CoupeG1[1] + CalcOppTan(Alpha-90, Point0[0] - CoupeG1[0]));
						CoupeSurCote2 = CalcAdjTan(Alpha - 180,Point1[1]);
						CoupeSurCote2 = CoupeSurCote2 - CalcHypSin(Alpha-90, CalcHypSin(Alpha-90, CoupeSurCote2 - CoupeG1[0]));
						CoupeSurCote3 = CalcOppTan(Alpha - 180,Point2[0]);
						CoupeSurCote3 = CoupeSurCote3 - CalcHypSin(Alpha-180, CalcHypSin(Alpha-180, CoupeSurCote3 - CoupeG1[1]));
						CoupeSurCote4 = CalcOppTan(Alpha - 270,Point3[1]);
						CoupeSurCote4 = - CoupeSurCote4 + CalcHypSin(Alpha-270, CalcHypSin(Alpha-270, CoupeSurCote4 + CoupeG1[0]));

						intSurAretX = - CalcAdjCos(Resultat[1],ArSurSab); // Abyssse du point d'intersection du plan CoupeGamma avec l'aretier.
						intSurAretY = - CalcOppSin(Resultat[1],ArSurSab);; // Ordonnee du point d'intersection du plan CoupeGamma avec l'aretier.

						if (intSurAretX <= Point0[0] && CoupeSurCote1 > Point1[1] && Alpha > 45 && Alpha < 135) {
							CoupeG2[0] = Point0[0];
							CoupeG2[1] = CoupeSurCote1;
						}

						// Cote 2
						else if (Alpha == 180) {
							CoupeG2[0] = CoupeG1[0];
							CoupeG2[1] = Point1[1];
						}
						else if (intSurAretY <= Point1[1] && CoupeSurCote1 <= Point1[1] && Alpha > 45 && Alpha < 225) {
							CoupeG2[0] = CoupeSurCote2;
							CoupeG2[1] = Point1[1];
						}
						else if (Alpha-Resultat[1] > 90 && intSurAretX <= Point2[0] && CoupeSurCote2 <= Point2[0] && Alpha > 45 && Alpha < 225) {
							CoupeG2[0] = CoupeSurCote2;
							CoupeG2[1] = Point1[1];
						}
						else if (Alpha-Resultat[1] > 90 && CoupeSurCote2 >= Point1[0] && CoupeSurCote2 <= Point2[0] && Alpha > 45 && Alpha < 225) {
							CoupeG2[0] = CoupeSurCote2;
							CoupeG2[1] = Point1[1];
						}

						// Cote 3
						else if (Alpha == 270) {
							CoupeG2[0] = Point2[0];
							CoupeG2[1] = CoupeG1[1];
						}
						else if (CoupeSurCote2 >= Point1[0] && CoupeSurCote2 <= Point2[0] && intSurAretY <= Point2[1] && Alpha > 180 && Alpha < 225) {
							CoupeG2[0] = CoupeSurCote2;
							CoupeG2[1] = Point1[1];
						}
						else if (CoupeSurCote3 >= Point2[1] && CoupeSurCote3 <= Point3[1] && CoupeSurCote3 >= IntArCote3Y && Alpha > 180 && Alpha < 315) {
							CoupeG2[0] = Point2[0];
							CoupeG2[1] = CoupeSurCote3;
						}
						else if (Alpha-Resultat[1] > 90 && CoupeSurCote3 >= Point2[1] && CoupeSurCote3 <= Point3[1] && Alpha > 180 && Alpha < 315) {
							CoupeG2[0] = Point2[0];
							CoupeG2[1] = CoupeSurCote3;
						}
						
						// Cote 4
						else if (Alpha-Resultat[1] > 90 && CoupeSurCote4 >= Point4[0] && CoupeSurCote4 <= Point3[0] && Alpha > 225 && Alpha < 360) {
							CoupeG2[0] = CoupeSurCote4;
							CoupeG2[1] = Point3[1];
						}

						// Arretier
						else {
							CoupeG2[0] = - CalcAdjCos(Resultat[1],ArSurSab);
							CoupeG2[1] = - CalcOppSin(Resultat[1],ArSurSab);
						}
				}
				G2iso = AbstoIso(CoupeG2);
				G2iso = ToOrtho(G2iso,OriginOrtho);

				CoupeG3 = [CoupeG2[0], CoupeG2[1], CalcOppTan(Gamma,CalcPythyp(CoupeG2[0] - CoupeG1[0], CoupeG2[1] - CoupeG1[1]))];
				G3iso = AbstoIso(CoupeG3);
				G3iso = ToOrtho(G3iso,OriginOrtho);

				SvgStringGamma = G1iso[0]+" "+G1iso[1]+", "+G2iso[0]+" "+G2iso[1]+", "+G3iso[0]+" "+G3iso[1];
				document.getElementById("CoupeGamma").setAttribute("points", SvgStringGamma);

				CRondG2 = [(CoupeG2[0] - CoupeG1[0]) * RondCoupeProp + CoupeG1[0], (CoupeG2[1] - CoupeG1[1]) * RondCoupeProp + CoupeG1[1], (CoupeG2[2] - CoupeG1[2]) * RondCoupeProp + CoupeG1[2]];
				GR2iso = AbstoIso(CRondG2);
				GR2iso = ToOrtho(GR2iso,OriginOrtho);

				CRondG3 = [(CoupeG3[0] - CoupeG1[0]) * RondCoupeProp + CoupeG1[0], (CoupeG3[1] - CoupeG1[1]) * RondCoupeProp + CoupeG1[1], (CoupeG3[2] - CoupeG1[2]) * RondCoupeProp + CoupeG1[2]];
				GR3iso = AbstoIso(CRondG3);
				GR3iso = ToOrtho(GR3iso,OriginOrtho);

				SvgStringGamma = G1iso[0]+" "+G1iso[1]+", "+GR2iso[0]+" "+GR2iso[1]+", "+GR3iso[0]+" "+GR3iso[1];
				document.getElementById("CoupeGammaRond").setAttribute("points", SvgStringGamma);

			}

			// Tracer du dessin par default.
			var AlphaDef = 230,
				BetaDef = 45,
				GammaDef = 36;
			Dtdp(AlphaDef,BetaDef,GammaDef);
			
			// Fonction vider placeholder
			function EmptyPlace() {
				
				var Form = document.getElementById('Form'),
				inputs = document.getElementsByTagName('input'),
				inputsLength = inputs.length;
				
				for (var i = 0 ; i < inputsLength ; i++) {
					if (inputs[i].type == 'number') {
						inputs[i].placeholder = '';
					}
				}
			};

			// Vider les Value+PlaceHolder
			function EmptyAll() {
					
				var Form = document.getElementById('Form'),
					inputs = document.getElementsByTagName('input'),
					inputsLength = inputs.length;
				
				for (var i = 0 ; i < inputsLength ; i++) {
					if (inputs[i].type == 'number') {
						inputs[i].placeholder = '';
						inputs[i].value = '';
						inputs[i].className = '';
					}
				}		
			};
		
			// Verification des donnees de depart (en cas d'actualisation de la page)
			document.getElementById('fAlpha').value = AlphaDef;
			document.getElementById('fBeta').value = BetaDef;
			document.getElementById('fGamma').value = GammaDef;

			var fAlpha = document.getElementById('fAlpha'),
				fBeta = document.getElementById('fBeta'),
				fGamma = document.getElementById('fGamma'),
				fAlphaValue = fAlpha.value,
				fBetaValue = fBeta.value,
				fGammaValue = fGamma.value;

			Resultat = CalcComplet(fAlphaValue,fBetaValue,fGammaValue);
			document.getElementById('rCor').placeholder = arrondir(Resultat[0]);
			document.getElementById('rArApDelBeta').placeholder = arrondir(Resultat[2]);
			document.getElementById('rArApDelGamma').placeholder = arrondir(Resultat[3]);
			if (fAlphaValue > 180) {
				document.getElementById('spanCor').innerText = "Bissectrice sur Noue :    ";
			}
			else {
				document.getElementById('spanCor').innerText = "Bissectrice sur arêtier : ";
			}

			fAlpha.className = 'correct';
			fBeta.className = 'correct';
			fGamma.className = 'correct';

			Dtdp(fAlphaValue, fBetaValue, fGammaValue);

			// Fonctions de verification du formulaire

			var check = {};

			check['fAlpha'] = function() {

				var fAlpha = document.getElementById('fAlpha'),
					fBeta = document.getElementById('fBeta'),
					fGamma = document.getElementById('fGamma'),
					fAlphaValue = fAlpha.value,
					fBetaValue = fBeta.value,
					fGammaValue = fGamma.value;

				if (!isNaN(fAlphaValue) && Number(fAlphaValue) > 0 && Number(fAlphaValue) < 360  && fAlphaValue != String("")) {
					fAlpha.className = 'correct';
				}
				else if (fAlphaValue == String("")) {
					InvSVGb("Face");
					InvSVGb("FaceCache");
					InvSVGb("CoupeAlpha");
					InvSVGb("CoupeBeta");
					InvSVGb("CoupeGamma");
					fAlpha.className = 'incorrect';
				}
				else {
					// Suppression du trace precedent
					InvSVGb("Face");
					InvSVGb("FaceCache");
					InvSVGb("CoupeAlpha");
					InvSVGb("CoupeBeta");
					InvSVGb("CoupeGamma");
					fAlpha.className = "incorrect";
				}
				// Verification complete.
				if ((!isNaN(fAlphaValue) && Number(fAlphaValue) > 0 && Number(fAlphaValue) < 360  && fAlphaValue != String(""))
					&& (!isNaN(fBetaValue) && Number(fBetaValue) >= 0 && Number(fBetaValue) < 90 && fBetaValue != String(""))
					&& (!isNaN(fGammaValue) && Number(fGammaValue) >= 0 && Number(fGammaValue) < 90 && fGammaValue != String(""))) {
					Resultat = CalcComplet(fAlphaValue,fBetaValue,fGammaValue);
					document.getElementById('rCor').placeholder = arrondir(Resultat[0]);
					document.getElementById('rArApDelBeta').placeholder = arrondir(Resultat[2]);
					document.getElementById('rArApDelGamma').placeholder = arrondir(Resultat[3]);
					if (fAlphaValue > 180) {
						document.getElementById('spanCor').innerText = "Bissectrice sur Noue :    ";
					}
					else {
						document.getElementById('spanCor').innerText = "Bissectrice sur arêtier : ";
					}
					Dtdp(fAlphaValue, fBetaValue, fGammaValue);
				}
				else {
					document.getElementById('rCor').placeholder = "";
					document.getElementById('rArApDelBeta').placeholder = "";
					document.getElementById('rArApDelGamma').placeholder = "";
				}
			};

			check['fBeta'] = function() {

				var fAlpha = document.getElementById('fAlpha'),
					fBeta = document.getElementById('fBeta'),
					fGamma = document.getElementById('fGamma'),
					fAlphaValue = fAlpha.value,
					fBetaValue = fBeta.value,
					fGammaValue = fGamma.value;

				if (!isNaN(fBetaValue) && Number(fBetaValue) >= 0 && Number(fBetaValue) < 90 && fBetaValue != String("")) {
					fBeta.className = 'correct';
				}
				else if (fBetaValue == String("")) {
					InvSVGb("Face");
					InvSVGb("FaceCache");
					InvSVGb("CoupeAlpha");
					InvSVGb("CoupeBeta");
					InvSVGb("CoupeGamma");
					fBeta.className = 'incorrect';
				}
				else {
					InvSVGb("Face");
					InvSVGb("FaceCache");
					InvSVGb("CoupeAlpha");
					InvSVGb("CoupeBeta");
					InvSVGb("CoupeGamma");
					fBeta.className = "incorrect";
				}
				// Verification complete.
				if ((!isNaN(fAlphaValue) && Number(fAlphaValue) > 0 && Number(fAlphaValue) < 360  && fAlphaValue != String(""))
					&& (!isNaN(fBetaValue) && Number(fBetaValue) >= 0 && Number(fBetaValue) < 90 && fBetaValue != String(""))
					&& (!isNaN(fGammaValue) && Number(fGammaValue) >= 0 && Number(fGammaValue) < 90 && fGammaValue != String(""))) {
					Resultat = CalcComplet(fAlphaValue,fBetaValue,fGammaValue);
					document.getElementById('rCor').placeholder = arrondir(Resultat[0]);
					document.getElementById('rArApDelBeta').placeholder = arrondir(Resultat[2]);
					document.getElementById('rArApDelGamma').placeholder = arrondir(Resultat[3]);
					if (fAlphaValue > 180) {
						document.getElementById('spanCor').innerText = "Bissectrice sur Noue :    ";
					}
					else {
						document.getElementById('spanCor').innerText = "Bissectrice sur arêtier : ";
					}
					Dtdp(fAlphaValue, fBetaValue, fGammaValue);
				}
				else {
					document.getElementById('rCor').placeholder = "";
					document.getElementById('rArApDelBeta').placeholder = "";
					document.getElementById('rArApDelGamma').placeholder = "";
				}
			};

			check['fGamma'] = function() {

				var fAlpha = document.getElementById('fAlpha'),
					fBeta = document.getElementById('fBeta'),
					fGamma = document.getElementById('fGamma'),
					fAlphaValue = fAlpha.value,
					fBetaValue = fBeta.value,
					fGammaValue = fGamma.value;
					console.log("t'es censé me le trouver la : "+fGamma.value);

				if (!isNaN(fGammaValue) && Number(fGammaValue) >= 0 && Number(fGammaValue) < 90 && fGammaValue != String("")) {
					fGamma.className = 'correct';
				}
				else if (fGammaValue == String("")) {
					InvSVGb("Face");
					InvSVGb("FaceCache");
					InvSVGb("CoupeAlpha");
					InvSVGb("CoupeBeta");
					InvSVGb("CoupeGamma");
					fGamma.className = 'incorrect';
				}
				else {
					InvSVGb("Face");
					InvSVGb("FaceCache");
					InvSVGb("CoupeAlpha");
					InvSVGb("CoupeBeta");
					InvSVGb("CoupeGamma");
					fGamma.className = "incorrect";
				}
				console.log("t'es censé me le trouver la : "+fGamma.value);
				// Verification complete.
				if ((!isNaN(fAlphaValue) && Number(fAlphaValue) > 0 && Number(fAlphaValue) < 360  && fAlphaValue != String(""))
					&& (!isNaN(fBetaValue) && Number(fBetaValue) >= 0 && Number(fBetaValue) < 90 && fBetaValue != String(""))
					&& (!isNaN(fGammaValue) && Number(fGammaValue) >= 0 && Number(fGammaValue) < 90 && fGammaValue != String(""))) {
					Resultat = CalcComplet(fAlphaValue,fBetaValue,fGammaValue);
					document.getElementById('rCor').placeholder = arrondir(Resultat[0]);
					document.getElementById('rArApDelBeta').placeholder = arrondir(Resultat[2]);
					document.getElementById('rArApDelGamma').placeholder = arrondir(Resultat[3]);
					if (fAlphaValue > 180) {
						document.getElementById('spanCor').innerText = "Bissectrice sur Noue : ";
					}
					else {
						document.getElementById('spanCor').innerText = "Bissectrice sur arêtier : ";
					}
					Dtdp(fAlphaValue, fBetaValue, fGammaValue);
				}
				else {
					document.getElementById('rCor').placeholder = "";
					document.getElementById('rArApDelBeta').placeholder = "";
					document.getElementById('rArApDelGamma').placeholder = "";
				}
			};

			// Mise en place des evenements
			(function() {
			
				var Form = document.getElementById('Form'),
					inputs = document.getElementsByTagName('input'),
					inputsLength = inputs.length;
			
				for (var i = 0 ; i < inputsLength ; i++) {
					if (inputs[i].type == 'number') {
			
						inputs[i].addEventListener('keyup', function(e) {
							check[e.target.id](e.target.id); // "e.target" represente l'input actuellement modifie
						}, false);
			
					}
				}
			})();

		</script>

		<div id="footer">
			<p>
				Faire un don <strong><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=593KJEYR3GQ2S" title="PayPal">Paypal</a></strong> - 
				<a href="https://framagit.org/Frederiiic/Pythagone">Code Source</a>	GPLv3 <img id="copyleft" src="img/Copyleft.svg" alt="Copyleft"> <a href="https://www.fredericpavageau.net">Frederic Pavageau</a> 2025.
			</p>
		</div>
		
	</body>
</html>