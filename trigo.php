<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Pythagone : Trigonométrie et Pythagore</title>
		<meta name="viewport" content="width=800, user-scalable=no">
		<meta name="description" content="Pythagone : Calcule toutes les valeurs possibles à l'aide du théorème de pythagore & la trigonométrie dans un triangle rectangle, selon les valeurs fournies, par Frédéric Pavageau." />
		<meta name="keywords" content="Pythagore, Euclide, Trigonométrie, Cosinus, Sinus, Tangeante, hypothénuse, angle, angle de coupe, hypothénuse, triangle, calcule, géométrie, construction, frédéric Pavageau." />
		<meta property="og:site_name" content="Pythagone" /> 
		<meta property="og:title" content="Pythagone : Trigonométrie & Pythagore" />
		<meta property="og:type" content="website" /> 
		<meta property="og:url" content="https://www.pythagone.net/" />
		<meta property="og:description" content="Calcule toutes les valeurs possibles à l'aide du théorème de pythagore & la trigonométrie dans un triangle rectangle, selon les valeurs fournies par Frédéric pavageau." />
		<meta property="og:image" content="https://pythagone.fredericpavageau.net/img/Pythagone.svg" />
		<meta name="twitter:card" content="summary_large_image" />
		<link rel="stylesheet" href="style.css" />
		<link rel="stylesheet" href="trigo.css" />
		<link rel="icon" type="image/svg+xml" href="img/Pythagone.svg" sizes="any"/>
		<link rel="icon" type="image/png" href="img/16-flavico.png" sizes="16x16"/>
		<link rel="icon" type="image/png" href="img/32-flavico.png" sizes="32x32"/>
		<link rel="icon" type="image/png" href="img/64-flavico.png" sizes="64x64"/>
		<link rel="icon" type="image/png" href="img/96-flavico.png" sizes="96x96"/>
		<link rel="icon" type="image/png" href="img/256-flavico.png" sizes="256x256"/>
	</head>

  	<body>

		<h1>Pythagone : Trigonométrie et Pythagore</h1>
		<h2>Pythagone : Calcule toutes les valeurs possibles à l'aide du théorème de pythagore & la trigonométrie dans un triangle rectangle, selon les valeurs fournies, par Frédéric Pavageau.</h2>
	
		<div id="backdrop"></div>
		
		<div id="sidenav">
			<?php require "menu.php"; ?>
			<script>
				mdf = document.getElementById("trigo");
				mdf.className = "active";
			</script>
		</div>
		
		<div id="content">
    
			<header>
				<div id="menu-toggle">
					<img id="menu" src="img/Menu.svg" alt="Bouton d'ouverture du menu" />
				</div>
			</header>
		
			<div id="formula">
				<div class="uform">
					<math xmlns="http://www.w3.org/1998/Math/MathML">
						<mrow>
							<msup>
								<mi>Hypoténuse</mi>
								<mn>2</mn>
							</msup>
							<mo>=</mo>
							<msup>
								<mi>a</mi>
								<mn>2</mn>
							</msup>
							<mo>+</mo>
							<msup>
								<mi>b</mi>
								<mn>2</mn>
							</msup>
						</mrow>
					</math>
					&nbsp;(<a href="https://fr.wikipedia.org/wiki/Pythagore" target="_blank" class="wikilink" title="Wikipédia">Pythagore</a>)
				</div>
				<div class="uform">
					<math xmlns="http://www.w3.org/1998/Math/MathML">
						<mrow>
							<mi>Cos (β)</mi>
							<mo>=</mo>
							<mfrac>
								<mrow>
								<mi>Côté adjacent (b)</mi>
								</mrow>
									<mrow>
									<mi>Hypoténuse</mi>
								</mrow>
							</mfrac>
						</mrow>
					</math>
					&nbsp;(<a href="https://fr.wikipedia.org/wiki/Trigonom%C3%A9trie" target="_blank" class="wikilink" title="Wikipédia">Trigo</a>)
				</div>
				<div class="uform">
					<math xmlns="http://www.w3.org/1998/Math/MathML">
						<mrow>
							<mi>Sin (β)</mi>
							<mo>=</mo>
							<mfrac>
								<mrow>
									<mi>Côté opposé (a)</mi>
								</mrow>
								<mrow>
									<mi>Hypoténuse</mi>
								</mrow>
							</mfrac>
						</mrow>
					</math>
					&nbsp;(<a href="https://fr.wikipedia.org/wiki/Trigonom%C3%A9trie" target="_blank" class="wikilink" title="Wikipédia">Trigo</a>)
				</div>
				<div class="uform">
					<math xmlns="http://www.w3.org/1998/Math/MathML">
						<mrow>
							<mi>Tan (β)</mi>
							<mo>=</mo>
							<mfrac>
								<mrow>
									<mi>Côté opp. (a)</mi>
								</mrow>
								<mrow>
									<mi>Côté adj. (b)</mi>
								</mrow>
							</mfrac>
						</mrow>
					</math>
					&nbsp;(<a href="https://fr.wikipedia.org/wiki/Trigonom%C3%A9trie" target="_blank" class="wikilink" title="Wikipédia">Trigo</a>)
				</div>
			</div>

			<svg id="fond" viewBox="0 0 202 98" xml:lang="fr"
				xmlns="http://www.w3.org/2000/svg"
				xmlns:xlink="http://www.w3.org/1999/xlink">
				<title>Triangle Rectangle</title>
				<polyline points="64 13, 76 22, 85 10" stroke="#69c" fill="transparent" stroke-width="0.5"/>
				<path d="M26 97 Q 25 84 16 77" stroke="#69c" fill="transparent" stroke-width="0.5"/>
				<path d="M166 97 Q 166 84 173 76" stroke="#69c" fill="transparent" stroke-width="0.5"/>
				<polygon points="73 1, 201 97, 1 97" stroke="#036" fill="transparent" stroke-width="0.5"/>
				<path class="alpha" d="M26 97 Q 25 84 16 77" stroke="rgba(68, 191, 68, 1)" fill="transparent" stroke-width="0.5"/>
				<path class="beta" d="M166 97 Q 166 84 173 76" stroke="rgba(68, 191, 68, 1)" fill="transparent" stroke-width="0.5"/>
				<polyline class="sega" points="1 97, 73 1" stroke="rgba(68, 191, 68, 1)" fill="transparent" stroke-width="0.5"/>
				<polyline class="segb" points="73 1, 201 97" stroke="rgba(68, 191, 68, 1)" fill="transparent" stroke-width="0.5"/>
				<polyline class="hyp" points="201 97, 1 97" stroke="rgba(68, 191, 68, 1)" fill="transparent" stroke-width="0.5"/>
			</svg>

			<form id="Form">
				<span id="spanalpha" class="cont">α : <input name="alpha" onfocus="Show('alpha')" onblur="unShow('alpha')" id="alpha" type="number"/><br/>
					<span class="tooltip">Doit être un angle.</span>
				</span>
				<span id="spanbeta" class="cont">β : <input name="beta" onfocus="Show('beta')" onblur="unShow('beta')" id="beta" type="number"/><br/>
					<span class="tooltip">Doit être un angle.</span>
				</span>
				<span id="spanb" class="cont">b : <input name="segb" onfocus="Show('segb')" onblur="unShow('segb')" id="segb" type="number"/><br/>
					<span class="tooltip">Impossible.</span>
				</span>
				<span id="spana" class="cont">a : <input name="sega" onfocus="Show('sega')" onblur="unShow('sega')" id="sega" type="number"/><br/>
					<span class="tooltip">Impossible.</span>
				</span>
				<span id="spanhyp" class="cont">Hypoténuse : <input name="hyp" onfocus="Show('hyp')" onblur="unShow('hyp')" id="hyp" type="number"/><br/>
					<span class="tooltip">Impossible.</span>
				</span>
				<span id="spanreset"><input id="reset" type="reset" value="Réinitialiser" /></span>
			</form>

		</div>

		<script src="sidenav.min.js"></script>
		
		<script src="Menu.js"></script>
    
		<script>
		
			// Modification des formules de Math pour chrome et internet explorer
			if (navigator.userAgent.toLowerCase().match('chrome') || /MSIE 10/i.test(navigator.userAgent) || /MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /Edge\/\d./i.test(navigator.userAgent)) {
				var	ellst = document.getElementsByClassName('uform'),
					ellstlength = ellst.length,
					uform = document.getElementById('uform');
						
				ellst[0].innerHTML = 'Cos(β) = Côté adjacent (b) / Hypoténuse &nbsp; (<a href="https://fr.wikipedia.org/wiki/Trigonom%C3%A9trie" target="_blank" title="Wikipédia">Trigonométrie</a>)';
				ellst[1].innerHTML = 'Sin(β) = Côté opposé (a) / Hypoténuse &nbsp; (<a href="https://fr.wikipedia.org/wiki/Trigonom%C3%A9trie" target="_blank" title="Wikipédia">Trigonométrie</a>)';
				ellst[2].innerHTML = 'Tan(β) = Côté opposé (a) / Côté adjacent (b) &nbsp; (<a href="https://fr.wikipedia.org/wiki/Trigonom%C3%A9trie" target="_blank" title="Wikipédia">Trigonométrie</a>)';
				ellst[3].innerHTML = 'Hypoténuse&#178 = a&#178 + b&#178 &nbsp; (<a href="https://fr.wikipedia.org/wiki/Pythagore" target="_blank" title="Wikipédia">Pythagore</a>)';
				for (var i = 0; i < ellstlength; i++) {
					ellst[i].style.fontSize = "1.8vw";
					ellst[i].style.margin = 0;
				}
				
				if (/MSIE 10/i.test(navigator.userAgent) || /MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /Edge\/\d./i.test(navigator.userAgent)) {
					document.getElementsByTagName('html').height = "100%";
					document.getElementsByTagName('body').height = "100%";
					document.getElementById('content').style.height = "100%";
				}		
			}

			// Fonction de conversion deg radian
			Math.radians = function(degrees) {
				return degrees * Math.PI / 180;
			};

			Math.degrees = function(radians) {
				return radians * 180 / Math.PI;
			};

			// Fonction de désactivation de l'affichage des tooltips
			function deactivateTooltips() {
			
				var spans = document.getElementsByTagName('span'),
					spansLength = spans.length;
				
				for (var i = 0 ; i < spansLength ; i++) {
					if (spans[i].className == 'tooltip') {
						spans[i].style.display = 'none';
					}
				}
			}

			// Fonction de récupération Tooltip
			function getTooltip(elements) {
			
				while (elements = elements.nextSibling) {
					if (elements.className === 'tooltip') {
						return elements;
					}
				}
				
				return false;
			}
			
			// fonction apparition des détails de calcule
			function Show(x) {
				var ellst = document.getElementsByClassName(x);
				for (i = 0; i < ellst.length; i++) {
				ellst[i].style.opacity = 1;
				}
			}

			function unShow(x) {
				var ellst = document.getElementsByClassName(x);
				for (i = 0; i < ellst.length; i++) {
				ellst[i].style.opacity = 0;
				}
			}
		
			// Fonction vider placeholder		
			function EmptyPlace() {
				
				var Form = document.getElementById('Form'),
				inputs = document.getElementsByTagName('input'),
				inputsLength = inputs.length;
				
				for (var i = 0 ; i < inputsLength ; i++) {
					if (inputs[i].type == 'number') {
						inputs[i].placeholder = '';
					}
				}		
			};

			// Fonctions de vérification du formulaire
			var check = {};

			check['alpha'] = function() {

				var hyp = document.getElementById('hyp'),
					segb = document.getElementById('segb'),
					sega = document.getElementById('sega'),
					alpha = document.getElementById('alpha'),
					beta = document.getElementById('beta'),
					tooltipStyle = getTooltip(alpha).style,
					hypValue = hyp.value,
					segbValue = segb.value,
					segaValue = sega.value,
					alphaValue = alpha.value,
					betaValue = beta.value;
				
				if (!isNaN(alphaValue) && Number(alphaValue) >= 0 && Number(alphaValue) <= 90 && alphaValue != String("")) {
					// alpha & beta
					result = 90 - Number(alphaValue);
					document.getElementById('beta').placeholder = result;
					// check valeurs remplies
					if (betaValue != result) {
						if (betaValue == String("")) {beta.className = '';}
						else {beta.className = 'incorrect';}
					} else {beta.className = 'correct';}
					// alpha & a
					if (!isNaN(segaValue) && segaValue != "") {				
						segb.className= 'correct';
						// hyp
						radalpha = Math.radians(alphaValue);
						result = segaValue / Math.cos(radalpha);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (hypValue != result) {
							if (hypValue == String("")) {hyp.className = '';}
							else {hyp.className = 'incorrect';}
						} else {hyp.className = 'correct';}
						document.getElementById('hyp').placeholder = result;
						// segb
						result = segaValue * Math.tan(radalpha);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (segbValue != result) {
							if (segbValue == String("")) {segb.className = '';}
							else {segb.className = 'incorrect';}
						} else {segb.className = 'correct';}
						document.getElementById('segb').placeholder = result;
						// Beta
						result = Math.asin(Math.cos(radalpha))
						result = Math.degrees(result);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (betaValue != result) {
							if (betaValue == String("")) {beta.className = '';}
							else {beta.className = 'incorrect';}
						} else {beta.className = 'correct';}
						document.getElementById('beta').placeholder = result;
					}
					// alpha & b
					else if (!isNaN(segbValue) && segbValue != "") {
						segb.className= 'correct';
						// hyp
						radalpha = Math.radians(alphaValue);
						result = segbValue / Math.sin(radalpha);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (hypValue != result) {
							if (hypValue == String("")) {hyp.className = '';}
							else {hyp.className = 'incorrect';}
						} else {hyp.className = 'correct';}
						document.getElementById('hyp').placeholder = result;
						// sega
						result = segbValue / Math.tan(radalpha);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (segaValue != result) {
							if (segaValue == String("")) {sega.className = '';}
							else {sega.className = 'incorrect';}
						} else {sega.className = 'correct';}
						document.getElementById('sega').placeholder = result;
						// Beta
						result = Math.asin(Math.cos(radalpha))
						result = Math.degrees(result);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (betaValue != result) {
							if (betaValue == String("")) {beta.className = '';}
							else {beta.className = 'incorrect';}
						} else {beta.className = 'correct';}
						document.getElementById('beta').placeholder = result;
					}
					// alpha & hyp
					else if (!isNaN(hypValue) && hypValue != "") {
						hyp.className= 'correct';
						// Calcule de segb
						radalpha = Math.radians(alphaValue);
						result = Math.cos(radalpha) * hypValue;
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (segbValue != result) {
							if (segbValue == String("")) {segb.className = '';}
							else {segb.className = 'incorrect';}
						} else {segb.className = 'correct';}
						document.getElementById('segb').placeholder = result;
						// Calcule de sega
						result = Math.sin(radalpha) * hypValue;
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (segaValue != result) {
							if (segaValue == String("")) {sega.className = '';}
							else {sega.className = 'incorrect';}
						} else {sega.className = 'correct';}
						document.getElementById('sega').placeholder = result;
						// Calcule de Beta
						result = Math.asin(Math.cos(radalpha))
						result = Math.degrees(result);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (betaValue != result) {
							if (betaValue == String("")) {beta.className = '';}
							else {beta.className = 'incorrect';}
						} else {beta.className = 'correct';}
						document.getElementById('beta').placeholder = result;
					}
					alpha.className = 'correct';
					tooltipStyle.display = 'none';
				} else if (alphaValue == String("")) {
					alpha.className = '';
					tooltipStyle.display = 'none';
					EmptyPlace();
					inputs = document.getElementsByTagName('input'),
					inputsLength = inputs.length;
					for(var i = 0; i < inputsLength; i++) {
						if (inputs[i].value != String("")) {
							id = inputs[i].id;
							check[id]();
							break;
						}
					}
				} else {
					alpha.className = 'incorrect';
					tooltipStyle.display = 'inline-block';
				}

			};
			
			check['beta'] = function() {

				var hyp = document.getElementById('hyp'),
					segb = document.getElementById('segb'),
					sega = document.getElementById('sega'),
					alpha = document.getElementById('alpha'),
					beta = document.getElementById('beta'),
					tooltipStyle = getTooltip(beta).style,
					hypValue = hyp.value,
					segbValue = segb.value,
					segaValue = sega.value,
					alphaValue = alpha.value,
					betaValue = beta.value;
				
				if (!isNaN(betaValue) && Number(betaValue) >= 0 && Number(betaValue) <= 90 && betaValue != String("")) {
					// beta & alpha
					result = 90 - Number(betaValue);
					document.getElementById('alpha').placeholder = result;
					// check valeurs remplies
					if (alphaValue != result) {
						if (alphaValue == String("")) {alpha.className = '';}
						else {alpha.className = 'incorrect';}
					} else {alpha.className = 'correct';}
					// Beta & a
					if (!isNaN(segaValue) && segaValue != "") {
						segb.className= 'correct';
						// hyp
						radbeta = Math.radians(betaValue);
						result = segaValue / Math.sin(radbeta);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (hypValue != result) {
							if (hypValue == String("")) {hyp.className = '';}
							else {hyp.className = 'incorrect';}
						} else {hyp.className = 'correct';}
						document.getElementById('hyp').placeholder = result;
						// b
						result = segaValue / Math.tan(radbeta);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (segbValue != result) {
							if (segbValue == String("")) {segb.className = '';}
							else {segb.className = 'incorrect';}
						} else {segb.className = 'correct';}
						document.getElementById('segb').placeholder = result;
						// Calcule de alpha
						result = Math.asin(Math.cos(radbeta))
						result = Math.degrees(result);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (alphaValue != result) {
							if (alphaValue == String("")) {alpha.className = '';}
							else {alpha.className = 'incorrect';}
						} else {alpha.className = 'correct';}
						document.getElementById('alpha').placeholder = result;
					}
					// Beta & b
					else if (!isNaN(segbValue) && segbValue != "") {
						sega.className= 'correct';
						// hyp
						radbeta = Math.radians(betaValue);
						result = segbValue / Math.cos(radbeta);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (hypValue != result) {
							if (hypValue == String("")) {hyp.className = '';}
							else {hyp.className = 'incorrect';}
						} else {hyp.className = 'correct';}
						document.getElementById('hyp').placeholder = result;
						// sega
						result =  segbValue * Math.tan(radbeta);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (segaValue != result) {
							if (segaValue == String("")) {sega.className = '';}
							else {sega.className = 'incorrect';}
						} else {sega.className = 'correct';}
						document.getElementById('sega').placeholder = result;
						// alpha
						result = Math.asin(Math.cos(radbeta))
						result = Math.degrees(result);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (alphaValue != result) {
							if (alphaValue == String("")) {alpha.className = '';}
							else {alpha.className = 'incorrect';}
						} else {alpha.className = 'correct';}
						document.getElementById('alpha').placeholder = result;
					}
					// Beta & hyp
					else if (!isNaN(hypValue) && hypValue != "") {
						hyp.className = 'correct';
						// a
						radbeta = Math.radians(betaValue);
						result = Math.cos(radbeta) * hypValue;
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (segbValue != result) {
							if (segbValue == String("")) {segb.className = '';}
							else {segb.className = 'incorrect';}
						} else {segb.className = 'correct';}
						document.getElementById('segb').placeholder = result;
						// b
						result = Math.sin(radbeta) * hypValue;
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (segaValue != result) {
							if (segaValue == String("")) {sega.className = '';}
							else {sega.className = 'incorrect';}
						} else {sega.className = 'correct';}
						document.getElementById('sega').placeholder = result;
						// alpha
						result = Math.asin(Math.cos(radbeta))
						result = Math.degrees(result);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (alphaValue != result) {
							if (alphaValue == String("")) {alpha.className = '';}
							else {alpha.className = 'incorrect';}
						} else {alpha.className = 'correct';}
						document.getElementById('alpha').placeholder = result;
					}			
					beta.className = 'correct';
					tooltipStyle.display = 'none';
				} else if (betaValue == String("")) {
					beta.className = '';
					tooltipStyle.display = 'none';
					EmptyPlace();
					inputs = document.getElementsByTagName('input'),
					inputsLength = inputs.length;
					for(var i = 0; i < inputsLength; i++) {
						if (inputs[i].value != String("")) {
							id = inputs[i].id;
							check[id]();
							break;
						}
					}
				} else {
					beta.className = 'incorrect';
					tooltipStyle.display = 'inline-block';
				}

			};
		
			check['sega'] = function() {
				
				var hyp = document.getElementById('hyp'),
					segb = document.getElementById('segb'),
					sega = document.getElementById('sega'),
					alpha = document.getElementById('alpha'),
					beta = document.getElementById('beta'),
					tooltipStyle = getTooltip(sega).style,
					hypValue = hyp.value,
					segbValue = segb.value,
					segaValue = sega.value,
					alphaValue = alpha.value,
					betaValue = beta.value;
				
				if (!isNaN(segaValue) && Number(segaValue) >= 0 && segaValue != String("")) {
					// a & b
					if (!isNaN(segbValue) && segbValue != "") {
						segb.className= 'correct';
						// Alpha
						result = Math.atan(segbValue / segaValue);
						result = Math.degrees(result);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (alphaValue != result) {
							if (alphaValue == String("")) {alpha.className = '';}
							else {alpha.className = 'incorrect';}
						} else {alpha.className = 'correct';}
						document.getElementById('alpha').placeholder = result;
						// Beta
						result = Math.atan(segaValue / segbValue);
						result = Math.degrees(result);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (betaValue != result) {
							if (betaValue == String("")) {beta.className = '';}
							else {beta.className = 'incorrect';}
						} else {beta.className = 'correct';}
						document.getElementById('beta').placeholder = result;
						// hypothénuse
						result = Math.sqrt(Math.pow(segbValue,2)+Math.pow(segaValue,2));
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (hypValue != result) {
							if (hypValue == String("")) {hyp.className = '';}
							else {hyp.className = 'incorrect';}
						} else {hyp.className = 'correct';}
						document.getElementById('hyp').placeholder = result;
					}
					// a & hyp
					else if (!isNaN(hypValue) && hypValue != "" && Number(segbValue) <= Number(hypValue)) {
						hyp.className= 'correct';
						// Alpha
						result = Math.acos(segaValue / hypValue);
						result = Math.degrees(result);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (alphaValue != result) {
							if (alphaValue == String("")) {alpha.className = '';}
							else {alpha.className = 'incorrect';}
						} else {alpha.className = 'correct';}
						document.getElementById('alpha').placeholder = result;
						// Beta
						result = Math.asin(segaValue / hypValue);
						result = Math.degrees(result);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (betaValue != result) {
							if (betaValue == String("")) {beta.className = '';}
							else {beta.className = 'incorrect';}
						} else {beta.className = 'correct';}
						document.getElementById('beta').placeholder = result;
						// a
						result = Math.sqrt(Math.pow(hypValue,2)-Math.pow(segaValue,2));
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (segbValue != result) {
							if (segbValue == String("")) {segb.className = '';}
							else {segb.className = 'incorrect';}
						} else {segb.className = 'correct';}
						document.getElementById('segb').placeholder = result;
					}
					// a & alpha
					else if (!isNaN(alphaValue) && alphaValue != "") {
						alpha.className= 'correct';
						// hyp
						radalpha = Math.radians(alphaValue);
						result = segaValue/Math.cos(radalpha);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (hypValue != result) {
							if (hypValue == String("")) {hyp.className = '';}
							else {hyp.className = 'incorrect';}
						} else {hyp.className = 'correct';}
						document.getElementById('hyp').placeholder = result;
						// segb
						result = segaValue * Math.tan(radalpha);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (segbValue != result) {
							if (segbValue == String("")) {segb.className = '';}
							else {segb.className = 'incorrect';}
						} else {segb.className = 'correct';}
						document.getElementById('segb').placeholder = result;
						// Beta
						result = Math.asin(Math.cos(radalpha))
						result = Math.degrees(result);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (betaValue != result) {
							if (betaValue == String("")) {beta.className = '';}
							else {beta.className = 'incorrect';}
						} else {beta.className = 'correct';}
						document.getElementById('beta').placeholder = result;
					}
					// a & beta
					else if (!isNaN(betaValue) && betaValue != "") {
						beta.className= 'correct';
						// hyp
						radbeta = Math.radians(betaValue);
						result = segaValue / Math.sin(radbeta);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (hypValue != result) {
							if (hypValue == String("")) {hyp.className = '';}
							else {hyp.className = 'incorrect';}
						} else {hyp.className = 'correct';}
						document.getElementById('hyp').placeholder = result;
						// segb
						result =  segaValue / Math.tan(radbeta);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (segbValue != result) {
							if (segbValue == String("")) {segb.className = '';}
							else {segb.className = 'incorrect';}
						} else {segb.className = 'correct';}
						document.getElementById('segb').placeholder = result;
						// alpha
						result = Math.asin(Math.cos(radbeta))
						result = Math.degrees(result);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (alphaValue != result) {
							if (alphaValue == String("")) {alpha.className = '';}
							else {alpha.className = 'incorrect';}
						} else {alpha.className = 'correct';}
						document.getElementById('alpha').placeholder = result;
					}
					sega.className = 'correct';
					tooltipStyle.display = 'none';
					if (Number(segaValue) > Number(hypValue) && hypValue != String("")) {
						hyp.className = 'incorrect';
						EmptyPlace();
					}
				} else if (segaValue == String("")) {
					sega.className = '';
					tooltipStyle.display = 'none';
					EmptyPlace();
					inputs = document.getElementsByTagName('input'),
					inputsLength = inputs.length;
					for(var i = 0; i < inputsLength; i++) {
						if (inputs[i].value != String("")) {
							id = inputs[i].id;
							check[id]();
							break;
						}
					}
				}
				else {
					sega.className = 'incorrect';
					tooltipStyle.display = 'inline-block';

				}

			};
		
			check['segb'] = function() {

				var hyp = document.getElementById('hyp'),
					segb = document.getElementById('segb'),
					sega = document.getElementById('sega'),
					alpha = document.getElementById('alpha'),
					beta = document.getElementById('beta'),
					tooltipStyle = getTooltip(segb).style,
					hypValue = hyp.value,
					segbValue = segb.value,
					segaValue = sega.value,
					alphaValue = alpha.value,
					betaValue = beta.value;
				
				if (!isNaN(segbValue) && Number(segbValue) >= 0 && segbValue != String("")) {
					// b & a
					if (!isNaN(segaValue) && segaValue != "") {
						sega.className= 'correct';
						// alpha
						result = Math.atan(segbValue / segaValue);
						result = Math.degrees(result);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (alphaValue != result) {
							if (alphaValue == String("")) {alpha.className = '';}
							else {alpha.className = 'incorrect';}
						} else {alpha.className = 'correct';}
						document.getElementById('alpha').placeholder = result;
						// Beta
						result = Math.atan(segaValue / segbValue);
						result = Math.degrees(result);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (betaValue != result) {
							if (betaValue == String("")) {beta.className = '';}
							else {beta.className = 'incorrect';}
						} else {beta.className = 'correct';}
						document.getElementById('beta').placeholder = result;
						// hyp
						result = Math.sqrt(Math.pow(segbValue,2)+Math.pow(segaValue,2));
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (hypValue != result) {
							if (hypValue == String("")) {hyp.className = '';}
							else {hyp.className = 'incorrect';}
						} else {hyp.className = 'correct';}
						document.getElementById('hyp').placeholder = result;
					}
					// b & hyp
					else if (!isNaN(hypValue) && hypValue != "" && Number(segbValue) <= Number(hypValue)) {
						hyp.className= 'correct';
						// Alpha
						result = Math.asin(segbValue / hypValue);
						result = Math.degrees(result);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (alphaValue != result) {
							if (alphaValue == String("")) {alpha.className = '';}
							else {alpha.className = 'incorrect';}
						} else {alpha.className = 'correct';}
						document.getElementById('alpha').placeholder = result;
						// Beta
						result = Math.acos(segbValue / hypValue);
						result = Math.degrees(result);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (betaValue != result) {
							if (betaValue == String("")) {beta.className = '';}
							else {beta.className = 'incorrect';}
						} else {beta.className = 'correct';}
						document.getElementById('beta').placeholder = result;
						// b
						result = Math.sqrt(Math.pow(hypValue,2)-Math.pow(segbValue,2));
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (segaValue != result) {
							if (segaValue == String("")) {sega.className = '';}
							else {sega.className = 'incorrect';}
						} else {sega.className = 'correct';}
						document.getElementById('sega').placeholder = result;
					}
					// b & alpha
					else if (!isNaN(alphaValue) && alphaValue != "") {
						alpha.className= 'correct';
						// hyp
						radalpha = Math.radians(alphaValue);
						result = segbValue/Math.sin(radalpha);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (hypValue != result) {
							if (hypValue == String("")) {hyp.className = '';}
							else {hyp.className = 'incorrect';}
						} else {hyp.className = 'correct';}
						document.getElementById('hyp').placeholder = result;
						// Calcule de sega
						result = segbValue / Math.tan(radalpha);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (segaValue != result) {
							if (segaValue == String("")) {sega.className = '';}
							else {sega.className = 'incorrect';}
						} else {sega.className = 'correct';}
						document.getElementById('sega').placeholder = result;
						// Calcule de Beta
						result = Math.asin(Math.cos(radalpha))
						result = Math.degrees(result);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (betaValue != result) {
							if (betaValue == String("")) {beta.className = '';}
							else {beta.className = 'incorrect';}
						} else {beta.className = 'correct';}
						document.getElementById('beta').placeholder = result;
					}
					// b & beta
					else if (!isNaN(betaValue) && betaValue != "") {
						beta.className= 'correct';
						// Calcule de l'hyp
						radbeta = Math.radians(betaValue);
						result = segbValue/Math.cos(radbeta);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (hypValue != result) {
							if (hypValue == String("")) {hyp.className = '';}
							else {hyp.className = 'incorrect';}
						} else {hyp.className = 'correct';}
						document.getElementById('hyp').placeholder = result;
						// Calcule de sega
						result = Math.tan(radbeta) * segbValue;
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (segaValue != result) {
							if (segaValue == String("")) {sega.className = '';}
							else {sega.className = 'incorrect';}
						} else {sega.className = 'correct';}
						document.getElementById('sega').placeholder = result;
						// Calcule de alpha
						result = Math.asin(Math.cos(radbeta))
						result = Math.degrees(result);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (alphaValue != result) {
							if (alphaValue == String("")) {alpha.className = '';}
							else {alpha.className = 'incorrect';}
						} else {alpha.className = 'correct';}
						document.getElementById('alpha').placeholder = result;
					}
					segb.className = 'correct';
					tooltipStyle.display = 'none';
					if (Number(segbValue) > Number(hypValue) && hypValue != String("")) {
						hyp.className = 'incorrect';
						EmptyPlace();
					}
				} else if (segbValue == String("")) {
					segb.className = '';
					tooltipStyle.display = 'none';
					EmptyPlace();
					inputs = document.getElementsByTagName('input'),
					inputsLength = inputs.length;
					for(var i = 0; i < inputsLength; i++) {
						if (inputs[i].value != String("")) {
							id = inputs[i].id;
							check[id]();
							break;
						}
					}
				} else {
					segb.className = 'incorrect';
					tooltipStyle.display = 'inline-block';
				}
			};
		
			check['hyp'] = function() {

				var hyp = document.getElementById('hyp'),
					segb = document.getElementById('segb'),
					sega = document.getElementById('sega'),
					alpha = document.getElementById('alpha'),
					beta = document.getElementById('beta'),
					tooltipStyle = getTooltip(hyp).style,
					hypValue = hyp.value,
					segbValue = segb.value,
					segaValue = sega.value,
					alphaValue = alpha.value,
					betaValue = beta.value;
				
				if (!isNaN(hypValue) && hypValue >= 0 && Number(hypValue) >= Number(segbValue) && Number(hypValue) >= Number(segaValue) && hypValue != String("")) {
					if (!isNaN(segbValue) && segbValue != "" && Number(segbValue) <= Number(hypValue)) {
						segb.className= 'correct';
						// Calcule de Alpha
						result = Math.asin(segbValue / hypValue);
						result = Math.degrees(result);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (alphaValue != result) {
							if (alphaValue == String("")) {alpha.className = '';}
							else {alpha.className = 'incorrect';}
						} else {alpha.className = 'correct';}
						document.getElementById('alpha').placeholder = result;
						// Calcule de Beta
						result = Math.acos(segbValue / hypValue);
						result = Math.degrees(result);
						result = Math.round(result*100)/100;
						// Check valeur remplie
						if (betaValue != result) {
							if (betaValue == String("")) {beta.className = '';}
							else {beta.className = 'incorrect';}
						} else {beta.className = 'correct';}
						document.getElementById('beta').placeholder = result;
						// Caclule Pythagore de sega
						result = Math.sqrt(Math.pow(hypValue,2)-Math.pow(segbValue,2));
						result = Math.round(result*100)/100;
						// Check valeur remplie
						if (segaValue != result) {
							if (segaValue == String("")) {sega.classname = '';}
							else {sega.className = 'incorrect';}
						} else {sega.className = 'correct';}
						document.getElementById('sega').placeholder = result;
					}
					// hyp & a
					else if (!isNaN(segaValue) && segaValue != "" && Number(segaValue) <= Number(hypValue)) {
						sega.className= 'correct';
						// alpha
						result = Math.acos(segaValue / hypValue);
						result = Math.degrees(result);
						result = Math.round(result*100)/100;
						// Check valeur remplie
						if (alphaValue != result) {
							if (alphaValue == String("")) {alpha.className = '';}
							else {alpha.className = 'incorrect';}
						} else {alpha.className = 'correct';}
						document.getElementById('alpha').placeholder = result;
						// Beta
						result = Math.asin(segaValue / hypValue);
						result = Math.degrees(result);
						result = Math.round(result*100)/100;
						// Check valeur remplie
						if (betaValue != result) {
							if (betaValue == String("")) {beta.className = '';}
							else {beta.className = 'incorrect';}
						} else {beta.className = 'correct';}
						document.getElementById('beta').placeholder = result;
						// sega
						result = Math.sqrt(Math.pow(hypValue,2)-Math.pow(segaValue,2));
						result = Math.round(result*100)/100;
						// Check valeur remplie
						if (segbValue != result) {
							if (segbValue == String("")) {segb.classname = '';}
							else {segb.className = 'incorrect';}
						} else {segb.className = 'correct';}
						document.getElementById('segb').placeholder = result;
					}
					// Hyp & alpha
					else if (!isNaN(alphaValue) && alphaValue != "") {
						alpha.className= 'correct';
						// segb
						radalpha = Math.radians(alphaValue);
						result = Math.cos(radalpha) * hypValue;
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (segbValue != result) {
							if (segbValue == String("")) {segb.className = '';}
							else {segb.className = 'incorrect';}
						} else {segb.className = 'correct';}
						document.getElementById('segb').placeholder = result;
						// sega
						result = Math.sin(radalpha) * hypValue;
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (segaValue != result) {
							if (segaValue == String("")) {sega.className = '';}
							else {sega.className = 'incorrect';}
						} else {sega.className = 'correct';}
						document.getElementById('sega').placeholder = result;
						// Beta
						result = Math.asin(Math.cos(radalpha))
						result = Math.degrees(result);
						result = Math.round(result*100)/100;
						// check valeurs remplies
						if (Number(betaValue) != Number(result)) {
							if (betaValue == String("")) {beta.className = '';}
							else {beta.className = 'incorrect';}
						} else {beta.className = 'correct';}
						document.getElementById('beta').placeholder = result;
					}
					// hyp & beta
					else if (!isNaN(betaValue) && betaValue != "") {
						beta.className= 'correct';
						// segb
						radbeta = Math.radians(betaValue);
						result = Math.cos(radbeta) * hypValue;
						// check valeurs remplies
						if (segbValue != result) {
							if (segbValue == String("")) {segb.className = '';}
							else {segb.className = 'incorrect';}
						} else {segb.className = 'correct';}
						result = Math.round(result*100)/100;
						document.getElementById('segb').placeholder = result;
						// sega
						result = Math.sin(radbeta) * hypValue;
						// check valeurs remplies
						if (segaValue != result) {
							if (segaValue == String("")) {sega.className = '';}
							else {sega.className = 'incorrect';}
						} else {sega.className = 'correct';}
						result = Math.round(result*100)/100;
						document.getElementById('sega').placeholder = result;
						// alpha
						result = Math.asin(Math.cos(radbeta))
						result = Math.degrees(result);
						// check valeurs remplies
						if (Number(alphaValue) != result) {
							if (alphaValue == String("")) {alpha.className = '';}
							else {alpha.className = 'incorrect';}
						} else {alpha.className = 'correct';}
						result = Math.round(result*100)/100;
						document.getElementById('alpha').placeholder = result;
					}
					hyp.className = 'correct';
					tooltipStyle.display = 'none';
				} else if (hypValue == String("")) {
					hyp.className = '';
					tooltipStyle.display = 'none';
					EmptyPlace();
					inputs = document.getElementsByTagName('input'),
					inputsLength = inputs.length;
					for(var i = 0; i < inputsLength; i++) {
						if (inputs[i].value != String("")) {
							id = inputs[i].id;
							check[id]();
							break;
						}
					}
				} else {
					hyp.className = 'incorrect';
					tooltipStyle.display = 'inline-block';
				}
			};

			// Mise en place des événements
			(function() {
			
				var Form = document.getElementById('Form'),
					inputs = document.getElementsByTagName('input'),
					inputsLength = inputs.length;
			
				for (var i = 0 ; i < inputsLength ; i++) {
					if (inputs[i].type == 'number') {
			
						inputs[i].addEventListener('keyup', function(e) {
							check[e.target.id](e.target.id);
						}, false);
			
					}
				}
			
				Form.addEventListener('reset', function() {
			
					for (var i = 0 ; i < inputsLength ; i++) {
						if (inputs[i].type == 'number') {
							inputs[i].className = '';
							inputs[i].placeholder = '';
						}
					}
			
					deactivateTooltips();
			
				}, false);
			})();

			// Désactiver tooltips
			deactivateTooltips();

    	</script>

		<div id="footer">
			<p>
				Faire un don <strong><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=593KJEYR3GQ2S" title="PayPal">Paypal</a></strong> - 
				<a href="https://framagit.org/Frederiiic/Pythagone">Code Source</a>	GPLv3 <img id="copyleft" src="img/Copyleft.svg" alt="Copyleft"> <a href="https://www.fredericpavageau.net">Frederic Pavageau</a> 2025.
			</p>
		</div>

	</body>
</html>