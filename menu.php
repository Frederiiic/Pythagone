﻿<div class="header">
			<img id="logo" src="img/Pythagone.svg" alt="logo" height="104" width="104"/>
			<div class="title">Pytha<span id="orange">g</span>one</div>
</div>
<div>
    <ul>
        <li id="trigo">
            <a href="trigo.php"><span><h3>Triangle Rectangle</h3></span><br><h4>Trigonométrie / Pythagore</h4></a>
        </li>
        <li id="trigogen">
            <a href="trigogen.php"><span><h3>Triangle Quelconque</h3></span><br><h4>Trigonométrie / Al-Kachi</h4></a>
        </li>
        <li id="contreventement">
            <a href="contreventement.php"><span><h3>Contreventement / Diagonale</h3></span><br><h4>Trigonométrie / Pythagore / Euclide</h4></a>
        </li>
        <li  id="pyramide">
            <a href="pyramide.php"><span><h3>Pyramide Régulière</h3></span><br><h4>Trigonométrie / Pythagore / Euclide</h4></a>
        </li>
        <li  id="corroyage">
            <a href="corroyage.php"><span><h3>Arêtier / Corroyage</h3></span><br><h4>Calcule d'angles de corroyage avec deux pentes</h4></a>
        </li>
    </ul>
</div>
<div id="menufooter">
    <p>
        Faire un don <strong><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=593KJEYR3GQ2S" title="PayPal">Paypal</a></strong>.<br>
        Faire un don en <strong>Bitcoins</strong> : 1ALYQEemhofFAoM43TYkxXB4Wt46ep4Wg6<br>
        <a href="https://framagit.org/Frederiiic/Pythagone">Code Source</a> GPLv3 <img id="copyleft" src="img/Copyleft.svg" alt="Copyleft"> <a href="https://www.fredericpavageau.net">Frederic Pavageau</a> 2025.<br>
    </p>
    <div id="compteur">
        <span><strong>
        <?php
        session_start();
        if(file_exists('cptindex.txt'))
        {
                $compteur_f = fopen('cptindex.txt', 'r+');
                $compte = fgets($compteur_f);
        }
        else
        {
                $compteur_f = fopen('cptindex.txt', 'a+');
                $compte = 1;
        }
        if(!isset($_SESSION['cptindex']))
        {
                $_SESSION['cptindex'] = 'visite';
                $compte++;
                fseek($compteur_f, 0);
                fputs($compteur_f, $compte);
        }
        fclose($compteur_f);
        echo $compte;
        ?>
        </strong></br>visites</span>
    </div>
</div>