<!DOCTYPE html>
<html>
	<head>
        <meta charset="utf-8" />
        <title>Pythagone : Pyramide régulière</title>
        <meta name="viewport" content="width=800, user-scalable=no" />
        <meta name="description" content="Pyramide : Calcule des cotés et angles de corroyage nécessaires pour la construction d'une pyramide régulière selon le nombre de face, sa hauteur et le rayon de sa base, par Frédéric Pavageau." />
        <meta name="keywords" content="frédéric Pavageau, Angle de corroyage, Corroyage, Pyramide, Construiction, Construire, Menuiserie, Pythagore, Euclide, Trigonométrie, Cosinus, Sinus, Tangeante, angle, angle de corroyage, diagonales, triangle, plan, calcule, géométrie, tracé, Trait, Trait d'ébéniste, Trait de charpente, Charpente" />
        <meta property="og:site_name" content="Pythagone" /> 
        <meta property="og:title" content="Pythagone : Pyramide" />
        <meta property="og:type" content="website" /> 
        <meta property="og:url" content="https://www.pythagone.net/pyramide.php" />
        <meta property="og:description" content="Calcule des cotés triangles et angles de coupes nécessaires pour la construction d'une pyramide régulière selon le nombre de face, sa hauteur et le rayon de sa base par Frédéric Pavageau." />
        <meta property="og:image" content="https://pythagone.fredericpavageau.net/img/Pythagone.svg" />
        <meta name="twitter:card" content="summary_large_image" />
        <link rel="stylesheet" href="style.css" />
		<link rel="stylesheet" href="pyramide.css" />
		<link rel="icon" type="image/svg+xml" href="img/Pythagone.svg" sizes="any"/>
		<link rel="icon" type="image/png" href="img/16-flavico.png" sizes="16x16"/>
		<link rel="icon" type="image/png" href="img/32-flavico.png" sizes="32x32"/>
		<link rel="icon" type="image/png" href="img/64-flavico.png" sizes="64x64"/>
		<link rel="icon" type="image/png" href="img/96-flavico.png" sizes="96x96"/>
		<link rel="icon" type="image/png" href="img/256-flavico.png" sizes="256x256"/>
	</head>

	<body>

		<h1>Pythagone : Pyramide régulière</h1>
		<h2>Pyramide : Calcule des cotés et angles de corroyage nécessaires pour la construction d'une pyramide régulière selon le nombre de face, sa hauteur et le rayon de sa base, par Frédéric Pavageau.</h2>
  
		<div id="backdrop"></div>
  
		<div id="sidenav">
			<?php require "menu.php"; ?>
			<script>
				mdf = document.getElementById("pyramide");
    			mdf.className = "active";
			</script>
    	</div>
  
		<div id="content">
		
			<header>
				<div id="menu-toggle">
					<img id="menu" src="img/Menu.svg" alt="Bouton d'ouverture du menu" />
				</div>
			</header>

			<div id="formula">
				<div class="uform" onmouseover="Show('OAB');Show('PointO');Show('PointA');Show('PointB');" onmouseout="unShow('OAB');unShow('PointO');unShow('PointA');unShow('PointB');">
					<math xmlns="http://www.w3.org/1998/Math/MathML">
						<mrow>
							<mi>AB</mi>
							<mo>=</mo>
							<msqrt>
								<msup>
									<mi>OA</mi>
									<mn>2</mn>
								</msup>
								<mo>+</mo>
								<msup>
									<mi>OB</mi>
									<mn>2</mn>
								</msup>
							</msqrt>
						</mrow>
					</math>
					&nbsp;(<a href="https://fr.wikipedia.org/wiki/Pythagore" target="_blank" class="wikilink" title="Wikipédia">Pythagore</a>)
				</div>
				<div class="uform" onmouseover="Show('OBC');Show('PointO');Show('PointB');Show('PointC');" onmouseout="unShow('OBC');unShow('PointO');unShow('PointB');unShow('PointC');">
					<math xmlns="http://www.w3.org/1998/Math/MathML">
						<mrow>
							<mi>CB</mi>
							<mo>=</mo>
							<mi>Sin</mi>
							<mfenced>
								<mfrac>
									<mrow>
										<mi>&nbsp;&nbsp;360&nbsp;</mi>
									</mrow>
									<mrow>
										<mi id="Mathnb">&nbsp;x&nbsp;&nbsp;</mi>
									</mrow>
								</mfrac>
							</mfenced>
							<mo>x</mo>
							<mi>OB</mi>
						</mrow>
					</math>
					&nbsp;(<a href="https://fr.wikipedia.org/wiki/Trigonom%C3%A9trie" target="_blank" class="wikilink" title="Wikipédia">Trigonométrie</a>)
				</div>
				<div class="uform" onmouseover="Show('ABC');Show('PointA');Show('PointB');Show('PointC');" onmouseout="unShow('ABC');unShow('PointA');unShow('PointB');unShow('PointC');">
					<math xmlns="http://www.w3.org/1998/Math/MathML">
						<mrow>
							<mover accent="true">
								<mi>ABC</mi>
								<mo> &#x2054; </mo>
							</mover>
							<mo>=</mo>
							<msup>
								<mi>Cos</mi>
								<mn>-1</mn>
							</msup>
							<mfenced>
								<mfrac>
									<mrow>
										<mi>&nbsp;&nbsp;CB&nbsp;</mi>
									</mrow>
									<mrow>
										<mi>&nbsp;2</mi>
										<mo>x</mo>
										<mi>AB&nbsp;&nbsp;</mi>
									</mrow>
								</mfrac>
							</mfenced>
						</mrow>
					</math>
					&nbsp;(<a href="https://fr.wikipedia.org/wiki/Trigonom%C3%A9trie" target="_blank" class="wikilink" title="Wikipédia">Trigonométrie</a>)
				</div>
				<div class="uform" onmouseover="Show('CEB');Show('PointC');Show('PointE');Show('PointB');" onmouseout="unShow('CEB');unShow('PointC');unShow('PointE');unShow('PointB');">
					<math xmlns="http://www.w3.org/1998/Math/MathML">
						<mrow>
							<mi>CE</mi>
							<mo>=</mo>
							<mi>Sin</mi>
							<mfenced>
								<mover accent="true">
									<mi>ABC</mi>
									<mo> &#x2054; </mo>
								</mover>
							</mfenced>
							<mo>x</mo>
							<mi>CB</mi>
						</mrow>
					</math>
					&nbsp;(<a href="https://fr.wikipedia.org/wiki/Trigonom%C3%A9trie" target="_blank" class="wikilink" title="Wikipédia">Trigonométrie</a>)
				</div>
				<div class="uform" onmouseover="Show('OBC');Show('PointO');Show('PointB');Show('PointC');" onmouseout="unShow('OBC');unShow('PointO');unShow('PointB');unShow('PointC');">
					<math xmlns="http://www.w3.org/1998/Math/MathML">
						<mrow>
							<mover accent="true">
								<mi>OBC</mi>
								<mo> &#x2054; </mo>
							</mover>
							<mo>=</mo>
								<mfrac>
									<mrow>
										<mi>180</mi>
										<mo>-</mo>
										<mfenced>
											<mfrac>
												<mrow>
													<mi>&nbsp;&nbsp;360&nbsp;</mi>
												</mrow>
												<mrow>
													<mi id="Mathnb">&nbsp;x&nbsp;&nbsp;</mi>
												</mrow>
											</mfrac>
										</mfenced>
									</mrow>
									<mrow>
										<mi>&nbsp;2&nbsp;&nbsp;</mi>
									</mrow>
								</mfrac>
						</mrow>
					</math>
					&nbsp;(<a href="https://fr.wikipedia.org/wiki/Euclide" target="_blank" class="wikilink" title="Wikipédia">Euclide</a>)
				</div>
				<div class="uform" onmouseover="Show('CFB');Show('PointC');Show('PointF');Show('PointB');" onmouseout="unShow('CFB');unShow('PointC');unShow('PointF');unShow('PointB');">
					<math xmlns="http://www.w3.org/1998/Math/MathML">
						<mrow>
							<mi>CF</mi>
							<mo>=</mo>
							<mi>Sin</mi>
							<mfenced>
								<mover accent="true">
									<mi>OBC</mi>
									<mo> &#x2054; </mo>
								</mover>
							</mfenced>
							<mo>x</mo>
							<mi>CB</mi>
						</mrow>
					</math>
					&nbsp;(<a href="https://fr.wikipedia.org/wiki/Trigonom%C3%A9trie" target="_blank" class="wikilink" title="Wikipédia">Trigonométrie</a>)
				</div>
				<div class="uform" onmouseover="Show('FEC');Show('PointF');Show('PointE');Show('PointC');" onmouseout="unShow('FEC');unShow('PointF');unShow('PointE');unShow('PointC');">
					<math xmlns="http://www.w3.org/1998/Math/MathML">
						<mrow>
							<mover accent="true">
								<mi>FEC</mi>
								<mo> &#x2054; </mo>
							</mover>
							<mo>=</mo>
							<mi>Sin</mi>
							<mfenced>
								<mfrac>
									<mrow>
										<mi>&nbsp;&nbsp;CF&nbsp;</mi>
									</mrow>
									<mrow>
										<mi>&nbsp;CE&nbsp;&nbsp;</mi>
									</mrow>
								</mfrac>
							</mfenced>
						</mrow>
					</math>
					&nbsp;(<a href="https://fr.wikipedia.org/wiki/Trigonom%C3%A9trie" target="_blank" class="wikilink" title="Wikipédia">Trigonométrie</a>)
				</div>
			</div>
		
			<svg id="fond" viewBox="0 0 200 100" xml:lang="fr"
				xmlns="http://www.w3.org/2000/svg"
				xmlns:xlink="http://www.w3.org/1999/xlink">
				<title>Pyramide</title>
				<circle id="PointOrtho" stroke="transparent" fill="#036" r="0.5"/>
				<polyline id="svgrPointe" class="rPointe" stroke="#4b4" fill="transparent" stroke-width="0.5" opacity="0"/>
				<polyline id="svgrBase" class="rBase" stroke="#4b4" fill="transparent" stroke-width="0.5" opacity="0"/>
				<polyline id="svgrAreteSom" class="rAreteSom" stroke="#4b4" fill="transparent" stroke-width="0.5" opacity="0"/>
				<polygon id="svgrAAreteSom" class="rAreteSom" stroke="transparent" fill="rgba(68, 191, 68, 0.75)" stroke-width="0.5" opacity="0"/>
				<text id="PointA" class="PointA" fill="#3a3" opacity="0">A</text>
				<text id="PointB" class="PointB" fill="#3a3" opacity="0">B</text>
				<text id="PointC" class="PointC" fill="#3a3" opacity="0">C</text>
				<text id="PointE" class="PointE" fill="#3a3" opacity="0">E</text>
				<text id="PointF" class="PointF" fill="#3a3" opacity="0">F</text>
				<text id="PointO" class="PointO" fill="#3a3" opacity="0">O</text>
				<polygon id="OAB" class="OAB" stroke="#4b4" fill="rgba(68, 191, 68, 0.5)" stroke-width="0.5" opacity="0"/>
				<polygon id="OBC" class="OBC" stroke="#4b4" fill="rgba(68, 191, 68, 0.5)" stroke-width="0.5" opacity="0"/>
				<polygon id="ABC" class="ABC" stroke="#4b4" fill="rgba(68, 191, 68, 0.5)" stroke-width="0.5" opacity="0"/>
				<polygon id="CEB" class="CEB" stroke="#4b4" fill="rgba(68, 191, 68, 0.5)" stroke-width="0.5" opacity="0"/>
				<polygon id="CFB" class="CFB" stroke="#4b4" fill="rgba(68, 191, 68, 0.5)" stroke-width="0.5" opacity="0"/>
				<polygon id="FEC" class="FEC" stroke="#4b4" fill="rgba(68, 191, 68, 0.5)" stroke-width="0.5" opacity="0"/>
				<polyline id="svgfHauteur" class="fHauteur" stroke="#4b4" fill="transparent" stroke-width="0.5" opacity="0"/>
				<polyline id="svgfRayon" class="fRayon" stroke="#4b4" fill="transparent" stroke-width="0.5" opacity="0"/>
				<polygon id="svgfnb" class="fnb" stroke="#4b4" fill="transparent" stroke-width="0.5" opacity="0"/>
				<polyline id="coupe" stroke="#69c" fill="transparent" stroke-width="0.5"/>
				<polyline id="dottedCoupe" stroke="#69c" fill="transparent" stroke-width="0.3" stroke-dasharray="1, 1"/>
			</svg>
	
		
			<form id="Form">
				<span id="spanfnb" class="cont">Nbre de Côtés : <input name="fnb" class="incorrect" onfocus="Show('fnb')" onblur="unShow('fnb')" id="fnb" type="number"/>
				</span><br/>
				<span id="spanfHauteur" class="cont">Hauteur de la Pyramide : <input name="fHauteur" class="incorrect" onfocus="Show('fHauteur')" onblur="unShow('fHauteur')" id="fHauteur" type="number"/><br/>
				</span>
				<span id="spanfRayon" class="cont">Rayon de la base : <input name="fRayon" class="incorrect" onfocus="Show('fRayon')" onblur="unShow('fRayon')" id="fRayon" type="number"/><br/>
				</span>
				<span id="spanreset"><input id="reset" type="button" onmousedown="EmptyAll()" value="Réinitialiser" /></span>
				<br><br>
				<span id="spanrPointe" class="cont" onmouseover="Show('rPointe')" onmouseout="unShow('rPointe')">Longueur arêtes isocèles : <input name="rPointe" id="rPointe" type="number" disabled="disabled"/><br/>
				</span>
				<span id="spanrAreteSom" class="cont" onmouseover="Show('rAreteSom')" onmouseout="unShow('rAreteSom')">Angle de corroyage : <input name="rAreteSom" id="rAreteSom" type="number" disabled="disabled"/><br/>
				</span>
				<span id="spanrBase" class="cont" onmouseover="Show('rBase')" onmouseout="unShow('rBase')">Longueur arête de base : <input name="rBase" id="rBase" type="number" disabled="disabled"/><br/>
				</span>
				<span id="spanrAreteBase" class="cont">Corroyage de la base : <input name="rAreteBase" id="rAreteBase" type="number" disabled="disabled"/><br/>
				</span>
			</form>

		</div>
		
		<script src="sidenav.min.js"></script>
		
		<script src="Menu.js"></script>

		<script>
		
			// Modification des formules de Math pour chrome et internet explorer
			if (navigator.userAgent.toLowerCase().match('chrome') || /MSIE 10/i.test(navigator.userAgent) || /MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /Edge\/\d./i.test(navigator.userAgent)) {
				var	ellst = document.getElementsByClassName('uform'),
					ellstlength = ellst.length,
					uform = document.getElementById('uform');
				
				ellst[0].innerHTML = 'AB &#8730; ( OA&#178; + OB&#178; ) &nbsp; (<a href="https://fr.wikipedia.org/wiki/Pythagore" target="_blank" class="wikilink" title="Wikipédia">Pythagore</a>)';
				ellst[1].innerHTML = 'CB = Sin (360 / 4) x OB &nbsp; (<a href="https://fr.wikipedia.org/wiki/Trigonom%C3%A9trie" target="_blank" class="wikilink" title="Wikipédia">Trigonométrie</a>)';
				ellst[2].innerHTML = 'ABC = Cos<sup>-1</sup>( CB / 2AB ) &nbsp; (<a href="https://fr.wikipedia.org/wiki/Trigonom%C3%A9trie" target="_blank" class="wikilink" title="Wikipédia">Trigonométrie</a>)';
				ellst[3].innerHTML = 'CE = Sin(ABC) x CB &nbsp; (<a href="https://fr.wikipedia.org/wiki/Trigonom%C3%A9trie" target="_blank" class="wikilink" title="Wikipédia">Trigonométrie</a>)';
				ellst[4].innerHTML = 'OBC = ( 180 - (360 / 4) ) / 2 &nbsp; (<a href="https://fr.wikipedia.org/wiki/Euclide" target="_blank"  class="wikilink" title="Wikipédia">Euclide</a>)';
				ellst[5].innerHTML = 'CF = Sin(OBC) x CB &nbsp; (<a href="https://fr.wikipedia.org/wiki/Trigonom%C3%A9trie" target="_blank" class="wikilink" title="Wikipédia">Trigonométrie</a>)';
				ellst[6].innerHTML = 'FEC = Sin( CF / CE) &nbsp; (<a href="https://fr.wikipedia.org/wiki/Trigonom%C3%A9trie" target="_blank" class="wikilink" title="Wikipédia">Trigonométrie</a>)';
				for (var i = 0; i < ellstlength; i++) {
					ellst[i].style.fontSize = "1vw";
					ellst[i].style.margin = 0;
				}
				
				if (/MSIE 10/i.test(navigator.userAgent) || /MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /Edge\/\d./i.test(navigator.userAgent)) {
					document.getElementsByTagName('html').height = "100%";
					document.getElementById('content').style.height = "100%";
				}
			}

			// fonction apparition des détails de calcule
			function Show(x) {
				var ellst = document.getElementsByClassName(x);
				for (i = 0; i < ellst.length; i++) {
				ellst[i].style.opacity = 1;
				}
			}
			function unShow(x) {
				var ellst = document.getElementsByClassName(x);
				for (i = 0; i < ellst.length; i++) {
				ellst[i].style.opacity = 0;
				}
			}

			// Fonction arrondis
			function arrondir(Val) {
				result = Math.round(Val * 100) / 100;

				return result;
			}

			// Fonction de conversion deg radian
			Math.radians = function(degrees) {
				return degrees * Math.PI / 180;
			};

			Math.degrees = function(radians) {
				return radians * 180 / Math.PI;
			};
		
			// Fonction de suppresion de dessin selon ClassName
			function SupprSVGb(ClassName) {
				Poubelle = document.getElementsByClassName(ClassName);
				while (Poubelle.length > 0) {
					Poubelle[0].remove();
				}
			}
			
			// fonction de dessin de la pyramide
			function DPyr(nb,Hauteur,Rayon) {
				var nb = Number(nb),
					Hauteur = Number(Hauteur),
					Rayon = Number(Rayon),
					OriginOrtho = [60, 90],
					RgrdCercle = 55,
					RptCercle = 10,
					AngleDepart = 55,
					SommetP = [0, 0],
					Point = [],
					TLPoints = [],
					AngleEspAngle = 0,
					AngleTravail = AngleDepart,
					RadTravail = 0,
					SvgString = '',
					iMoin1 = 0,
					SVG = document.getElementById('fond'),
					a = 0,
					lCoupe = 0;
					
				// Suppression du tracé précédent
					SupprSVGb("Face");
					SupprSVGb("FaceCache");

				// Mise à l'échelle de la hauteur de la pyramide pour le dessin
					Hauteur = (Hauteur * RgrdCercle) / Rayon;

				// Placement du sommetP
					SommetP = [OriginOrtho[0], (OriginOrtho[1]-Hauteur)];

				// Placement du point de centre ortho
					NewPoint = document.getElementById("PointOrtho");
					NewPoint.setAttribute("cx", OriginOrtho[0]);
					NewPoint.setAttribute("cy", OriginOrtho[1]);
				
				// Boucle de tracé pyramide
					for (i = 0; i <= nb; i++) {
						// Angle de chaque point
						AngleEspAngle = 360/nb;
						RadTravail = Math.radians(AngleTravail);
						// x dans Px
						Px = OriginOrtho[0] + RgrdCercle * Math.cos(RadTravail);
						// y dans Py
						Py = OriginOrtho[1] + RptCercle * Math.sin(RadTravail);
						TLPoints[i] = [Px,Py];
						// Déterminon a et b de f(x) des droites des points i et iMoin1 au sommet de la pyramide
						a = (TLPoints[iMoin1][1] - SommetP[1]) / (TLPoints[iMoin1][0] - SommetP[0]);
						b = TLPoints[iMoin1][1] - a * TLPoints[iMoin1][0];
						
						if (i==0) {}
						else if ((TLPoints[i][0] <= SommetP[0] && TLPoints[i][1] <= a * TLPoints[i][0] + b) || (TLPoints[iMoin1][0] > SommetP[0] && TLPoints[i][1] >= a * TLPoints[i][0] + b) || (TLPoints[i][0] < SommetP[0] && TLPoints[iMoin1][0] > SommetP[0])) {
							// Création triangle par Face
							SvgString = TLPoints[i][0]+","+TLPoints[i][1]+" "+SommetP[0]+","+SommetP[1]+" "+TLPoints[iMoin1][0]+","+TLPoints[iMoin1][1];
							NewTriangle = document.createElementNS("http://www.w3.org/2000/svg","polygon");
							NewTriangle.setAttribute("class", "Face");
							NewTriangle.setAttribute("points", SvgString);
							NewTriangle.setAttribute("fill", "transparent");
							NewTriangle.setAttribute("stroke", "#036");
							NewTriangle.setAttribute("stroke-width", "0.5");
							SVG.appendChild(NewTriangle);
						}
						else {
							// Création triangle Face
							SvgString = TLPoints[iMoin1][0]+","+TLPoints[iMoin1][1]+" "+TLPoints[i][0]+","+TLPoints[i][1]+" "+SommetP[0]+","+SommetP[1];
							NewTriangle = document.createElementNS("http://www.w3.org/2000/svg","polyline");
							NewTriangle.setAttribute("class", "FaceCache");
							NewTriangle.setAttribute("points", SvgString);
							NewTriangle.setAttribute("fill", "transparent");
							NewTriangle.setAttribute("stroke", "#036");
							NewTriangle.setAttribute("stroke-width", "0.3");
							NewTriangle.setAttribute("stroke-dasharray", "1, 1");
							SVG.insertBefore(NewTriangle,document.getElementById("coupe"));
						}
						AngleTravail = AngleTravail + AngleEspAngle;
						iMoin1 = i;
					}
					
					// Tracé hauteur & Rayon caché vert
						SvgString = OriginOrtho[0]+", "+OriginOrtho[1]+" "+SommetP[0]+", "+SommetP[1];
						document.getElementById("svgfHauteur").setAttribute("points", SvgString);
						SvgString = OriginOrtho[0]+", "+OriginOrtho[1]+" "+Px+", "+Py;
						document.getElementById("svgfRayon").setAttribute("points", SvgString);

					// Tracé base de la pyramide vert
						SvgString = TLPoints[0][0]+","+TLPoints[0][1];
						for (i = 1; i <= nb-1; i++) {
							SvgString = SvgString+" "+TLPoints[i][0]+","+TLPoints[i][1];
						}
						NewLine = document.getElementById("svgfnb");
						NewLine.setAttribute("points", SvgString);
						SVG.appendChild(NewLine);
					
				// Tracé de plan de coupe
					// Calcule de la longueur d'un coté egal du triangle isocèle
					IsoCoteEgaux = Math.sqrt(Math.pow(Hauteur,2) + Math.pow(RgrdCercle,2));
					// Calcule du coté base
					IsoCotePietement = 2 * Math.sin(Math.PI/(nb)) * RgrdCercle;
					// Calcule des angles egaux base
					IsoAngle = Math.acos(Math.pow(IsoCotePietement,2) / (2 * IsoCoteEgaux * IsoCotePietement));
					// Calcule de lCoupe
					lCoupe = Math.cos(IsoAngle) * IsoCotePietement;
					// Calcule de alpha
					alpha = Math.atan(Hauteur / RgrdCercle);
					// Calcule de g à partir de lCoupe
					g = Math.cos(alpha) * lCoupe;
					// Calcule de HauteurCoupe
					HauteurCoupe = Math.sin(alpha) * lCoupe;
					// Calcule des cercles grand et petits pour projection
					RgrdCoupe = RgrdCercle - g;
					RptCoupe = (RgrdCoupe * RptCercle) / RgrdCercle;
					// Coordonnées PtCoupe le point haut de la coupe sur le coté de départ
					PtCoupe = {x:(OriginOrtho[0] + RgrdCoupe * Math.cos(RadTravail)), y:(OriginOrtho[1] - HauteurCoupe + RptCoupe * Math.sin(RadTravail))};
					document.getElementById("coupe").setAttribute("points", TLPoints[1][0]+","+TLPoints[1][1]+" "+PtCoupe.x+","+PtCoupe.y+" "+TLPoints[nb-1][0]+","+TLPoints[nb-1][1]);
					document.getElementById("dottedCoupe").setAttribute("points", TLPoints[1][0]+","+TLPoints[1][1]+" "+TLPoints[nb-1][0]+","+TLPoints[nb-1][1]);
			
				// Tracé des Cotés Verts présentation cases
					// Arete Iso
					SvgString = TLPoints[0][0]+","+TLPoints[0][1]+" "+SommetP[0]+","+SommetP[1];
					NewLine = document.getElementById("svgrPointe");
					NewLine.setAttribute("points", SvgString);
					SVG.appendChild(NewLine);
					// Arete Base
					SvgString = TLPoints[0][0]+","+TLPoints[0][1]+" "+TLPoints[1][0]+","+TLPoints[1][1];
					NewLine = document.getElementById("svgrBase");
					NewLine.setAttribute("points", SvgString);
					SVG.appendChild(NewLine);
					// Angle coupe, deux droites
					PtcentreCD = {x:((TLPoints[1][0] + TLPoints[nb-1][0]) / 2), y:((TLPoints[1][1] + TLPoints[nb-1][1]) / 2)};
					SvgString = TLPoints[1][0]+","+TLPoints[1][1]+" "+PtCoupe.x+","+PtCoupe.y+" "+PtcentreCD.x+","+PtcentreCD.y;
					NewLine = document.getElementById("svgrAreteSom");
					NewLine.setAttribute("points", SvgString);
					SVG.appendChild(NewLine);
					// Angle coupe, arc de cercle
					ArcPoint1 = {x:(TLPoints[1][0]+((PtCoupe.x - TLPoints[1][0]) / 4) *3), y:(PtCoupe.y+((TLPoints[1][1] - PtCoupe.y) / 4))};
					ArcPoint2 = {x:(PtCoupe.x + PtcentreCD.x) / 2, y:(PtCoupe.y + PtcentreCD.y) / 2};
					SvgString = PtCoupe.x+","+PtCoupe.y+" "+ArcPoint1.x+","+ArcPoint1.y+" "+ArcPoint2.x+","+ArcPoint2.y;
					NewTriangle = document.getElementById("svgrAAreteSom");
					NewTriangle.setAttribute("points", SvgString);
					SVG.appendChild(NewTriangle);
				
				// Tracé des Cotés Verts présentation démonstration
					// Modif OAB
					SvgString = OriginOrtho[0]+","+OriginOrtho[1]+" "+SommetP[0]+","+SommetP[1]+" "+TLPoints[0][0]+","+TLPoints[0][1];
					NewLine = document.getElementById("OAB");
					NewLine.setAttribute("points", SvgString);
					// Modif OBC
					SvgString = OriginOrtho[0]+","+OriginOrtho[1]+" "+TLPoints[0][0]+","+TLPoints[0][1]+" "+TLPoints[1][0]+","+TLPoints[1][1];
					NewLine = document.getElementById("OBC");
					NewLine.setAttribute("points", SvgString);
					// Modif ABC
					SvgString = SommetP[0]+","+SommetP[1]+" "+TLPoints[0][0]+","+TLPoints[0][1]+" "+TLPoints[1][0]+","+TLPoints[1][1];
					NewLine = document.getElementById("ABC");
					NewLine.setAttribute("points", SvgString);
					// Modif CEB
					SvgString = TLPoints[1][0]+","+TLPoints[1][1]+" "+PtCoupe.x+","+PtCoupe.y+" "+TLPoints[0][0]+","+TLPoints[0][1];
					NewLine = document.getElementById("CEB");
					NewLine.setAttribute("points", SvgString);
					// Modif CFB
					SvgString = TLPoints[1][0]+","+TLPoints[1][1]+" "+PtcentreCD.x+","+PtcentreCD.y+" "+TLPoints[0][0]+","+TLPoints[0][1];
					NewLine = document.getElementById("CFB");
					NewLine.setAttribute("points", SvgString);
					// Modif FEC
					SvgString = PtcentreCD.x+","+PtcentreCD.y+" "+PtCoupe.x+","+PtCoupe.y+" "+TLPoints[1][0]+","+TLPoints[1][1];
					NewLine = document.getElementById("FEC");
					NewLine.setAttribute("points", SvgString);
					// Modif PointO
					NewPoint = document.getElementById("PointO");
					NewPoint.setAttribute("x", OriginOrtho[0]-5);
					NewPoint.setAttribute("y", OriginOrtho[1]);
					SVG.appendChild(NewPoint);
					// Modif PointA
					NewPoint = document.getElementById("PointA");
					NewPoint.setAttribute("x", SommetP[0]-5);
					NewPoint.setAttribute("y", SommetP[1]);
					SVG.appendChild(NewPoint);
					// Modif PointB
					NewPoint = document.getElementById("PointB");
					NewPoint.setAttribute("x", TLPoints[0][0]);
					NewPoint.setAttribute("y", TLPoints[0][1]-2);
					SVG.appendChild(NewPoint);
					// Modif PointC
					NewPoint = document.getElementById("PointC");
					NewPoint.setAttribute("x", TLPoints[1][0]-3.5);
					NewPoint.setAttribute("y", TLPoints[1][1]-2);
					SVG.appendChild(NewPoint);
					// Modif PointE
					NewPoint = document.getElementById("PointE");
					NewPoint.setAttribute("x", PtCoupe.x);
					NewPoint.setAttribute("y", PtCoupe.y-2);
					SVG.appendChild(NewPoint);
					// Modif PointF
					NewPoint = document.getElementById("PointF");
					NewPoint.setAttribute("x", PtcentreCD.x-2);
					NewPoint.setAttribute("y", PtcentreCD.y);
					SVG.appendChild(NewPoint);
			}

			// Tracer de la pyramide par default
			var nbPyrDef = 4;
				HauteurPyrDef = 85;
				RayonPyrDef = 55;
			DPyr(nbPyrDef,HauteurPyrDef,RayonPyrDef);

			// Fonction de Calcule longueur Base
			function LArBa(nb,Rayon) {
				var nb = Number(nb);
					Rayon = Number(Rayon);
					lCB = 0;

				// Calcule de la longueur de l'arête de la base (trigo)
				lCB = 2 * (Math.sin(Math.PI/(nb)) * Rayon);

				// Arrondis
				result = arrondir(lCB);

				return result;
			}

			// Fonction de Calcule de la longueur de l'arete isocèle
			function LArSom(Hauteur,Rayon) {
				var	Hauteur = Number(Hauteur);
					Rayon = Number(Rayon);
					lAB = 0;

				// Calcule de la longueur de l'arête du isocèle (Pythagore)
				lAB = Math.sqrt(Math.pow(Hauteur,2) + Math.pow(Rayon,2));

				// Arrondis
				result = arrondir(lAB);			

				return result;
			}

			// Fonction de Calcule de L'angle le long de l'arête de la base
			function AArBa(nb,Hauteur,Rayon) {
				var lCB = 0;
					HauteurOsCB = 0;

				// Calcule de la longueur de l'arête de la base (trigo)
				lCB = 2 * (Math.sin(Math.PI/nb) * Rayon);
				// Calcule de la longueur de la hauteur issue de O sur CB (trigo)
				HauteurOsCB = (Math.cos(Math.PI/nb) * Rayon);
				// Calcule de L'angle de coupe Base
				aBase = Math.atan(Hauteur / HauteurOsCB) / 2;
				aBase = Math.degrees(aBase);

				result = aBase;

				return result;
			}

			// Fonction de Calcule de l'angle de corroyage le long de l'arête du sommet
			function AArSom(nb,Hauteur,Rayon) {
				var nb = Number(nb);
					Hauteur = Number(Hauteur);
					Rayon = Number(Rayon);
					lAB = 0;
					lCB = 0;
					lCE = 0;
					lCF = 0;
					aABC = 0;
					aOBC = 0;
					aFEC = 0;
				
				// Calcule de la longueur de l'arête isocèle (Pythagore)
				lAB = Math.sqrt(Math.pow(Hauteur,2) + Math.pow(Rayon,2));
				// Calcule de la longueur de l'arête de la base (trigo)
				lCB = 2 * (Math.sin(Math.PI/(nb)) * Rayon);
				// Calcule de l'angle ABC (trigo)
				aABC = Math.acos(lCB / (2 * lAB));
				// Calcule de la longueur de la hauteur issue de C sur AB au point E (trigo)
				lCE = Math.sin(aABC) * lCB;
				// Calcule de l'angle OBC (euclide)
				aOBC = (180 - (360/nb)) / 2;
				aOBC = Math.radians(aOBC);
				// Calcule de la longeur de CD (trigonométrie)
				lCF = (Math.sin(aOBC) * lCB);
				// Cacule de l Angle FEC, Angle de coupe le long de l'arête du sommet (trigo)
				aFEC = Math.asin(lCF / lCE);
				aFEC = Math.degrees(aFEC);

				result = aFEC;

				return result;
			}

			// Fonction de Calcule de réglage machine
			function ARegMach(Angle) {
				Result = 90 - Angle;
				return Result;
			}
		
			// Fonction vider placeholder
			function EmptyPlace() {
				
				var Form = document.getElementById('Form'),
				inputs = document.getElementsByTagName('input'),
				inputsLength = inputs.length;
				
				for (var i = 0 ; i < inputsLength ; i++) {
					if (inputs[i].type == 'number') {
						inputs[i].placeholder = '';
					}
				}
			};

			// Vider les Value+PlaceHolder
			function EmptyAll() {
					
					var Form = document.getElementById('Form'),
					inputs = document.getElementsByTagName('input'),
					inputsLength = inputs.length;
					
					for (var i = 0 ; i < inputsLength ; i++) {
						if (inputs[i].type == 'number') {
							inputs[i].placeholder = '';
							inputs[i].value = '';
						}
					}
				};
		
			// Verification des données de départ (en cas d'actualisation de la page)
			var fnb = document.getElementById('fnb'),
				fHauteur = document.getElementById('fHauteur'),
				fRayon = document.getElementById('fRayon'),
				fnbValue = fnb.value,
				fHauteurValue = fHauteur.value,
				fRayonValue = fRayon.value;

			if (!isNaN(fnbValue) && Number(fnbValue) > 2 && parseFloat(fnbValue) == parseInt(fnbValue)) {
				fnb.className = 'correct';
				if (!isNaN(fHauteurValue) && Number(fHauteurValue) > 0) {
					if (!isNaN(fRayonValue) && Number(fRayonValue) > 0) {
						DPyr(fnbValue,fHauteurValue,fRayonValue);

						rAreteSom = arrondir(ARegMach(AArSom(fnbValue,fHauteurValue,fRayonValue)));
						document.getElementById('rAreteSom').placeholder = rAreteSom;

						rBase = arrondir(LArBa(fnbValue,fRayonValue));
						document.getElementById('rBase').placeholder = rBase;

						rPointe = arrondir(LArSom(fHauteurValue,fRayonValue));
						document.getElementById('rPointe').placeholder = rPointe;

						rAreteBase =  arrondir(ARegMach(AArBa(fnbValue,fHauteurValue,fRayonValue)));
						document.getElementById('rAreteBase').placeholder = rAreteBase;
					}
					else {
						// pyramide par default
						DPyr(nbPyrDef,HauteurPyrDef,RayonPyrDef);
					}
				}
				else {
					// pyramide par default
					DPyr(nbPyrDef,HauteurPyrDef,RayonPyrDef);
				}
			}
			else {
				// pyramide par default
				DPyr(nbPyrDef,HauteurPyrDef,RayonPyrDef);
				fnb.className = 'incorrect';
			}
			if (!isNaN(fHauteurValue) && Number(fHauteurValue) > 0) {
				fHauteur.className = 'correct';
			}
			if (!isNaN(fRayonValue) && Number(fRayonValue) > 0) {
				fRayon.className = 'correct';
			}

			// Fonctions de vérification du formulaire

			var check = {};

			check['fnb'] = function() {

				var fnb = document.getElementById('fnb'),
					fHauteur = document.getElementById('fHauteur'),
					fRayon = document.getElementById('fRayon'),
					fnbValue = fnb.value,
					fHauteurValue = fHauteur.value,
					fRayonValue = fRayon.value;
				
				if (!isNaN(fnbValue) && Number(fnbValue) > 2 && parseFloat(fnbValue) == parseInt(fnbValue)) {
					fnb.className = 'correct';
					if (!isNaN(fHauteurValue) && Number(fHauteurValue) > 0) {
						if (!isNaN(fRayonValue) && Number(fRayonValue) > 0) {
							DPyr(fnbValue,fHauteurValue,fRayonValue);

							rAreteSom = arrondir(ARegMach(AArSom(fnbValue,fHauteurValue,fRayonValue)));
							document.getElementById('rAreteSom').placeholder = rAreteSom;

							rBase = arrondir(LArBa(fnbValue,fRayonValue));
							document.getElementById('rBase').placeholder = rBase;

							rPointe = arrondir(LArSom(fHauteurValue,fRayonValue));
							document.getElementById('rPointe').placeholder = rPointe;

							rAreteBase = arrondir(ARegMach(AArBa(fnbValue,fHauteurValue,fRayonValue)));
							document.getElementById('rAreteBase').placeholder = rAreteBase;
						}
						else {
							// pyramide par default
							DPyr(nbPyrDef,HauteurPyrDef,RayonPyrDef);
						}
					}
					else {
						// pyramide par default
						DPyr(nbPyrDef,HauteurPyrDef,RayonPyrDef);
					}
				}
				else {
					// pyramide par default
					DPyr(nbPyrDef,HauteurPyrDef,RayonPyrDef);
					fnb.className = 'incorrect';
					EmptyPlace();
				}
			};

			check['fHauteur'] = function() {

				var fnb = document.getElementById('fnb'),
					fHauteur = document.getElementById('fHauteur'),
					fRayon = document.getElementById('fRayon'),
					fnbValue = fnb.value,
					fHauteurValue = fHauteur.value,
					fRayonValue = fRayon.value;
				
				if (!isNaN(fHauteurValue) && Number(fHauteurValue) > 0) {
					fHauteur.className = 'correct';
					if (!isNaN(fnbValue) && Number(fnbValue) > 2 && parseFloat(fnbValue) == parseInt(fnbValue)) {
						if (!isNaN(fRayonValue) && Number(fRayonValue) > 0) {
							DPyr(fnbValue,fHauteurValue,fRayonValue);

							rAreteSom = arrondir(ARegMach(AArSom(fnbValue,fHauteurValue,fRayonValue)));
							document.getElementById('rAreteSom').placeholder = rAreteSom;

							rBase = arrondir(LArBa(fnbValue,fRayonValue));
							document.getElementById('rBase').placeholder = rBase;

							rPointe = arrondir(LArSom(fHauteurValue,fRayonValue));
							document.getElementById('rPointe').placeholder = rPointe;

							rAreteBase = arrondir(ARegMach(AArBa(fnbValue,fHauteurValue,fRayonValue)));
							document.getElementById('rAreteBase').placeholder = rAreteBase;
						}
						else {
							// pyramide par default
							DPyr(nbPyrDef,HauteurPyrDef,RayonPyrDef);
						}
					}
					else {
						// pyramide par default
						DPyr(nbPyrDef,HauteurPyrDef,RayonPyrDef);
					}
				}
				else {
					// pyramide par default
					DPyr(nbPyrDef,HauteurPyrDef,RayonPyrDef);
					fHauteur.className = 'incorrect';
					EmptyPlace();
				}
			};

			check['fRayon'] = function() {

				var fnb = document.getElementById('fnb'),
					fHauteur = document.getElementById('fHauteur'),
					fRayon = document.getElementById('fRayon'),
					fnbValue = fnb.value,
					fHauteurValue = fHauteur.value,
					fRayonValue = fRayon.value;

				if (!isNaN(fRayonValue) && Number(fRayonValue) > 0) {
					fRayon.className = 'correct';
					if (!isNaN(fHauteurValue) && Number(fHauteurValue) > 0) {
						if (!isNaN(fnbValue) && Number(fnbValue) > 2 && parseFloat(fnbValue) == parseInt(fnbValue)) {
							DPyr(fnbValue,fHauteurValue,fRayonValue);

							rAreteSom = arrondir(ARegMach(AArSom(fnbValue,fHauteurValue,fRayonValue)));
							document.getElementById('rAreteSom').placeholder = rAreteSom;

							rBase = arrondir(LArBa(fnbValue,fRayonValue));
							document.getElementById('rBase').placeholder = rBase;

							rPointe = arrondir(LArSom(fHauteurValue,fRayonValue));
							document.getElementById('rPointe').placeholder = rPointe;

							rAreteBase = arrondir(ARegMach(AArBa(fnbValue,fHauteurValue,fRayonValue)));
							document.getElementById('rAreteBase').placeholder = rAreteBase;
						}
						else {
							// pyramide par default
							DPyr(nbPyrDef,HauteurPyrDef,RayonPyrDef);
						}
					}
					else {
						// pyramide par default
						DPyr(nbPyrDef,HauteurPyrDef,RayonPyrDef);
					}
				}
				else {
					// pyramide par default
					DPyr(nbPyrDef,HauteurPyrDef,RayonPyrDef);
					fRayon.className = 'incorrect';
					EmptyPlace();
				}
			};

			// Mise en place des événements
			(function() {
			
				var Form = document.getElementById('Form'),
					inputs = document.getElementsByTagName('input'),
					inputsLength = inputs.length;
			
				for (var i = 0 ; i < inputsLength ; i++) {
					if (inputs[i].type == 'number') {
			
						inputs[i].addEventListener('keyup', function(e) {
							check[e.target.id](e.target.id); // "e.target" représente l'input actuellement modifié
						}, false);
			
					}
				}
			})();

		</script>

		<div id="footer">
			<p>
				Faire un don <strong><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=593KJEYR3GQ2S" title="PayPal">Paypal</a></strong> - 
				<a href="https://framagit.org/Frederiiic/Pythagone">Code Source</a>	GPLv3 <img id="copyleft" src="img/Copyleft.svg" alt="Copyleft"> <a href="https://www.fredericpavageau.net">Frederic Pavageau</a> 2025.
			</p>
		</div>

  	</body>
</html>