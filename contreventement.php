<!DOCTYPE html>
<html>
	<head>
        <meta charset="utf-8" />
        <title>Pythagone : Contreventement et diagonale</title>
        <meta name="viewport" content="width=800, user-scalable=no" />
        <meta name="description" content="Pythagone : Calcule des angles de coupes et de la longeur hors tout (de pointe à pointe) d'une diagonale selon son épaisseur, et la hauteur et largeur du rectangle dans lequel elle s'inscrit, par Frédéric Pavageau." />
        <meta name="keywords" content="Pythagore, Euclide, Trigonométrie, Cosinus, Sinus, Tangeante, angle, angle de coupe, diagonale, contreventement, béquille, triangle, plan, calcule, construction, géométrie, frédéric Pavageau." />
        <meta property="og:site_name" content="Pythagone" /> 
        <meta property="og:title" content="Pythagone : Contreventement & diagonale" />
        <meta property="og:type" content="website" /> 
        <meta property="og:url" content="https://www.pythagone.net/" />
        <meta property="og:description" content="Calcule des angles de coupes et de la longeur hors tout (de pointe à pointe) d'une diagonale selon son épaisseur, et la hauteur & largeur du rectangle dans lequel elle s'inscrit par Frédéric Pavageau." />
        <meta property="og:image" content="https://pythagone.fredericpavageau.net/img/Pythagone.svg" />
        <meta name="twitter:card" content="summary_large_image" />
        <link rel="stylesheet" href="style.css" />
		<link rel="stylesheet" href="contreventement.css" />
		<link rel="icon" type="image/svg+xml" href="img/Pythagone.svg" sizes="any"/>
        <link rel="icon" type="image/png" href="img/16-flavico.png" sizes="16x16"/>
		<link rel="icon" type="image/png" href="img/32-flavico.png" sizes="32x32"/>
		<link rel="icon" type="image/png" href="img/64-flavico.png" sizes="64x64"/>
		<link rel="icon" type="image/png" href="img/96-flavico.png" sizes="96x96"/>
		<link rel="icon" type="image/png" href="img/256-flavico.png" sizes="256x256"/>
	</head>

	<body>

		<h1>Pythagone : Contreventement et diagonale</h1>
		<h2>Pythagone : Calcule des angles de coupes et de la longeur hors tout (de pointe à pointe) d'une diagonale selon son épaisseur, et la hauteur et largeur du rectangle dans lequel elle s'inscrit, par Frédéric Pavageau.</h2>
  
		<div id="backdrop"></div>
  
		<div id="sidenav">
			<?php require "menu.php"; ?>
			<script>
				mdf = document.getElementById("contreventement");
    			mdf.className = "active";
			</script>
    	</div>
  
		<div id="content">
		
			<header>
			<div id="menu-toggle">
				<img id="menu" src="img/Menu.svg" alt="Bouton d'ouverture du menu" />
			</div>
			</header>
		
			<div id="formula">
				<div class="uform" onmouseover="Show('pythagore')" onmouseout="unShow('pythagore')">
					<math xmlns="http://www.w3.org/1998/Math/MathML">
						<mrow>
							<mi>d</mi>
							<mo>=</mo>
							<msqrt>
								<msup>
									<mi>a</mi>
									<mn>2</mn>
								</msup>
								<mo>+</mo>
								<msup>
									<mi>b</mi>
									<mn>2</mn>
								</msup>
							</msqrt>
						</mrow>
					</math>
					&nbsp;(<a href="https://fr.wikipedia.org/wiki/Pythagore" target="_blank" class="wikilink" title="Wikipédia">Pythagore</a>)
				</div>
				<div class="uform" onmouseover="Show('trigo1')" onmouseout="unShow('trigo1')">
					<math xmlns="http://www.w3.org/1998/Math/MathML">
						<mrow>
							<mi>α</mi>
							<mo>=</mo>
							<msup>
								<mi>Sin</mi>
								<mn>-1</mn>
							</msup>
							<mfenced>
								<mfrac>
									<mrow>
										<mi>&nbsp;&nbsp;c&nbsp;</mi>
									</mrow>
									<mrow>
										<mi>&nbsp;d&nbsp;&nbsp;</mi>
									</mrow>
								</mfrac>
							</mfenced>
						</mrow>
					</math>
					&nbsp;(<a href="https://fr.wikipedia.org/wiki/Trigonom%C3%A9trie" target="_blank" class="wikilink" title="Wikipédia">Trigonométrie</a>)
				</div>
				<div class="uform" onmouseover="Show('trigo2')" onmouseout="unShow('trigo2')">
					<math xmlns="http://www.w3.org/1998/Math/MathML">
						<mrow>
							<mi>β</mi>
							<mo>=</mo>
							<msup>
								<mi>Tan</mi>
								<mn>-1</mn>
							</msup>
							<mfenced>
								<mfrac>
									<mrow>
										<mi>&nbsp;&nbsp;a&nbsp;</mi>
									</mrow>
									<mrow>
										<mi>&nbsp;b&nbsp;&nbsp;</mi>
									</mrow>
								</mfrac>
							</mfenced>
						</mrow>
					</math>
					&nbsp;(<a href="https://fr.wikipedia.org/wiki/Trigonom%C3%A9trie" target="_blank" class="wikilink" title="Wikipédia">Trigonométrie</a>)
				</div>
				<div class="uform" onmouseover="Show('euclide')" onmouseout="unShow('euclide')">
					<math xmlns="http://www.w3.org/1998/Math/MathML">
						<mrow>
							<mi>γ1</mi>
							<mo>=</mo>
							<mi>α</mi>
							<mo>+</mo>
							<mi>β</mi>
						</mrow>
					</math>
					&nbsp;(<a href="https://fr.wikipedia.org/wiki/Euclide" target="_blank" class="wikilink" title="Wikipédia">Euclide</a>)
				</div>
			</div>

			<svg id="fond" viewBox="0 0 200 97" xml:lang="fr"
			xmlns="http://www.w3.org/2000/svg"
			xmlns:xlink="http://www.w3.org/1999/xlink">
			<title>Contreventement</title>
				<polygon points="3.7 38.2, 133.8 95.9, 188.8 95.9, 58.7 38.2" stroke="#036" fill="#def" stroke-width="0.5"/>
				<path class="trigo1 euclide" d="M 138.5 73.6 Q 137.3 76.5 136.3 79.5 L 188.8 95.9" stroke="#4b4" fill="rgba(68, 191, 68, 0.75)" stroke-width="0.5"/>
				<path class="trigo2 euclide" d="M 145.9 82.5 Q 143.8 89 143.8 95.9 L 188.8 95.9" stroke="#4b4" fill="rgba(68, 191, 68, 0.75)" stroke-width="0.5"/>
				<path d="M 7.8 29.1 Q 13.7 31.7 13.7 38.2" stroke="#69c" fill="transparent" stroke-width="0.5"/>
				<path d="M 161.1 95.9 Q 161.1 90 163.7 84.8" stroke="#69c" fill="transparent" stroke-width="0.5"/>
				<polyline points="3.7 38.2, 19.2 3.3" stroke="#69c" fill="transparent" stroke-width="0.5" stroke-dasharray="0.5, 0.5"/>
				<polyline points="188.8 95.9, 195.3 81.3" stroke="#69c" fill="transparent" stroke-width="0.5" stroke-dasharray="0.5, 0.5"/>
				<polygon points="3.7 38.2, 3.7 95.9, 188.8 95.9, 188.8 38.2" stroke="#036" fill="transparent" stroke-width="0.5"/>
				<polyline points="3.7 38.2, 133.8 95.9" stroke="#036" fill="transparent" stroke-width="0.5"/>
				<polyline points="58.7 38.2, 188.8 95.9" stroke="#036" fill="transparent" stroke-width="0.5"/>
				<polyline points="20 5.6, 18.3 1" stroke="#69c" fill="transparent" stroke-width="0.5"/>
				<polyline points="196 83.7, 194.4 79" stroke="#69c" fill="transparent" stroke-width="0.5"/>
				<polyline points="19.2 3.3, 195.3 81.3" stroke="#69c" fill="transparent" stroke-width="0.5"/>
				<polyline points="58.8 62.6, 67.9 42.3" stroke="#69c" fill="transparent" stroke-width="0.5"/>
				<polyline points="59.7 65, 57.9 60.3" stroke="#69c" fill="transparent" stroke-width="0.5"/>
				<polyline points="68.8 44.6, 67 39.9" stroke="#69c" fill="transparent" stroke-width="0.5"/>
				<polyline class="sega" points="3.7 38.2, 3.7 95.9" stroke="#4b4" fill="transparent" stroke-width="0.5"/>
				<polyline class="segb" points="3.7 95.9, 188.8 95.9" stroke="#4b4" fill="transparent" stroke-width="0.5"/>
				<polyline class="segc" points="58.8 62.6, 67.9 42.3" stroke="#4b4" fill="transparent" stroke-width="0.5"/>
				<polyline class="segc" points="59.7 65, 57.9 60.3" stroke="#4b4" fill="transparent" stroke-width="0.5"/>
				<polyline class="segc" points="68.8 44.6, 67 39.9" stroke="#4b4" fill="transparent" stroke-width="0.5"/>
				<polygon class="pythagore trigo2" points="3.7 38.2, 3.7 95.9, 188.8 95.9" stroke="#4b4" fill="transparent" stroke-width="0.5"/>
				<polygon class="trigo1" points="3.7 38.2, 12.7 17.8, 188.8 95.9" stroke="#4b4" fill="transparent" stroke-width="0.5"/>         
			</svg>
			
			<form id="Form">
				<span id="spana" class="cont">a : <input name="sega" onfocus="Show('sega')" onblur="unShow('sega')" id="sega" type="number"/><br/>
					<span class="tooltip">Impossible.</span>
				</span>
				<span id="spanb" class="cont">b : <input name="segb" onfocus="Show('segb')" onblur="unShow('segb')" id="segb" type="number"/><br/>
					<span class="tooltip">Impossible.</span>
				</span>
				<span id="spanc" class="cont">c : <input name="segc" onfocus="Show('segc')" onblur="unShow('segc')" id="segc" type="number"/><br/>
					<span class="tooltip">Impossible.</span>
				</span>
				<span id="spane" class="cont">e : <input name="sege" id="sege" type="number" disabled="disabled"/><br>
					<span class="tooltip">Doit rester vide.</span>
				</span>
				<span id="spangamma1" class="cont">γ1 : <input name="gamma1" id="gamma1" type="number" disabled="disabled"/><br/>
					<span class="tooltip">Doit rester vide.</span>
				</span>
				<span id="spangamma2" class="cont">γ2 : <input name="gamma2" id="gamma2" type="number" disabled="disabled"/><br/>
					<span class="tooltip">Doit rester vide.</span>
				</span>
				<span id="spand" class="pythagore trigo1 expl">d</span>
				<span id="spanalpha" class="trigo1 euclide expl">α</span>
				<span id="spanbeta" class="trigo2 euclide expl">β</span>
				<span id="spanreset"><input id="reset" type="reset" value="Réinitialiser"/></span>
			</form>

		</div>
    
		<script src="sidenav.min.js"></script>
		
		<script src="Menu.js"></script>

		<script>
		
			// Modification des formules de Math pour chrome et internet explorer
			if (navigator.userAgent.toLowerCase().match('chrome') || /MSIE 10/i.test(navigator.userAgent) || /MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /Edge\/\d./i.test(navigator.userAgent)) {
				var	ellst = document.getElementsByClassName('uform'),
					ellstlength = ellst.length,
					uform = document.getElementById('uform');
				
				ellst[0].innerHTML = 'd = racine ( a&#178 + b&#178 ) &nbsp; (<a href="https://fr.wikipedia.org/wiki/Pythagore" target="_blank" class="wikilink" title="Wikipédia">Pythagore</a>)';
				ellst[1].innerHTML = 'Sin-1 (α) = c / d &nbsp; (<a href="https://fr.wikipedia.org/wiki/Trigonom%C3%A9trie" target="_blank" class="wikilink" title="Wikipédia">Trigonométrie</a>)';
				ellst[2].innerHTML = 'Tan-1 (β) = a / b &nbsp; (<a href="https://fr.wikipedia.org/wiki/Trigonom%C3%A9trie" target="_blank" class="wikilink" title="Wikipédia">Trigonométrie</a>)';
				ellst[3].innerHTML = 'γ1 = α + β &nbsp; (<a href="https://fr.wikipedia.org/wiki/Euclide" target="_blank"  class="wikilink" title="Wikipédia">Euclide</a>)';
				for (var i = 0; i < ellstlength; i++) {
					ellst[i].style.fontSize = "2.5vw";
					ellst[i].style.margin = 0;
				}
				
				if (/MSIE 10/i.test(navigator.userAgent) || /MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /Edge\/\d./i.test(navigator.userAgent)) {
					document.getElementsByTagName('html').height = "100%";
					document.getElementById('content').style.height = "100%";
				}
			}

			// fonction apparition des détails de calcule
			function Show(x) {
				var ellst = document.getElementsByClassName(x);
				for (i = 0; i < ellst.length; i++) {
				ellst[i].style.opacity = 1;
				}
			}
			
			function unShow(x) {
				var ellst = document.getElementsByClassName(x);
				for (i = 0; i < ellst.length; i++) {
				ellst[i].style.opacity = 0;
				}
			}

			// Fonction de conversion deg radian
			Math.radians = function(degrees) {
				return degrees * Math.PI / 180;
			};

			Math.degrees = function(radians) {
				return radians * 180 / Math.PI;
			};

			// Fonction de désactivation de l'affichage des tooltips
			function deactivateTooltips() {
			
				var spans = document.getElementsByTagName('span'),
				spansLength = spans.length;
				
				for (var i = 0 ; i < spansLength ; i++) {
					if (spans[i].className == 'tooltip') {
						spans[i].style.display = 'none';
					}
				}
			}

			// Fonction de récupération Tooltip
			function getTooltip(elements) {
			
				while (elements = elements.nextSibling) {
					if (elements.className === 'tooltip') {
						return elements;
					}
				}
				return false;
			}
		
			// Fonction vider placeholder
			function EmptyPlace() {
				
				var Form = document.getElementById('Form'),
				inputs = document.getElementsByTagName('input'),
				inputsLength = inputs.length;
				
				for (var i = 0 ; i < inputsLength ; i++) {
					if (inputs[i].type == 'number') {
						inputs[i].placeholder = '';
					}
				}
			};

			// Fonctions de vérification du formulaire
			var check = {};

			check['sega'] = function() {

				var sega = document.getElementById('sega'),
					segb = document.getElementById('segb'),
					segc = document.getElementById('segc'),
					tooltipStyle = getTooltip(sega).style,
					segaValue = sega.value,
					segbValue = segb.value,
					segcValue = segc.value;
				
				if (!isNaN(segaValue) && segaValue > 0 && segaValue != "") {
					// vérif segb et segc
					if (!isNaN(segbValue) && segbValue != "" && !isNaN(segcValue) && segcValue != "") {
						// hyp
						hyp = Math.sqrt(Math.pow(segaValue,2) + Math.pow(segbValue,2));
						// Gamma1
						result = Math.asin(segcValue / hyp) + Math.atan(segaValue / segbValue);
						result = Math.degrees(result);
						around = Math.round(result * 100) / 100;
						document.getElementById('gamma1').placeholder = around;
						// Gamma2
						result = 90 - result;
						around = Math.round(result * 100) / 100;
						document.getElementById('gamma2').placeholder = around;
						// e
						result = Math.sqrt(Math.pow(hyp,2) - Math.pow(segcValue,2));
						around = Math.round(result * 100) / 100;
						document.getElementById('sege').placeholder = around;
					}
					sega.className = 'correct';
					tooltipStyle.display = 'none';
				}
				else if (segaValue == String("")) {
					sega.className = '';
					tooltipStyle.display = 'none';
					EmptyPlace();
				}
				else {
					sega.className = 'incorrect';
					tooltipStyle.display = 'inline-block';
				}
				
				
			};
			
			check['segb'] = function() {

				var sega = document.getElementById('sega'),
					segb = document.getElementById('segb'),
					segc = document.getElementById('segc'),
					tooltipStyle = getTooltip(segb).style,
					segaValue = sega.value,
					segbValue = segb.value,
					segcValue = segc.value;
				
				if (!isNaN(segbValue) && segbValue > 0 && Number(segcValue) <= Number(segbValue) && segbValue != "") {
					// vérif segb et segc
					if (!isNaN(segaValue) && segaValue != "" && !isNaN(segcValue) && segcValue != "") {
						// hyp
						hyp = Math.sqrt(Math.pow(segaValue,2) + Math.pow(segbValue,2));
						// Gamma1
						result = Math.asin(segcValue / hyp) + Math.atan(segaValue / segbValue);
						result = Math.degrees(result);
						around = Math.round(result * 100) / 100;
						document.getElementById('gamma1').placeholder = around;
						// Gamma2
						result = 90 - result;
						around = Math.round(result * 100) / 100;
						document.getElementById('gamma2').placeholder = around;
						// e
						result = Math.sqrt(Math.pow(hyp,2) - Math.pow(segcValue,2));
						around = Math.round(result * 100) / 100;
						document.getElementById('sege').placeholder = around;
					}
					segb.className = 'correct';
					if (segcValue != String("")) {
						segc.className = 'correct';
						getTooltip(segc).style.display = 'none';
						}
					tooltipStyle.display = 'none';
				}
				else if (segcValue != String("") && Number(segcValue) >= Number(segbValue)) {
					if (segbValue == String("")) {
						segb.className = '';
						tooltipStyle.display = 'none';
						}
					else {
						getTooltip(segb).innerHTML = "Doit être > à c."
						segb.className = 'incorrect';
						tooltipStyle.display = 'inline-block';
					}
				}
				else if (segbValue == String("")) {
					segb.className = '';
					tooltipStyle.display = 'none';
					EmptyPlace();
				}
				else {
					getTooltip(segb).innerHTML = "Impossible."
					segb.className = 'incorrect';
					tooltipStyle.display = 'inline-block';
				}
				
			};
			
			check['segc'] = function() {

				var sega = document.getElementById('sega'),
					segb = document.getElementById('segb'),
					segc = document.getElementById('segc'),
					tooltipStyle = getTooltip(segc).style,
					segaValue = sega.value,
					segbValue = segb.value,
					segcValue = segc.value;
				
				if (!isNaN(segcValue) && segcValue > 0 && (segbValue == String("") || Number(segcValue) <= Number(segbValue)) && segcValue != "") {
					// vérif segb et segc
					if (!isNaN(segaValue) && segaValue != "" && !isNaN(segbValue) && segbValue != "") {
						// hyp
						hyp = Math.sqrt(Math.pow(segaValue,2) + Math.pow(segbValue,2));
						// Gamma1
						result = Math.asin(segcValue/hyp) + Math.atan(segaValue/segbValue);
						result = Math.degrees(result);
						around = Math.round(result * 100) / 100;
						document.getElementById('gamma1').placeholder = around;
						// Gamma2
						result = 90 - result;
						around = Math.round(result * 100) / 100;
						document.getElementById('gamma2').placeholder = around;
						// e
						result = Math.sqrt(Math.pow(hyp,2) - Math.pow(segcValue,2));
						around = Math.round(result * 100) / 100;
						document.getElementById('sege').placeholder = around;
					}
					segc.className = 'correct';
					if (segbValue != String("")) {
						segb.className = 'correct';
						getTooltip(segb).style.display = 'none';
					}
					tooltipStyle.display = 'none';
				}
				else if (segbValue != String("") && Number(segcValue) >= Number(segbValue)) {
					if (segcValue == String("")) {
						segc.className = '';
						tooltipStyle.display = 'none';
						}
					else {
						getTooltip(segc).innerHTML = "Doit être < à b."
						segc.className = 'incorrect';
						tooltipStyle.display = 'inline-block';
					}
				}
				else if (segcValue == String("")) {
					segc.className = '';
					tooltipStyle.display = 'none';
					EmptyPlace();
				}
				else {
					getTooltip(segc).innerHTML = "Impossible."
					segc.className = 'incorrect';
					tooltipStyle.display = 'inline-block';
				}
				
			};
			
			check['sege'] = function() {
				
				var sege = document.getElementById('sege'),
					tooltipStyle = getTooltip(sege).style,
					segeValue = sege.value;
					
				if (segeValue != String("")) {
					sege.className = 'incorrect';
					tooltipStyle.display = 'inline-block';
				}
				else {
					sege.className = '';
					tooltipStyle.display = 'none';
				}
				
			};
			
			check['gamma1'] = function() {
				
				var gamm1 = document.getElementById('gamma1'),
					tooltipStyle = getTooltip(gamma1).style,
					gamma1Value = gamma1.value;
					
				if (gamma1Value != String("")) {
					gamma1.className = 'incorrect';
					tooltipStyle.display = 'inline-block';
				}
				else {
					gamma1.className = '';
					tooltipStyle.display = 'none';
				}

			};
			
			check['gamma2'] = function() {
				
				var gamma2 = document.getElementById('gamma2'),
					tooltipStyle = getTooltip(gamma2).style,
					gamma2Value = gamma2.value;
					
				if (gamma2Value != String("")) {
					gamma2.className = 'incorrect';
					tooltipStyle.display = 'inline-block';
				}
				else {
					gamma2.className = '';
					tooltipStyle.display = 'none';
				}
			};

			// Mise en place des événements
			(function() {
			
				var Form = document.getElementById('Form'),
					inputs = document.getElementsByTagName('input'),
					inputsLength = inputs.length;
			
				for (var i = 0 ; i < inputsLength ; i++) {
					if (inputs[i].type == 'number') {
			
						inputs[i].addEventListener('keyup', function(e) {
							check[e.target.id](e.target.id); // "e.target" représente l'input actuellement modifié
						}, false);
			
					}
				}
			
				Form.addEventListener('reset', function() {
			
					for (var i = 0 ; i < inputsLength ; i++) {
						if (inputs[i].type == 'number') {
							inputs[i].className = '';
							inputs[i].placeholder = '';
						}
					}
			
					deactivateTooltips();
			
				}, false);
			
			})();


			// Désactiver les "tooltips"
			deactivateTooltips();

			</script>

		<div id="footer">
			<p>
				Faire un don <strong><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=593KJEYR3GQ2S" title="PayPal">Paypal</a></strong> - 
				<a href="https://framagit.org/Frederiiic/Pythagone">Code Source</a>	GPLv3 <img id="copyleft" src="img/Copyleft.svg" alt="Copyleft"> <a href="https://www.fredericpavageau.net">Frederic Pavageau</a> 2025.
			</p>
		</div>
	
	</body>
</html>