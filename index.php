<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Pythagone</title>
    <meta name="viewport" content="width=800, user-scalable=0" />
    <meta name="description" content="Pythagone : Une application web pour simplifier les calcules mathématiques de construction." />
    <meta name="keywords" content="Pythagore, Euclide, Thalès, Trigonométrie, Cosinus, Sinus, Tangeante, angle, angle de coupe, diagonales, contreventements, pyramides, triangle, calcule, résolution, résultats, frédéric Pavageau." />
    <meta property="og:site_name" content="Pythagone" /> 
    <meta property="og:title" content="Pythagone" />
    <meta property="og:type" content="website" /> 
    <meta property="og:url" content="https:/pythagone.fairelebonchoix.net/" /> 
    <meta property="og:description" content="Une application web pour simplifier les calcules mathématiques de construction." />
	<meta property="og:image" content="https://pythagone.fredericpavageau.net/img/Pythagone.svg" />
    <meta name="twitter:card" content="summary_large_image" />
    <link rel="stylesheet" href="style.css" />
    <link rel="icon" type="image/svg+xml" href="img/Pythagone.svg" sizes="any"/>
    <link rel="icon" type="image/png" href="img/16-flavico.png" sizes="16x16"/>
	<link rel="icon" type="image/png" href="img/32-flavico.png" sizes="32x32"/>
    <link rel="icon" type="image/png" href="img/64-flavico.png" sizes="64x64"/>
    <link rel="icon" type="image/png" href="img/96-flavico.png" sizes="96x96"/>
	<link rel="icon" type="image/png" href="img/256-flavico.png" sizes="256x256"/>
  </head>
  
  <body>
  
	<div id="index">
        <div id="sidenav">
        <?php require "menu.php"; ?>
        </div>
    </div>
    
	</body>
</html>
