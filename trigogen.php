<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Pythagone : Trigonométrie et Pythagore</title>
		<meta name="viewport" content="width=800, user-scalable=no">
		<meta name="description" content="Pythagone : Calcule toutes les réponses possibles à l'aide de la trigonométrie & du théorème d'al-kachi dans un triangle quelconque, selon les valeurs fournies, par Frédéric Pavageau." />
		<meta name="keywords" content="Pythagore, Euclide, Trigonométrie, al-kashi, Cosinus, Sinus, Tangeante, angle, angle de coupe, hypothénuse, triangle, calcule, géométrie, construction, frédéric Pavageau." />
		<meta property="og:site_name" content="Pythagone" /> 
		<meta property="og:title" content="Pythagone : Al-Kashi & trigonométrie" />
		<meta property="og:type" content="website" /> 
		<meta property="og:url" content="https://www.pythagone.net/" />
		<meta property="og:description" content="Al-Kashi, Calcule toutes les réponses possibles à l'aide de la trigonométrie & du théorème d'al-kachi dans un triangle quelconque, selon les valeurs fournies par Frédéric Pavageau." />
		<meta property="og:image" content="https://pythagone.fredericpavageau.net/img/Pythagone.svg" />
		<meta name="twitter:card" content="summary_large_image" />
		<link rel="stylesheet" href="style.css" />
		<link rel="stylesheet" href="trigogen.css" /> 
		<link rel="icon" type="image/svg+xml" href="img/Pythagone.svg" sizes="any"/>
		<link rel="icon" type="image/png" href="img/16-flavico.png" sizes="16x16"/>
		<link rel="icon" type="image/png" href="img/32-flavico.png" sizes="32x32"/>
		<link rel="icon" type="image/png" href="img/64-flavico.png" sizes="64x64"/>
		<link rel="icon" type="image/png" href="img/96-flavico.png" sizes="96x96"/>
		<link rel="icon" type="image/png" href="img/256-flavico.png" sizes="256x256"/>
	</head>

	<body>

		<h1>Pythagone : Trigonométrie et Pythagore</h1>
		<h2>Pythagone : Calcule toutes les réponses possibles à l'aide de la trigonométrie & du théorème d'al-kachi dans un triangle quelconque, selon les valeurs fournies, par Frédéric Pavageau.</h2>
	
		<div id="backdrop"></div>
		
		<div id="sidenav">
			<?php require "menu.php"; ?>
			<script>
					mdf = document.getElementById("trigogen");
					mdf.className = "active";
			</script>
		</div>
		
		<div id="content">
    
			<header>
				<div id="menu-toggle">
					<img id="menu" src="img/Menu.svg" alt="Bouton d'ouverture du menu" />
				</div>
			</header>
	
			<div id="formula">
				<div class="uform">
					<math xmlns="http://www.w3.org/1998/Math/MathML">
						<mrow>
							<mfrac>
								<mi>a</mi>
								<mrow>
									<mi>sin (α)</mi>
								</mrow>
							</mfrac>
							<mo>=</mo>
							<mfrac>
								<mi>b</mi>
								<mrow>
									<mi>sin (β)</mi>
								</mrow>
							</mfrac>
							<mo>=</mo>
							<mfrac>
								<mi>c</mi>
								<mrow>
									<mi>sin (γ)</mi>
								</mrow>
							</mfrac>
						</mrow>
					</math>
					&nbsp;(<a href="https://fr.wikipedia.org/wiki/Loi_des_sinus" target="_blank" class="wikilink" title="Wikipédia">Loi des Sinus</a>)
				</div>
				<div class="uform">
					<math xmlns="http://www.w3.org/1998/Math/MathML">
					<msup>
						<mi>c</mi>
						<mrow>
							<mn>2</mn>
						</mrow>
					</msup>
					<mo>=</mo>
					<msup>
						<mi>a</mi>
						<mrow>
							<mn>2</mn>
						</mrow>
					</msup>
					<mo>+</mo>
					<msup>
						<mi>b</mi>
						<mrow>
							<mn>2</mn>
						</mrow>
					</msup>
					<mo> − </mo>
					<mn>2</mn>
					<mi>a</mi>
					<mi>b</mi>
					<mi>&nbsp;cos (γ)</mi>
					</math>
					&nbsp;(<a href="https://fr.wikipedia.org/wiki/Al-Kashi" target="_blank" class="wikilink" title="Wikipédia">Al-Kashi</a>)
				</div>
				<div class="uform">
					<math xmlns="http://www.w3.org/1998/Math/MathML">
						<mrow>
							<mi>180°</mi>
							<mo>=</mo>
							<mi>α</mi>
							<mo>+</mo>
							<mi>β</mi>
							<mo>+</mo>
							<mi>γ</mi>
						</mrow>
					</math>
					&nbsp;(<a href="https://fr.wikipedia.org/wiki/Euclide" target="_blank" class="wikilink" title="Wikipédia">Euclide</a>)
				</div>
			</div>

			<svg id="fond" viewBox="0 0 200 100" xml:lang="fr"
				xmlns="http://www.w3.org/2000/svg"
				xmlns:xlink="http://www.w3.org/1999/xlink">
				<title>Triangle Rectangle</title>
				<path d="M 33.44 19.52 Q 48.95 25.85 58 11.54" stroke="#69c" fill="transparent" stroke-width="0.5"/>
				<path d="M 173.51 83.19 Q 169 90.45 169 99" stroke="#69c" fill="transparent" stroke-width="0.5"/>
				<path d="M 8.56 80.48 Q 21 85.56 21 99" stroke="#69c" fill="transparent" stroke-width="0.5"/>
				<polygon points="1 99, 41 1, 199 99" stroke="#036" fill="transparent" stroke-width="0.5"/>
				
				<path class="gamma" d="M 33.44 19.52 Q 48.95 25.85 58 11.54" stroke="rgba(68, 191, 68, 1)" fill="transparent" stroke-width="0.5"/>
				<path class="alpha" d="M 173.51 83.19 Q 169 90.45 169 99" stroke="rgba(68, 191, 68, 1)" fill="transparent" stroke-width="0.5"/>
				<path class="beta" d="M 8.56 80.48 Q 21 85.56 21 99" stroke="rgba(68, 191, 68, 1)" fill="transparent" stroke-width="0.5"/>
				<polyline class="sega" points="1 99, 41 1" stroke="rgba(68, 191, 68, 1)" fill="transparent" stroke-width="0.5"/>
				<polyline class="segb" points="41 1, 199 99" stroke="rgba(68, 191, 68, 1)" fill="transparent" stroke-width="0.5"/>
				<polyline class="segc" points="199 99, 1 99" stroke="rgba(68, 191, 68, 1)" fill="transparent" stroke-width="0.5"/>
			</svg>

			<form id="Form">
				<span id="spanalpha" class="cont">α : <input name="alpha" onfocus="Show('alpha')" onblur="unShow('alpha')" id="alpha" type="number"/><input name="alpha2" onfocus="Show('alpha2')" onblur="unShow('alpha2')" id="alpha2" type="text" disabled="disabled"/><br/>
					<span class="tooltip">Impossible.</span>
				</span>
				<span id="spanbeta" class="cont">β : <input name="beta" onfocus="Show('beta')" onblur="unShow('beta')" id="beta" type="number"/><input name="beta2" onfocus="Show('beta2')" onblur="unShow('beta2')" id="beta2" type="text" disabled="disabled"/><br/>
					<span class="tooltip">Impossible.</span>
				</span>
				<span id="spangamma" class="cont">γ : <input name="gamma" onfocus="Show('gamma')" onblur="unShow('gamma')" id="gamma" type="number"/><input name="gamma2" onfocus="Show('gamma2')" onblur="unShow('gamma2')" id="gamma2" type="text" disabled="disabled"/><br/>
					<span class="tooltip">Impossible.</span>
				</span>
				<span id="spana" class="cont">a : <input name="sega" onfocus="Show('sega')" onblur="unShow('sega')" id="sega" type="number"/><input name="sega2" onfocus="Show('sega2')" onblur="unShow('sega2')" id="sega2" type="text" disabled="disabled"/><br/>
					<span class="tooltip">Impossible.</span>
				</span>
				<span id="spanb" class="cont">b : <input name="segb" onfocus="Show('segb')" onblur="unShow('segb')" id="segb" type="number"/><input name="segb2" onfocus="Show('segb2')" onblur="unShow('segb2')" id="segb2" type="text" disabled="disabled"/><br/>
					<span class="tooltip">Impossible.</span>
				</span>
				<span id="spanc" class="cont">c : <input name="segc" onfocus="Show('segc')" onblur="unShow('segc')" id="segc" type="number"/><input name="segc2" onfocus="Show('segc2')" onblur="unShow('segc2')" id="segc2" type="text" disabled="disabled"/><br/>
					<span class="tooltip">Impossible.</span>
				</span>
				<span id="spanreset"><input id="reset" type="reset" value="Réinitialiser" /></span>
			</form>

    	</div>

		<script src="sidenav.min.js"></script>

		<script src="Menu.js"></script>

		<script>

			var	segaResult = '',
				segbResult = '',
				segcResult = '',
				alphaResult = '',
				betaResult = '',
				gammaResult = '';

			// Modification des formules de Math pour chrome et internet explorer
			if (navigator.userAgent.toLowerCase().match('chrome') || /MSIE 10/i.test(navigator.userAgent) || /MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /Edge\/\d./i.test(navigator.userAgent)) {
				var	ellst = document.getElementsByClassName('uform'),
					ellstlength = ellst.length,
					uform = document.getElementById('uform');
						
				ellst[0].innerHTML = 'a / Sin (α) = b / Sin (β) = c / Sin (γ) &nbsp; (<a href="https://fr.wikipedia.org/wiki/Loi_des_sinus" target="_blank" title="Wikipédia">Loi des Sinus</a>)';
				ellst[1].innerHTML = 'c² = a² + b² - 2ab Cos(γ) &nbsp; (<a href="https://fr.wikipedia.org/wiki/Al-Kashi" target="_blank" title="Wikipédia">Al-Kashi</a>)';
				ellst[2].innerHTML = '180° = α + β + γ &nbsp; (<a href="https://fr.wikipedia.org/wiki/Euclide" target="_blank"  class="wikilink" title="Wikipédia">Euclide</a>)';
				for (var i = 0; i < ellstlength; i++) {
					ellst[i].style.fontSize = "1.8vw";
					ellst[i].style.margin = 0;
				}
				
				if (/MSIE 10/i.test(navigator.userAgent) || /MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /Edge\/\d./i.test(navigator.userAgent)) {
					document.getElementsByTagName('html').height = "100%";
					document.getElementsByTagName('body').height = "100%";
					document.getElementById('content').style.height = "100%";
				}		
			}

			// Fonction de conversion deg radian
			Math.radians = function(degrees) {
				return degrees * Math.PI / 180;
			};

			Math.degrees = function(radians) {
				return radians * 180 / Math.PI;
			};

			// Fonction de désactivation de l'affichage des tooltips
			function deactivateTooltips() {
			
				var spans = document.getElementsByTagName('span'),
					spansLength = spans.length;
				
				for (var i = 0 ; i < spansLength ; i++) {
					if (spans[i].className == 'tooltip') {
						spans[i].style.display = 'none';
					}
				}	
			}

			// Fonction de récupération Tooltip
			function getTooltip(elements) {
			
				while (elements = elements.nextSibling) {
					if (elements.className === 'tooltip') {
						return elements;
					}
				}
				
				return false;
			}

			// fonction apparition des détails de calcule
			function Show(x) {
				var ellst = document.getElementsByClassName(x);
				for (i = 0; i < ellst.length; i++) {
				ellst[i].style.opacity = 1;
				}
			}

			function unShow(x) {
				var ellst = document.getElementsByClassName(x);
				for (i = 0; i < ellst.length; i++) {
				ellst[i].style.opacity = 0;
				}
			}

			// Fonction vider placeholder
			function EmptyPlace(prop) {
				
				var Form = document.getElementById('Form'),
				inputs = document.getElementsByTagName('input'),
				inputsLength = inputs.length;
				
				for (var i = 0 ; i < inputsLength ; i++) {
					if (inputs[i].type == 'number') {
						inputs[i].placeholder = '';
					}
				}
				if (prop = "all") {
					segaResult = '';
					segbResult = '';
					segcResult = '';
					alphaResult = '';
					betaResult = '';
					gammaResult = '';
					sega2.style.display = 'none';
					segb2.style.display = 'none';
					segc2.style.display = 'none';
					alpha2.style.display = 'none';
					beta2.style.display = 'none';
					gamma2.style.display = 'none';
					
				}
				if (prop = "a") { segaResult = ''; }
				if (prop = "b") { segcResult = ''; }
				if (prop = "c") { segcResult = ''; }
				if (prop = "alpha") { alphaResult = ''; }
				else if (prop = "beta") { betaResult = ''; }
				else if (prop = "gamma") { gammaResult = ''; }
			};
		
			// Fonction somme des angles Euclide
			function Euclide(Angle1,Angle2) {
				
				result = 180 - Angle1 - Angle2;
				return result;	
			};
			
			// Pythagore
			function pythagoreHypParSin(AngleRef,Hyp) {
				
				radAngleRef = Math.radians(AngleRef);
				result = Hyp * Math.sin(radAngleRef);
				return result;
			}
		
			// Fonction Loi des Sinus avec un angle
			function LdSinusAngle(AngleRef,CoteOppRef,Angle) {
			
				radAngleRef = Math.radians(AngleRef);
				radAngle = Math.radians(Angle);
				result = (CoteOppRef * Math.sin(radAngle)) / Math.sin(radAngleRef);
				return result;	
			}
			
			// Fonction Loi des Sinus avec coté
			function LdSinusCote(AngleRef,CoteOppRef,Cote) {
				
				radAngleRef = Math.radians(AngleRef);
				result = Math.degrees(Math.asin((Cote * Math.sin(radAngleRef)) / CoteOppRef));
				return result;	
			}
			
			// Loi des cosinus
			function LdCosinus(Cote1,Cote2,Angle) {
				
				radAngle = Math.radians(Angle);
				result = Math.sqrt(Math.pow(Cote1,2) + Math.pow(Cote2,2) - 2 * Cote1 * Cote2 * Math.cos(radAngle));
				return result;	
			}
			
			// Loi des cosinus
			function LdCosinusCote(Cote1,Cote2,CoteOpp) {
				
				result = Math.degrees(Math.acos((Math.pow(Cote1,2) + Math.pow(Cote2,2) - Math.pow(CoteOpp,2)) / (2 * Cote1 * Cote2)));
				return result;	
			}
			
			// Fonction Vérification des résultat dans les cases
			function checkResult(result,Value,element) {
				
				if (Number(Value) != Number(result)) {
					if (Value == String("")) {element.className = '';}
					else {element.className = 'incorrect';}
				} else {element.className = 'correct';}	
			}
			
			// Fonction pour arrondir
			function arrondir(result) {
				
				return Math.round(result*100)/100;		
			}

			// Fonctions de vérification du formulaire
			var check = {};

			check['alpha'] = function() {

				var sega = document.getElementById('sega'),
					segb = document.getElementById('segb'),
					segc = document.getElementById('segc'),
					alpha = document.getElementById('alpha'),
					beta = document.getElementById('beta'),
					gamma = document.getElementById('gamma'),
					tooltipStyle = getTooltip(alpha).style,
					segaValue = sega.value,
					segbValue = segb.value,
					segcValue = segc.value,
					alphaValue = alpha.value,
					betaValue = beta.value,
					gammaValue = gamma.value;
				
				if (!isNaN(alphaValue) && Number(alphaValue) >= 0 && Number(alphaValue) < 180 && alphaValue != String("")) {
					alpha.className = 'correct';
					tooltipStyle.display = 'none';

					// beta ou gamma existes
					if (!isNaN(betaValue) && betaValue != "" || !isNaN(gammaValue) && gammaValue != "") {
						if(Number(betaValue) != 0 && Number(alphaValue) + Number(betaValue) < 180 || Number(gammaValue) != 0 && Number(alphaValue) + Number(gammaValue) < 180) {
							if(!isNaN(betaValue) && betaValue != "") {
								// Calcule de gamma
								Euclide(alphaValue,betaValue);
								gammaResult = result;
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								checkResult(result,gammaValue,gamma);
								betaResult = betaValue;
							}
							else {
								// Calcule de beta
								Euclide(alphaValue,gammaValue);
								betaResult = result;
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								checkResult(result,betaValue,beta);
								gammaResult = gammaValue;
							}
							// && sega existe
							if (!isNaN(segaValue) && segaValue != "") {
								// Calcule de segb
								result = LdSinusAngle(alphaValue,segaValue,betaResult);
								segbResult = result;
								RoundResult = arrondir(result);
								segb.placeholder = RoundResult;
								checkResult(result,segbValue,segb);
								// Calcule de segc
								result = LdSinusAngle(alphaValue,segaValue,gammaResult);
								segcResult = result;
								RoundResult = arrondir(result);
								segc.placeholder = RoundResult;
								checkResult(result,segcValue,segc);
							}
							// && segb existe
							else if (!isNaN(segbValue) && segbValue != "") {
								// Calcule de sega
								result = LdSinusAngle(betaResult,segbValue,alphaValue);
								segaResult = result;
								RoundResult = arrondir(result);
								sega.placeholder = RoundResult;
								checkResult(result,segaValue,sega);
								// Calcule de segc
								result = LdSinusAngle(betaResult,segbValue,gammaResult);
								segcResult = result;
								RoundResult = arrondir(result);
								segc.placeholder = RoundResult;
								checkResult(result,segcValue,segc);
							}
							// && segc existe 
							else if (!isNaN(segcValue) && segcValue != "") {
								// Calcule de sega
								result = LdSinusAngle(gammaResult,segcValue,alphaValue);
								segaResult = result;
								RoundResult = arrondir(result);
								sega.placeholder = RoundResult;
								checkResult(result,segaValue,sega);
								// Calcule de segb
								result = LdSinusAngle(gammaResult,segcValue,betaResult);
								segbResult = result;
								RoundResult = arrondir(result);
								segb.placeholder = RoundResult;
								checkResult(result,segbValue,segb);
							}
						}
						else {
							alpha.className = 'incorrect';
							tooltipStyle.display = 'inline-block';
							beta.className = 'incorrect';
							EmptyPlace("all");
						}
					}
					// sega existe
					else if (!isNaN(segaValue) && segaValue != "") {
						// && segb existe
						if (!isNaN(segbValue) && segbValue != "") {
							if (Number(segbValue) > Number(segaValue) && Number(segaValue) > Number(pythagoreHypParSin(alphaValue,segbValue))) {
								// Calcule de discriminant
								discrim = Math.pow((-2*segbValue*Math.cos(Math.radians(alphaValue))),2)-4*(Math.pow(segbValue,2)-Math.pow(segaValue,2))
								// Calcule des deux valeurs de segc possible et inscription
								result = -((-2*segbValue*Math.cos(Math.radians(alphaValue)))+Math.sqrt(discrim))/2;
								segcResult = result;
								RoundResult = arrondir(result);
								segc.placeholder = RoundResult;
								result = -((-2*segbValue*Math.cos(Math.radians(alphaValue)))-Math.sqrt(discrim))/2;
								segc2Result = result;
								RoundResult = arrondir(result);
								segc2.placeholder = RoundResult;
								// Calcule deux valeurs de beta possibles
								result = LdCosinusCote(segcResult,segaValue,segbValue);
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								result = LdCosinusCote(segc2Result,segaValue,segbValue);
								RoundResult = arrondir(result);
								beta2.placeholder = RoundResult;
								// Calcule deux valeurs de gamma possibles
								result = LdCosinusCote(segbValue,segaValue,segcResult);
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								result = LdCosinusCote(segbValue,segaValue,segc2Result);
								RoundResult = arrondir(result);
								gamma2.placeholder = RoundResult;
								// Ajout d'une zone
								segc2.style.display = 'inline-block';
								gamma2.style.display = 'inline-block';
								beta2.style.display = 'inline-block';
							}
							else if (Number(segbValue) <= Number(segaValue) || Number(segaValue) == Number(pythagoreHypParSin(alphaValue,segbValue))) {
								EmptyPlace("all");
								// Caclule de beta
								result = LdSinusCote(alphaValue,segaValue,segbValue);
								betaResult = result;
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								checkResult(result,betaValue,beta);
								// Calcule de gamma
								Euclide(alphaValue,betaResult);
								gammaResult = result;
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								checkResult(result,gammaValue,gamma);
								// Calcule de segc
								result = LdSinusAngle(alphaValue,segaValue,gammaResult);
								segcResult = result;
								RoundResult = arrondir(result);
								segc.placeholder = RoundResult;
								checkResult(result,segcValue,segc);
							}
							else {
								EmptyPlace("all");
								alpha.className = 'incorrect';
								tooltipStyle.display = 'inline-block';
							}
						}
						// && segc existe
						else if (!isNaN(segcValue) && segcValue != "") {
							if (Number(segcValue) > Number(segaValue) && Number(segaValue) > Number(pythagoreHypParSin(alphaValue,segcValue))) {
								// Calcule de discriminant
								discrim = Math.pow((-2*segcValue*Math.cos(Math.radians(alphaValue))),2)-4*(Math.pow(segcValue,2)-Math.pow(segaValue,2))
								// Calcule des deux valeurs de segb possible et inscription
								result = -((-2*segcValue*Math.cos(Math.radians(alphaValue)))+Math.sqrt(discrim))/2;
								segbResult = result;
								RoundResult = arrondir(result);
								segb.placeholder = RoundResult;
								result = -((-2*segcValue*Math.cos(Math.radians(alphaValue)))-Math.sqrt(discrim))/2;
								segb2Result = result;
								RoundResult = arrondir(result);
								segb2.placeholder = RoundResult;
								// Calcule deux valeurs de gamma possibles
								result = LdCosinusCote(segbResult,segaValue,segcValue);
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								result = LdCosinusCote(segb2Result,segaValue,segcValue);
								RoundResult = arrondir(result);
								gamma2.placeholder = RoundResult;
								// Calcule deux valeurs de beta possibles
								result = LdCosinusCote(segcValue,segaValue,segbResult);
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								result = LdCosinusCote(segcValue,segaValue,segb2Result);
								RoundResult = arrondir(result);
								beta2.placeholder = RoundResult;
								// Ajout d'une zone
								segb2.style.display = 'inline-block';
								beta2.style.display = 'inline-block';
								gamma2.style.display = 'inline-block';
							}
							else if (Number(segcValue) <= Number(segaValue) || Number(segaValue) == Number(pythagoreHypParSin(alphaValue,segcValue))) {
								EmptyPlace("all");
								// Caclule de gamma
								result = LdSinusCote(alphaValue,segaValue,segcValue);
								gammaResult = result;
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								checkResult(result,gammaValue,gamma);
								// Calcule de beta
								Euclide(alphaValue,gammaResult);
								betaResult = result;
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								checkResult(result,betaValue,beta);
								// Calcule de segb
								result = LdSinusAngle(alphaValue,segaValue,betaResult);
								segbResult = result;
								RoundResult = arrondir(result);
								segb.placeholder = RoundResult;
								checkResult(result,segbValue,segb);
							}
							else {
								EmptyPlace("all");
								alpha.className = 'incorrect';
								tooltipStyle.display = 'inline-block';
							}
						}
					}
					// segb existe
					else if (!isNaN(segbValue) && segbValue != "") {
						// && segc existe
						if (!isNaN(segcValue) && segcValue != "") {
							// Calcule de sega
							result = LdCosinus(segbValue,segcValue,alphaValue);
							segaResult = result;
							RoundResult = arrondir(result);
							sega.placeholder = RoundResult;
							checkResult(result,segaValue,sega);
							// caclule de beta
							result = LdCosinusCote(segcValue,segaResult,segbValue);
							betaResult = result;
							RoundResult = arrondir(result);
							beta.placeholder = RoundResult;
							checkResult(result,betaValue,beta);
							// caclule de gamma
							result = Euclide(alphaValue,betaResult)
							gammaResult = result;
							RoundResult = arrondir(result);
							gamma.placeholder = RoundResult;
							checkResult(result,gammaValue,gamma);
						}
					}
				}
				else if (alphaValue == String("")) {
					alpha.className = '';
					tooltipStyle.display = 'none';
					EmptyPlace("alpha");
					inputs = document.getElementsByTagName('input'),
					inputsLength = inputs.length;
					for(var i = 0; i < inputsLength; i++) {
						if (inputs[i].value != String("")) {
							id = inputs[i].id;
							check[id]();
							break;
						}
					}
				}
				else {
					alpha.className = 'incorrect';
					tooltipStyle.display = 'inline-block';
				}
			};
		
			check['beta'] = function() {

				var sega = document.getElementById('sega'),
					segb = document.getElementById('segb'),
					segc = document.getElementById('segc'),
					alpha = document.getElementById('alpha'),
					beta = document.getElementById('beta'),
					gamma = document.getElementById('gamma'),
					tooltipStyle = getTooltip(beta).style,
					segaValue = sega.value,
					segbValue = segb.value,
					segcValue = segc.value,
					alphaValue = alpha.value,
					betaValue = beta.value,
					gammaValue = gamma.value;
				
				if (!isNaN(betaValue) && Number(betaValue) >= 0 && Number(betaValue) < 180 && betaValue != String("")) {
					beta.className = 'correct';
					tooltipStyle.display = 'none';

					// alpha ou gamma existes
					if (!isNaN(alphaValue) && alphaValue != "" || !isNaN(gammaValue) && gammaValue != "") {
						if(Number(alphaValue) != 0 && Number(betaValue) + Number(alphaValue) < 180 || Number(gammaValue) != 0 && Number(betaValue) + Number(gammaValue) < 180) {
							if(!isNaN(alphaValue) && alphaValue != "") {
								// Calcule de gamma
								Euclide(alphaValue,betaValue);
								gammaResult = result;
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								checkResult(result,gammaValue,gamma);
								alphaResult = alphaValue;
							}
							else {
								// Calcule de alpha
								Euclide(betaValue,gammaValue);
								alphaResult = result;
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								checkResult(result,alphaValue,alpha);
								gammaResult = gammaValue;
							}
							// && sega existe
							if (!isNaN(segaValue) && segaValue != "") {
								// Calcule de segb
								result = LdSinusAngle(alphaResult,segaValue,betaValue);
								segbResult = result;
								RoundResult = arrondir(result);
								segb.placeholder = RoundResult;
								checkResult(result,segbValue,segb);
								// Calcule de segc
								result = LdSinusAngle(alphaResult,segaValue,gammaResult);
								segcResult = result;
								RoundResult = arrondir(result);
								segc.placeholder = RoundResult;
								checkResult(result,segcValue,segc);
							}
							// && segb existe
							else if (!isNaN(segbValue) && segbValue != "") {
								// Calcule de sega
								result = LdSinusAngle(betaValue,segbValue,alphaResult);
								segaResult = result;
								RoundResult = arrondir(result);
								sega.placeholder = RoundResult;
								checkResult(result,segaValue,sega);
								// Calcule de segc
								result = LdSinusAngle(betaValue,segbValue,gammaResult);
								segcResult = result;
								RoundResult = arrondir(result);
								segc.placeholder = RoundResult;
								checkResult(result,segcValue,segc);
							}
							// && segc existe
							else if (!isNaN(segcalue) && segcValue != "") {
								// Calcule de segb
								result = LdSinusAngle(gammaResult,segcValue,betaValue);
								segbResult = result;
								RoundResult = arrondir(result);
								segb.placeholder = RoundResult;
								checkResult(result,segbValue,segb);
								// Calcule de sega
								result = LdSinusAngle(gammaResult,segcValue,alphaResult);
								segaResult = result;
								RoundResult = arrondir(result);
								sega.placeholder = RoundResult;
								checkResult(result,segaValue,sega);
							}
						}
					}
					// sega existe
					else if (!isNaN(segaValue) && segaValue != "") {
						// && segb existe
						if (!isNaN(segbValue) && segbValue != "") {
							if (Number(segaValue) > Number(segbValue) && Number(segbValue) > Number(pythagoreHypParSin(betaValue,segaValue))) {
								//Calcule de discriminant
								discrim = Math.pow((-2*segaValue*Math.cos(Math.radians(betaValue))),2)-4*(Math.pow(segaValue,2)-Math.pow(segbValue,2))
								//Calcule des deux valeurs de segc possible et inscription
								result = -((-2*segaValue*Math.cos(Math.radians(betaValue)))+Math.sqrt(discrim))/2;
								segcResult = result;
								RoundResult = arrondir(result);
								segc.placeholder = RoundResult;
								result = -((-2*segaValue*Math.cos(Math.radians(betaValue)))-Math.sqrt(discrim))/2;
								segc2Result = result;
								RoundResult = arrondir(result);
								segc2.placeholder = RoundResult;
								// Calcule deux valeurs de alpha possibles
								result = LdCosinusCote(segcResult,segbValue,segaValue);
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								result = LdCosinusCote(segc2Result,segbValue,segaValue);
								RoundResult = arrondir(result);
								alpha2.placeholder = RoundResult;
								// Calcule deux valeurs de gamma possibles
								result = LdCosinusCote(segaValue,segbValue,segcResult);
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								result = LdCosinusCote(segaValue,segbValue,segc2Result);
								RoundResult = arrondir(result);
								gamma2.placeholder = RoundResult;
								//Ajout d'une zone
								segc2.style.display = 'inline-block';
								gamma2.style.display = 'inline-block';
								alpha2.style.display = 'inline-block';
							}
							else if (Number(segaValue) <= Number(segbValue) || Number(segbValue) == Number(pythagoreHypParSin(betaValue,segaValue))) {
								EmptyPlace("all");
								// caclule de alpha
								result = LdSinusCote(betaValue,segbValue,segaValue);
								alphaResult = result;
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								checkResult(result,alphaValue,alpha);
								// Calcule de gamma
								Euclide(betaValue,alphaResult);
								gammaResult = result;
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								checkResult(result,gammaValue,gamma);
								// Calcule de segc
								result = LdSinusAngle(betaValue,segbValue,gammaResult);
								segcResult = result;
								RoundResult = arrondir(result);
								segc.placeholder = RoundResult;
								checkResult(result,segcValue,segc);
							}
							else {
								EmptyPlace("all");
								beta.className = 'incorrect';
								tooltipStyle.display = 'inline-block';
							}
						}
						// && segc existe
						else if (!isNaN(segcValue) && segcValue != "") {
							// Calcule de segb
							result = LdCosinus(segaValue,segcValue,betaValue);
							segbResult = result;
							RoundResult = arrondir(result);
							segb.placeholder = RoundResult;
							checkResult(result,segbValue,segb);
							// caclule de gamma
							result = LdCosinusCote(segaValue,segbResult,segcValue);
							gammaResult = result;
							RoundResult = arrondir(result);
							gamma.placeholder = RoundResult;
							checkResult(result,gammaValue,gamma);
							// caclule de alpha
							result = Euclide(betaValue,gammaResult)
							alphaResult = result;
							RoundResult = arrondir(result);
							alpha.placeholder = RoundResult;
							checkResult(result,alphaValue,alpha);
						}
					}
					// segb existe
					else if (!isNaN(segbValue) && segbValue != "") {
						// && segc existe
						if (!isNaN(segcValue) && segcValue != "") {
							if (Number(segcValue) > Number(segbValue) && Number(segbValue) > Number(pythagoreHypParSin(betaValue,segcValue))) {
								//Calcule de discriminant
								discrim = Math.pow((-2*segcValue*Math.cos(Math.radians(betaValue))),2)-4*(Math.pow(segcValue,2)-Math.pow(segbValue,2))
								//Calcule des deux valeurs de sega possible et inscription
								result = -((-2*segcValue*Math.cos(Math.radians(betaValue)))+Math.sqrt(discrim))/2;
								segaResult = result;
								RoundResult = arrondir(result);
								sega.placeholder = RoundResult;
								result = -((-2*segcValue*Math.cos(Math.radians(betaValue)))-Math.sqrt(discrim))/2;
								sega2Result = result;
								RoundResult = arrondir(result);
								sega2.placeholder = RoundResult;
								// Calcule deux valeurs de gamma possibles
								result = LdCosinusCote(segaResult,segbValue,segcValue);
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								result = LdCosinusCote(sega2Result,segbValue,segcValue);
								RoundResult = arrondir(result);
								gamma2.placeholder = RoundResult;
								// Calcule deux valeurs de alpha possibles
								result = LdCosinusCote(segcValue,segbValue,segaResult);
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								result = LdCosinusCote(segcValue,segbValue,sega2Result);
								RoundResult = arrondir(result);
								alpha2.placeholder = RoundResult;
								//Ajout d'une zone
								sega2.style.display = 'inline-block';
								alpha2.style.display = 'inline-block';
								gamma2.style.display = 'inline-block';
							}
							else if (Number(segcValue) <= Number(segbValue) || Number(segbValue) == Number(pythagoreHypParSin(betaValue,segcValue))) {
								EmptyPlace("all");
								// caclule de gamma
								result = LdSinusCote(betaValue,segbValue,segcValue);
								gammaResult = result;
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								checkResult(result,gammaValue,gamma);
								// Calcule de alpha
								Euclide(betaValue,gammaResult);
								alphaResult = result;
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								checkResult(result,alphaValue,alpha);
								// Calcule de sega
								result = LdSinusAngle(betaValue,segbValue,alphaResult);
								segaResult = result;
								RoundResult = arrondir(result);
								sega.placeholder = RoundResult;
								checkResult(result,segaValue,sega);
							}
							else {
								EmptyPlace("all");
								beta.className = 'incorrect';
								tooltipStyle.display = 'inline-block';
							}
						}
					}

				}
				else if (betaValue == String("")) {
					beta.className = '';
					tooltipStyle.display = 'none';
					EmptyPlace("beta");
					inputs = document.getElementsByTagName('input'),
					inputsLength = inputs.length;
					for(var i = 0; i < inputsLength; i++) {
						if (inputs[i].value != String("")) {
							id = inputs[i].id;
							check[id]();
							break;
						}
					}
				}
				else {
					beta.className = 'incorrect';
					tooltipStyle.display = 'inline-block';
				}
			};
			
			check['gamma'] = function() {

				var sega = document.getElementById('sega'),
					segb = document.getElementById('segb'),
					segc = document.getElementById('segc'),
					alpha = document.getElementById('alpha'),
					beta = document.getElementById('beta'),
					gamma = document.getElementById('gamma'),
					tooltipStyle = getTooltip(gamma).style,
					segaValue = sega.value,
					segbValue = segb.value,
					segcValue = segc.value,
					alphaValue = alpha.value,
					betaValue = beta.value,
					gammaValue = gamma.value;
				
				if (!isNaN(gammaValue) && Number(gammaValue) >= 0 && Number(gammaValue) < 180 && gammaValue != String("")) {
					gamma.className = 'correct';
					tooltipStyle.display = 'none';

					// alpha ou beta existes
					if (!isNaN(alphaValue) && alphaValue != "" || !isNaN(betaValue) && betaValue != "") {
						if(Number(alphaValue) != 0 && Number(gammaValue) + Number(alphaValue) < 180 || Number(betaValue) != 0 && Number(gammaValue) + Number(betaValue) < 180) {
							if(!isNaN(alphaValue) && alphaValue != "") {
								// Calcule de beta
								Euclide(gammaValue,alphaValue);
								betaResult = result;
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								checkResult(result,gammaValue,beta);
								alphaResult = alphaValue;
							}
							else {
								// Calcule de alpha
								Euclide(gammaValue,betaValue);
								alphaResult = result;
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								checkResult(result,alphaValue,alpha);
								betaResult = betaValue;
							}
							// && sega existe
							if (!isNaN(segaValue) && segaValue != "") {
								// Calcule de segb
								result = LdSinusAngle(gammaValue,segaValue,alphaResult);
								segbResult = result;
								RoundResult = arrondir(result);
								segb.placeholder = RoundResult;
								checkResult(result,segbValue,segb);
								// Calcule de segc
								result = LdSinusAngle(gammaValue,segaValue,betaResult);
								segcResult = result;
								RoundResult = arrondir(result);
								segc.placeholder = RoundResult;
								checkResult(result,segcValue,segc);
							}
							// && segb existe
							if (!isNaN(segbValue) && segbValue != "") {
								// Calcule de segc
								result = LdSinusAngle(betaResult,segbValue,gammaValue);
								segcResult = result;
								RoundResult = arrondir(result);
								segc.placeholder = RoundResult;
								checkResult(result,segcValue,segc);
								// Calcule de sega
								result = LdSinusAngle(betaResult,segbValue,alphaResult);
								segaResult = result;
								RoundResult = arrondir(result);
								sega.placeholder = RoundResult;
								checkResult(result,segaValue,sega);
							}
							// && segc existe
							if (!isNaN(segcValue) && segcValue != "") {
								// Calcule de sega
								result = LdSinusAngle(gammaValue,segcValue,alphaValue);
								segaResult = result;
								RoundResult = arrondir(result);
								sega.placeholder = RoundResult;
								checkResult(result,segaValue,sega);
								// Calcule de segb
								result = LdSinusAngle(gammaValue,segcValue,betaResult);
								segbResult = result;
								RoundResult = arrondir(result);
								segb.placeholder = RoundResult;
								checkResult(result,segbValue,segb);
							}
						}
					}
					
					// sega existe
					if (!isNaN(segaValue) && segaValue != "") {
						// && segb existe
						if (!isNaN(segbValue) && segbValue != "") {
							// Calcule de segc
							result = LdCosinus(segaValue,segbValue,gammaValue);
							segcResult = result;
							RoundResult = arrondir(result);
							segc.placeholder = RoundResult;
							checkResult(result,segcValue,segc);
							// caclule de alpha
							result = LdCosinusCote(segbValue,segcResult,segaValue);
							alphaResult = result;
							RoundResult = arrondir(result);
							alpha.placeholder = RoundResult;
							checkResult(result,alphaValue,alpha);
							// caclule de beta
							result = Euclide(alphaResult,gammaValue)
							betaResult = result;
							RoundResult = arrondir(result);
							beta.placeholder = RoundResult;
							checkResult(result,betaValue,beta);
						}
						// && segc existe
						if (!isNaN(segcValue) && segcValue != "") {
							if (Number(segaValue) > Number(segcValue) && Number(segcValue) > Number(pythagoreHypParSin(gammaValue,segaValue))) {
								//Calcule de discriminant
								discrim = Math.pow((-2*segaValue*Math.cos(Math.radians(gammaValue))),2)-4*(Math.pow(segaValue,2)-Math.pow(segcValue,2))
								//Calcule des deux valeurs de sega possible et inscription
								result = -((-2*segaValue*Math.cos(Math.radians(gammaValue)))+Math.sqrt(discrim))/2;
								segbResult = result;
								RoundResult = arrondir(result);
								segb.placeholder = RoundResult;
								result = -((-2*segaValue*Math.cos(Math.radians(gammaValue)))-Math.sqrt(discrim))/2;
								segb2Result = result;
								RoundResult = arrondir(result);
								segb2.placeholder = RoundResult;
								// Calcule deux valeurs de alpha possibles
								result = LdCosinusCote(segbResult,segcValue,segaValue);
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								result = LdCosinusCote(segb2Result,segcValue,segaValue);
								RoundResult = arrondir(result);
								alpha2.placeholder = RoundResult;
								// Calcule deux valeurs de beta possibles
								result = LdCosinusCote(segaValue,segcValue,segbResult);
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								result = LdCosinusCote(segaValue,segcValue,segb2Result);
								RoundResult = arrondir(result);
								beta2.placeholder = RoundResult;
								//Ajout d'une zone
								segb2.style.display = 'inline-block';
								alpha2.style.display = 'inline-block';
								beta2.style.display = 'inline-block';
							}
							else if (Number(segaValue) <= Number(segcValue) || Number(segcValue) == Number(pythagoreHypParSin(gammaValue,segaValue))) {
								EmptyPlace("all");
								// caclule de alpha
								result = LdSinusCote(gammaValue,segcValue,segaValue);
								betaResult = result;
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								checkResult(result,alphaValue,alpha);
								// Calcule de beta
								Euclide(gammaValue,alphaResult);
								betaResult = result;
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								checkResult(result,betaValue,beta);
								// Calcule de segb
								result = LdSinusAngle(gammaValue,segcValue,betaResult);
								segbResult = result;
								RoundResult = arrondir(result);
								segb.placeholder = RoundResult;
								checkResult(result,segbValue,segb);
							}
							else {
								EmptyPlace("all");
								gamma.className = 'incorrect';
								tooltipStyle.display = 'inline-block';
							}
						}
					}
						
					// segb existe
					if (!isNaN(segbValue) && segbValue != "") {
						// && segc existe
						if (!isNaN(segcValue) && segcValue != "") {
							if (Number(segbValue) > Number(segcValue) && Number(segcValue) > Number(pythagoreHypParSin(gammaValue,segbValue))) {
								//Calcule de discriminant
								discrim = Math.pow((-2*segbValue*Math.cos(Math.radians(gammaValue))),2)-4*(Math.pow(segbValue,2)-Math.pow(segcValue,2))
								//Calcule des deux valeurs de sega possible et inscription
								result = -((-2*segbValue*Math.cos(Math.radians(gammaValue)))+Math.sqrt(discrim))/2;
								segaResult = result;
								RoundResult = arrondir(result);
								sega.placeholder = RoundResult;
								result = -((-2*segbValue*Math.cos(Math.radians(gammaValue)))-Math.sqrt(discrim))/2;
								sega2Result = result;
								RoundResult = arrondir(result);
								sega2.placeholder = RoundResult;
								// Calcule deux valeurs de beta possibles
								result = LdCosinusCote(segaResult,segcValue,segbValue);
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								result = LdCosinusCote(sega2Result,segcValue,segbValue);
								RoundResult = arrondir(result);
								beta2.placeholder = RoundResult;
								// Calcule deux valeurs de alpha possibles
								result = LdCosinusCote(segbValue,segcValue,segaResult);
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								result = LdCosinusCote(segbValue,segcValue,sega2Result);
								RoundResult = arrondir(result);
								alpha2.placeholder = RoundResult;
								//Ajout d'une zone
								sega2.style.display = 'inline-block';
								alpha2.style.display = 'inline-block';
								beta2.style.display = 'inline-block';
							}
							else if (Number(segbValue) <= Number(segcValue) || Number(segcValue) == Number(pythagoreHypParSin(gammaValue,segbValue))) {
								EmptyPlace("all");
								// caclule de beta
								result = LdSinusCote(gammaValue,segcValue,segbValue);
								betaResult = result;
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								checkResult(result,betaValue,beta);
								// Calcule de alpha
								Euclide(gammaValue,betaResult);
								alphaResult = result;
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								checkResult(result,alphaValue,alpha);
								// Calcule de sega
								result = LdSinusAngle(gammaValue,segcValue,alphaResult);
								segaResult = result;
								RoundResult = arrondir(result);
								sega.placeholder = RoundResult;
								checkResult(result,segaValue,sega);
							}
							else {
								EmptyPlace("all");
								gamma.className = 'incorrect';
								tooltipStyle.display = 'inline-block';
							}
						}
					}

				} else if (gammaValue == String("")) {
					gamma.className = '';
					tooltipStyle.display = 'none';
					EmptyPlace("gamma");
					inputs = document.getElementsByTagName('input'),
					inputsLength = inputs.length;
					for(var i = 0; i < inputsLength; i++) {
						if (inputs[i].value != String("")) {
							id = inputs[i].id;
							check[id]();
							break;
						}
					}
				} else {
					gamma.className = 'incorrect';
					tooltipStyle.display = 'inline-block';
				}
			};
			
			check['sega'] = function() {
				
				var sega = document.getElementById('sega'),
					sega2 = document.getElementById('sega2'),
					segb = document.getElementById('segb'),
					segb2 = document.getElementById('segb2'),
					segc = document.getElementById('segc'),
					segc2 = document.getElementById('segc2'),
					alpha = document.getElementById('alpha'),
					alpha2 = document.getElementById('alpha2'),
					beta = document.getElementById('beta'),
					beta2 = document.getElementById('beta2'),
					gamma = document.getElementById('gamma'),
					gamma2 = document.getElementById('gamma2'),
					tooltipStyle = getTooltip(sega).style,
					segaValue = sega.value,
					segbValue = segb.value,
					segcValue = segc.value,
					alphaValue = alpha.value,
					betaValue = beta.value,
					gammaValue = gamma.value;
				
				// sega existe
				if (!isNaN(segaValue) && Number(segaValue) >= 0 && segaValue != String("")) {
					sega.className = 'correct';
					tooltipStyle.display = 'none';
					
					// alpha existe
					if (!isNaN(alphaValue) && alphaValue != "") {
						// && beta existe
						if (!isNaN(betaValue) && betaValue != "") {
							// Calcule de segb
							result = LdSinusAngle(alphaValue,segaValue,betaValue);
							segbResult = result;
							RoundResult = arrondir(result);
							segb.placeholder = RoundResult;
							checkResult(result,segbValue,segb);
							// Calcule de segc
							result = LdSinusAngle(alphaValue,segaValue,gammaResult);
							segcResult = result;
							RoundResult = arrondir(result);
							segc.placeholder = RoundResult;
							checkResult(result,segcValue,segc);
						}
						// && gamma existe
						else if (!isNaN(gammaValue) && gammaValue != "") {
							// Calcule de segc
							result = LdSinusAngle(alphaValue,segaValue,gammaValue);
							segcResult = result;
							RoundResult = arrondir(result);
							segc.placeholder = RoundResult;
							checkResult(result,segcValue,segc);
							// Calcule de segb
							result = LdSinusAngle(alphaValue,segaValue,betaResult);
							segbResult = result;
							RoundResult = arrondir(result);
							segb.placeholder = RoundResult;
							checkResult(result,segbValue,segb);
						}
						// && segb existe
						else if (!isNaN(segbValue) && segbValue != "") {
							if (Number(segbValue) > Number(segaValue) && Number(segaValue) > Number(pythagoreHypParSin(alphaValue,segbValue))) {
								//Calcule de discriminant
								discrim = Math.pow((-2*segbValue*Math.cos(Math.radians(alphaValue))),2)-4*(Math.pow(segbValue,2)-Math.pow(segaValue,2))
								//Calcule des deux valeurs de segc possible et inscription
								result = -((-2*segbValue*Math.cos(Math.radians(alphaValue)))+Math.sqrt(discrim))/2;
								segcResult = result;
								RoundResult = arrondir(result);
								segc.placeholder = RoundResult;
								result = -((-2*segbValue*Math.cos(Math.radians(alphaValue)))-Math.sqrt(discrim))/2;
								segc2Result = result;
								RoundResult = arrondir(result);
								segc2.placeholder = RoundResult;
								// Calcule deux valeurs de beta possibles
								result = LdCosinusCote(segcResult,segaValue,segbValue);
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								result = LdCosinusCote(segc2Result,segaValue,segbValue);
								RoundResult = arrondir(result);
								beta2.placeholder = RoundResult;
								// Calcule deux valeurs de gamma possibles
								result = LdCosinusCote(segbValue,segaValue,segcResult);
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								result = LdCosinusCote(segbValue,segaValue,segc2Result);
								RoundResult = arrondir(result);
								gamma2.placeholder = RoundResult;
								//Ajout d'une zone
								segc2.style.display = 'inline-block';
								gamma2.style.display = 'inline-block';
								beta2.style.display = 'inline-block';
							}
							else if (Number(segbValue) <= Number(segaValue) || Number(segaValue) == Number(pythagoreHypParSin(alphaValue,segbValue))) {
								EmptyPlace("all");
								// caclule de beta
								result = LdSinusCote(alphaValue,segaValue,segbValue);
								betaResult = result;
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								checkResult(result,betaValue,beta);
								// Calcule de gamma
								Euclide(alphaValue,betaResult);
								gammaResult = result;
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								checkResult(result,gammaValue,gamma);
								// Calcule de segc
								result = LdSinusAngle(alphaValue,segaValue,gammaResult);
								segcResult = result;
								RoundResult = arrondir(result);
								segc.placeholder = RoundResult;
								checkResult(result,segcValue,segc);
							}
							else {
								sega.className = 'incorrect';
								tooltipStyle.display = 'inline-block';
							}
						}
						// && segc existe
						else if (!isNaN(segcValue) && segcValue != "") {
							if (Number(segcValue) > Number(segaValue) && Number(segaValue) > Number(pythagoreHypParSin(alphaValue,segcValue))) {
								//Calcule de discriminant
								discrim = Math.pow((-2*segcValue*Math.cos(Math.radians(alphaValue))),2)-4*(Math.pow(segcValue,2)-Math.pow(segaValue,2))
								//Calcule des deux valeurs de segb possible et inscription
								result = -((-2*segcValue*Math.cos(Math.radians(alphaValue)))+Math.sqrt(discrim))/2;
								segbResult = result;
								RoundResult = arrondir(result);
								segb.placeholder = RoundResult;
								result = -((-2*segcValue*Math.cos(Math.radians(alphaValue)))-Math.sqrt(discrim))/2;
								segb2Result = result;
								RoundResult = arrondir(result);
								segb2.placeholder = RoundResult;
								// Calcule deux valeurs de gamma possibles
								result = LdCosinusCote(segbResult,segaValue,segcValue);
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								result = LdCosinusCote(segb2Result,segaValue,segcValue);
								RoundResult = arrondir(result);
								gamma2.placeholder = RoundResult;
								// Calcule deux valeurs de beta possibles
								result = LdCosinusCote(segcValue,segaValue,segbResult);
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								result = LdCosinusCote(segcValue,segaValue,segb2Result);
								RoundResult = arrondir(result);
								beta2.placeholder = RoundResult;
								//Ajout d'une zone
								segb2.style.display = 'inline-block';
								beta2.style.display = 'inline-block';
								gamma2.style.display = 'inline-block';
							}
							else if (Number(segcValue) <= Number(segaValue) || Number(segaValue) == Number(pythagoreHypParSin(alphaValue,segcValue))) {
								EmptyPlace("all");
								// caclule de gamma
								result = LdSinusCote(alphaValue,segaValue,segcValue);
								gammaResult = result;
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								checkResult(result,gammaValue,gamma);
								// Calcule de beta
								Euclide(alphaValue,gammaResult);
								betaResult = result;
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								checkResult(result,betaValue,beta);
								// Calcule de segb
								result = LdSinusAngle(alphaValue,segaValue,betaResult);
								segbResult = result;
								RoundResult = arrondir(result);
								segb.placeholder = RoundResult;
								checkResult(result,segbValue,segb);
							}
							else {
								sega.className = 'incorrect';
								tooltipStyle.display = 'inline-block';
							}
						}
					}
					// beta existe
					else if (!isNaN(betaValue) && betaValue != "") {
						// && gamma existe
						if (!isNaN(gammaValue) && gammaValue != "") {
							// Calcule de segc
							result = LdSinusAngle(alphaResult,segaValue,gammaValue);
							segcResult = result;
							RoundResult = arrondir(result);
							segc.placeholder = RoundResult;
							checkResult(result,segcValue,segc);
							// Calcule de segb
							result = LdSinusAngle(alphaResult,segaValue,betaValue);
							segbResult = result;
							RoundResult = arrondir(result);
							segb.placeholder = RoundResult;
							checkResult(result,segbValue,segb);
						}
						// && segb existe
						else if (!isNaN(segbValue) && segbValue != "") {
							if (Number(segaValue) > Number(segbValue) && Number(segbValue) > Number(pythagoreHypParSin(betaValue,segaValue))) {
								//Calcule de discriminant
								discrim = Math.pow((-2*segaValue*Math.cos(Math.radians(betaValue))),2)-4*(Math.pow(segaValue,2)-Math.pow(segbValue,2))
								//Calcule des deux valeurs de segc possible et inscription
								result = -((-2*segaValue*Math.cos(Math.radians(betaValue)))+Math.sqrt(discrim))/2;
								segcResult = result;
								RoundResult = arrondir(result);
								segc.placeholder = RoundResult;
								result = -((-2*segaValue*Math.cos(Math.radians(betaValue)))-Math.sqrt(discrim))/2;
								segc2Result = result;
								RoundResult = arrondir(result);
								segc2.placeholder = RoundResult;
								// Calcule deux valeurs de alpha possibles
								result = LdCosinusCote(segcResult,segbValue,segaValue);
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								result = LdCosinusCote(segc2Result,segbValue,segaValue);
								RoundResult = arrondir(result);
								alpha2.placeholder = RoundResult;
								// Calcule deux valeurs de gamma possibles
								result = LdCosinusCote(segaValue,segbValue,segcResult);
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								result = LdCosinusCote(segaValue,segbValue,segc2Result);
								RoundResult = arrondir(result);
								gamma2.placeholder = RoundResult;
								//Ajout d'une zone
								segc2.style.display = 'inline-block';
								gamma2.style.display = 'inline-block';
								alpha2.style.display = 'inline-block';
							}
							else if (Number(segaValue) <= Number(segbValue) || Number(segbValue) == Number(pythagoreHypParSin(betaValue,segaValue))) {
								EmptyPlace("all");
								// caclule de alpha
								result = LdSinusCote(betaValue,segbValue,segaValue);
								alphaResult = result;
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								checkResult(result,alphaValue,alpha);
								// Calcule de gamma
								Euclide(betaValue,alphaResult);
								gammaResult = result;
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								checkResult(result,gammaValue,gamma);
								// Calcule de segc
								result = LdSinusAngle(betaValue,segbValue,gammaResult);
								segcResult = result;
								RoundResult = arrondir(result);
								segc.placeholder = RoundResult;
								checkResult(result,segcValue,segc);
							}
							else {
								segb.className = 'incorrect';
								tooltipStyle.display = 'inline-block';
							}
						}
						// && segc existe
						else if (!isNaN(segcValue) && segcValue != "") {
							// cellules justes
							beta.className = 'correct';
							segc.className = 'correct';
							// Calcule de segb
							result = LdCosinus(segaValue,segcValue,betaValue);
							segbResult = result;
							RoundResult = arrondir(result);
							segb.placeholder = RoundResult;
							checkResult(result,segbValue,segb);
							// caclule de gamma
							result = LdCosinusCote(segaValue,segbResult,segcValue);
							gammaResult = result;
							RoundResult = arrondir(result);
							gamma.placeholder = RoundResult;
							checkResult(result,gammaValue,gamma);
							// caclule de alpha
							result = Euclide(gammaResult,betaValue)
							alphaResult = result;
							RoundResult = arrondir(result);
							alpha.placeholder = RoundResult;
							checkResult(result,alphaValue,alpha);
						}
					}
					// gamma existe
					else if (!isNaN(gammaValue) && gammaValue != "") {
						// && segb existe
						if (!isNaN(segbValue) && segbValue != "") {
							// cellules justes
							gamma.className = 'correct';
							segb.className = 'correct';
							// Calcule de segc
							result = LdCosinus(segaValue,segbValue,gammaValue);
							segcResult = result;
							RoundResult = arrondir(result);
							segc.placeholder = RoundResult;
							checkResult(result,segcValue,segc);
							// caclule de beta
							result = LdCosinusCote(segaValue,segcResult,segbValue);
							betaResult = result;
							RoundResult = arrondir(result);
							beta.placeholder = RoundResult;
							checkResult(result,betaValue,beta);
							// caclule de alpha
							result = Euclide(betaResult,gammaValue)
							alphaResult = result;
							RoundResult = arrondir(result);
							alpha.placeholder = RoundResult;
							checkResult(result,alphaValue,alpha);
						}
						// && segc existe
						else if (!isNaN(segcValue) && segcValue != "") {
							if (Number(segaValue) > Number(segcValue) && Number(segcValue) > Number(pythagoreHypParSin(gammaValue,segaValue))) {
								//Calcule de discriminant
								discrim = Math.pow((-2*segaValue*Math.cos(Math.radians(gammaValue))),2)-4*(Math.pow(segaValue,2)-Math.pow(segcValue,2))
								//Calcule des deux valeurs de sega possible et inscription
								result = -((-2*segaValue*Math.cos(Math.radians(gammaValue)))+Math.sqrt(discrim))/2;
								segbResult = result;
								RoundResult = arrondir(result);
								segb.placeholder = RoundResult;
								result = -((-2*segaValue*Math.cos(Math.radians(gammaValue)))-Math.sqrt(discrim))/2;
								segb2Result = result;
								RoundResult = arrondir(result);
								segb2.placeholder = RoundResult;
								// Calcule deux valeurs de alpha possibles
								result = LdCosinusCote(segbResult,segcValue,segaValue);
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								result = LdCosinusCote(segb2Result,segcValue,segaValue);
								RoundResult = arrondir(result);
								alpha2.placeholder = RoundResult;
								// Calcule deux valeurs de beta possibles
								result = LdCosinusCote(segaValue,segcValue,segbResult);
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								result = LdCosinusCote(segaValue,segcValue,segb2Result);
								RoundResult = arrondir(result);
								beta2.placeholder = RoundResult;
								//Ajout d'une zone
								segb2.style.display = 'inline-block';
								alpha2.style.display = 'inline-block';
								beta2.style.display = 'inline-block';
							}
							else if (Number(segaValue) <= Number(segcValue) || Number(segcValue) == Number(pythagoreHypParSin(gammaValue,segaValue))) {
								EmptyPlace("all");
								// caclule de alpha
								result = LdSinusCote(gammaValue,segcValue,segaValue);
								alphaResult = result;
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								checkResult(result,alphaValue,alpha);
								// Calcule de beta
								Euclide(gammaValue,alphaResult);
								betaResult = result;
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								checkResult(result,betaValue,beta);
								// Calcule de segb
								result = LdSinusAngle(gammaValue,segcValue,betaResult);
								segbResult = result;
								RoundResult = arrondir(result);
								segb.placeholder = RoundResult;
								checkResult(result,segbValue,segb);
							}
							else {
								segc.className = 'incorrect';
								tooltipStyle.display = 'inline-block';
							}
						}
					}
					// segb existe
					else if (!isNaN(segbValue) && segbValue != "") {
						// && segc existe
						if (!isNaN(segcValue) && segcValue != "") {
							if(Number(segaValue) < Number(segbValue) + Number(segcValue) && Number(segaValue) > Math.abs(Number(segbValue) - Number(segcValue))) {
								sega.className = 'correct';
								segb.className = 'correct';
								segb.nextSibling.nextSibling.nextSibling.nextSibling.style.display='none';
								segc.className = 'correct';
								segc.nextSibling.nextSibling.nextSibling.nextSibling.style.display='none';
								// Calcule de alpha
								result = LdCosinusCote(segbValue,segcValue,segaValue);
								alphaResult = result;
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								checkResult(result,alphaValue,alpha);
								// Calcule de beta
								result = LdCosinusCote(segaValue,segcValue,segbValue);
								betaResult = result;
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								checkResult(result,betaValue,beta);
								// Calcule de gamma
								result = LdCosinusCote(segaValue,segbValue,segcValue);
								gammaResult = result;
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								checkResult(result,gammaValue,gamma);
							}
							else {
								sega.className = 'incorrect';
								tooltipStyle.display = 'inline-block';
								segb.className = 'incorrect';
								segc.className = 'incorrect';
								EmptyPlace("all");
							}
						}
					}

				}
				else if (segaValue == String("")) {
					sega.className = '';
					tooltipStyle.display = 'none';
					EmptyPlace("a");
					inputs = document.getElementsByTagName('input'),
					inputsLength = inputs.length;
					for(var i = 0; i < inputsLength; i++) {
						if (inputs[i].value != String("")) {
							id = inputs[i].id;
							check[id]();
							break;
						}
					}
				}
				else {
					sega.className = 'incorrect';
					tooltipStyle.display = 'inline-block';
				}
			};
		
			check['segb'] = function() {

				var sega = document.getElementById('sega'),
					segb = document.getElementById('segb'),
					segc = document.getElementById('segc'),
					alpha = document.getElementById('alpha'),
					beta = document.getElementById('beta'),
					gamma = document.getElementById('gamma'),
					tooltipStyle = getTooltip(segb).style,
					segaValue = sega.value,
					segbValue = segb.value,
					segcValue = segc.value,
					alphaValue = alpha.value,
					betaValue = beta.value,
					gammaValue = gamma.value;
				
				// segb existe
				if (!isNaN(segbValue) && Number(segbValue) >= 0 && segbValue != String("")) {
					segb.className = 'correct';
					tooltipStyle.display = 'none';

					// alpha existe
					if (!isNaN(alphaValue) && alphaValue != "") {
						// && beta existe
						if (!isNaN(betaValue) && betaValue != "") {
							// Calcule de sega
							result = LdSinusAngle(betaValue,segbValue,alphaValue);
							segaResult = result;
							RoundResult = arrondir(result);
							sega.placeholder = RoundResult;
							checkResult(result,segaValue,sega);
							// Calcule de segc
							result = LdSinusAngle(betaValue,segbValue,gammaResult);
							segcResult = result;
							RoundResult = arrondir(result);
							segc.placeholder = RoundResult;
							checkResult(result,segcValue,segc);
						}
						// && gamma existe
						else if (!isNaN(gammaValue) && gammaValue != "") {
							// Calcule de sega
							result = LdSinusAngle(betaResult,segbValue,alphaValue);
							segaResult = result;
							RoundResult = arrondir(result);
							sega.placeholder = RoundResult;
							checkResult(result,segaValue,sega);
							// Calcule de segc
							result = LdSinusAngle(betaResult,segbValue,gammaValue);
							segcResult = result;
							RoundResult = arrondir(result);
							segc.placeholder = RoundResult;
							checkResult(result,segcValue,segc);
						}
						// && sega existe
						else if (!isNaN(segaValue) && segaValue != "") {
							if (Number(segbValue) > Number(segaValue) && Number(segaValue) > Number(pythagoreHypParSin(alphaValue,segbValue))) {
								//Calcule de discriminant
								discrim = Math.pow((-2*segbValue*Math.cos(Math.radians(alphaValue))),2)-4*(Math.pow(segbValue,2)-Math.pow(segaValue,2))
								//Calcule des deux valeurs de segc possible et inscription
								result = -((-2*segbValue*Math.cos(Math.radians(alphaValue)))+Math.sqrt(discrim))/2;
								segcResult = result;
								RoundResult = arrondir(result);
								segc.placeholder = RoundResult;
								result = -((-2*segbValue*Math.cos(Math.radians(alphaValue)))-Math.sqrt(discrim))/2;
								segc2Result = result;
								RoundResult = arrondir(result);
								segc2.placeholder = RoundResult;
								// Calcule deux valeurs de beta possibles
								result = LdCosinusCote(segcResult,segaValue,segbValue);
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								result = LdCosinusCote(segc2Result,segaValue,segbValue);
								RoundResult = arrondir(result);
								beta2.placeholder = RoundResult;
								// Calcule deux valeurs de gamma possibles
								result = LdCosinusCote(segbValue,segaValue,segcResult);
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								result = LdCosinusCote(segbValue,segaValue,segc2Result);
								RoundResult = arrondir(result);
								gamma2.placeholder = RoundResult;
								//Ajout d'une zone
								segc2.style.display = 'inline-block';
								gamma2.style.display = 'inline-block';
								beta2.style.display = 'inline-block';
							}
							else if (Number(segbValue) <= Number(segaValue) || Number(segaValue) == Number(pythagoreHypParSin(alphaValue,segbValue))) {
								EmptyPlace("all");
								// caclule de beta
								result = LdSinusCote(alphaValue,segaValue,segbValue);
								betaResult = result;
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								checkResult(result,betaValue,beta);
								// Calcule de gamma
								Euclide(alphaValue,betaResult);
								gammaResult = result;
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								checkResult(result,gammaValue,gamma);
								// Calcule de segc
								result = LdSinusAngle(alphaValue,segaValue,gammaResult);
								segcResult = result;
								RoundResult = arrondir(result);
								segc.placeholder = RoundResult;
								checkResult(result,segcValue,segc);
							}
							else {
								sega.className = 'incorrect';
								tooltipStyle.display = 'inline-block';
							}
						}
						// && segc existe
						else if (!isNaN(segcValue) && segcValue != "") {
							alpha.className = 'correct';
							segc.className = 'correct';
							// Calcule de sega
							result = LdCosinus(segbValue,segcValue,alphaValue);
							segaResult = result;
							RoundResult = arrondir(result);
							sega.placeholder = RoundResult;
							checkResult(result,segaValue,sega);
							// caclule de gamma
							result = LdCosinusCote(segbValue,segaResult,segcValue);
							gammaResult = result;
							RoundResult = arrondir(result);
							gamma.placeholder = RoundResult;
							checkResult(result,gammaValue,gamma);
							// Caclule de beta
							result = Euclide(gammaResult,alphaValue)
							betaResult = result;
							RoundResult = arrondir(result);
							beta.placeholder = RoundResult;
							checkResult(result,betaValue,beta);
						}
					}
					// beta existe
					else if (!isNaN(betaValue) && betaValue != "") {
						// && gamma existe
						if (!isNaN(gammaValue) && gammaValue != "") {
							// Calcule de segc
							result = LdSinusAngle(betaValue,segbValue,gammaValue);
							segcResult = result;
							RoundResult = arrondir(result);
							segc.placeholder = RoundResult;
							checkResult(result,segcValue,segc);
							// Calcule de sega
							result = LdSinusAngle(betaValue,segbValue,alphaResult);
							segaResult = result;
							RoundResult = arrondir(result);
							sega.placeholder = RoundResult;
							checkResult(result,segaValue,sega);
						}
						// && sega existe
						else if (!isNaN(segaValue) && segaValue != "") {
							if (Number(segaValue) > Number(segbValue) && Number(segbValue) > Number(pythagoreHypParSin(betaValue,segaValue))) {
								//Calcule de discriminant
								discrim = Math.pow((-2*segaValue*Math.cos(Math.radians(betaValue))),2)-4*(Math.pow(segaValue,2)-Math.pow(segbValue,2))
								//Calcule des deux valeurs de segc possible et inscription
								result = -((-2*segaValue*Math.cos(Math.radians(betaValue)))+Math.sqrt(discrim))/2;
								segcResult = result;
								RoundResult = arrondir(result);
								segc.placeholder = RoundResult;
								result = -((-2*segaValue*Math.cos(Math.radians(betaValue)))-Math.sqrt(discrim))/2;
								segc2Result = result;
								RoundResult = arrondir(result);
								segc2.placeholder = RoundResult;
								// Calcule deux valeurs de alpha possibles
								result = LdCosinusCote(segcResult,segbValue,segaValue);
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								result = LdCosinusCote(segc2Result,segbValue,segaValue);
								RoundResult = arrondir(result);
								alpha2.placeholder = RoundResult;
								// Calcule deux valeurs de gamma possibles
								result = LdCosinusCote(segaValue,segbValue,segcResult);
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								result = LdCosinusCote(segaValue,segbValue,segc2Result);
								RoundResult = arrondir(result);
								gamma2.placeholder = RoundResult;
								//Ajout d'une zone
								segc2.style.display = 'inline-block';
								gamma2.style.display = 'inline-block';
								alpha2.style.display = 'inline-block';
							}
							else if (Number(segaValue) <= Number(segbValue) || Number(segbValue) == Number(pythagoreHypParSin(betaValue,segaValue))) {
								EmptyPlace("all");
								// caclule de alpha
								result = LdSinusCote(betaValue,segbValue,segaValue);
								alphaResult = result;
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								checkResult(result,alphaValue,alpha);
								// Calcule de gamma
								Euclide(betaValue,alphaResult);
								gammaResult = result;
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								checkResult(result,gammaValue,gamma);
								// Calcule de segc
								result = LdSinusAngle(betaValue,segbValue,gammaResult);
								segcResult = result;
								RoundResult = arrondir(result);
								segc.placeholder = RoundResult;
								checkResult(result,segcValue,segc);
							}
							else {
								segb.className = 'incorrect';
								tooltipStyle.display = 'inline-block';
							}
						}
						// && segc existe
						else if (!isNaN(segcValue) && segcValue != "") {
							if (Number(segcValue) > Number(segbValue) && Number(segbValue) > Number(pythagoreHypParSin(betaValue,segcValue))) {
								//Calcule de discriminant
								discrim = Math.pow((-2*segcValue*Math.cos(Math.radians(betaValue))),2)-4*(Math.pow(segcValue,2)-Math.pow(segbValue,2))
								//Calcule des deux valeurs de sega possible et inscription
								result = -((-2*segcValue*Math.cos(Math.radians(betaValue)))+Math.sqrt(discrim))/2;
								segaResult = result;
								RoundResult = arrondir(result);
								sega.placeholder = RoundResult;
								result = -((-2*segcValue*Math.cos(Math.radians(betaValue)))-Math.sqrt(discrim))/2;
								sega2Result = result;
								RoundResult = arrondir(result);
								sega2.placeholder = RoundResult;
								// Calcule deux valeurs de gamma possibles
								result = LdCosinusCote(segaResult,segbValue,segcValue);
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								result = LdCosinusCote(sega2Result,segbValue,segcValue);
								RoundResult = arrondir(result);
								gamma2.placeholder = RoundResult;
								// Calcule deux valeurs de alpha possibles
								result = LdCosinusCote(segcValue,segbValue,segaResult);
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								result = LdCosinusCote(segcValue,segbValue,sega2Result);
								RoundResult = arrondir(result);
								alpha2.placeholder = RoundResult;
								//Ajout d'une zone
								sega2.style.display = 'inline-block';
								alpha2.style.display = 'inline-block';
								gamma2.style.display = 'inline-block';
							}
							else if (Number(segcValue) <= Number(segbValue) || Number(segbValue) == Number(pythagoreHypParSin(betaValue,segcValue))) {
								EmptyPlace("all");
								// caclule de gamma
								result = LdSinusCote(betaValue,segbValue,segcValue);
								gammaResult = result;
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								checkResult(result,gammaValue,gamma);
								// Calcule de alpha
								Euclide(betaValue,gammaResult);
								alphaResult = result;
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								checkResult(result,alphaValue,alpha);
								// Calcule de sega
								result = LdSinusAngle(betaValue,segbValue,alphaResult);
								segaResult = result;
								RoundResult = arrondir(result);
								sega.placeholder = RoundResult;
								checkResult(result,segaValue,sega);
							}
							else {
								segb.className = 'incorrect';
								tooltipStyle.display = 'inline-block';
							}
						}
					}
					// gamma existe
					else if (!isNaN(gammaValue) && gammaValue != "") {
						// && sega existe
						if (!isNaN(segaValue) && segaValue != "") {
							gamma.className = 'correct';
							sega.className = 'correct';
							// Calcule de segc
							result = LdCosinus(segaValue,segbValue,gammaValue);
							segcResult = result;
							RoundResult = arrondir(result);
							segc.placeholder = RoundResult;
							checkResult(result,segcValue,segc);
							// caclule de alpha
							result = LdCosinusCote(segbValue,segcResult,segaValue);
							alphaResult = result;
							RoundResult = arrondir(result);
							alpha.placeholder = RoundResult;
							checkResult(result,alphaValue,alpha);
							// caclule de beta
							result = Euclide(alphaResult,gammaValue)
							betaResult = result;
							RoundResult = arrondir(result);
							beta.placeholder = RoundResult;
							checkResult(result,betaValue,beta);
						}
						// && segc existe
						if (!isNaN(segcValue) && segcValue != "") {
							if (Number(segbValue) > Number(segcValue) && Number(segcValue) > Number(pythagoreHypParSin(gammaValue,segbValue))) {
								//Calcule de discriminant
								discrim = Math.pow((-2*segbValue*Math.cos(Math.radians(gammaValue))),2)-4*(Math.pow(segbValue,2)-Math.pow(segcValue,2))
								//Calcule des deux valeurs de sega possible et inscription
								result = -((-2*segbValue*Math.cos(Math.radians(gammaValue)))+Math.sqrt(discrim))/2;
								segaResult = result;
								RoundResult = arrondir(result);
								sega.placeholder = RoundResult;
								result = -((-2*segbValue*Math.cos(Math.radians(gammaValue)))-Math.sqrt(discrim))/2;
								sega2Result = result;
								RoundResult = arrondir(result);
								sega2.placeholder = RoundResult;
								// Calcule deux valeurs de beta possibles
								result = LdCosinusCote(segaResult,segcValue,segbValue);
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								result = LdCosinusCote(sega2Result,segcValue,segbValue);
								RoundResult = arrondir(result);
								beta2.placeholder = RoundResult;
								// Calcule deux valeurs de alpha possibles
								result = LdCosinusCote(segbValue,segcValue,segaResult);
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								result = LdCosinusCote(segbValue,segcValue,sega2Result);
								RoundResult = arrondir(result);
								alpha2.placeholder = RoundResult;
								//Ajout d'une zone
								sega2.style.display = 'inline-block';
								alpha2.style.display = 'inline-block';
								beta2.style.display = 'inline-block';
							}
							else if (Number(segbValue) <= Number(segcValue) || Number(segcValue) == Number(pythagoreHypParSin(gammaValue,segbValue))) {
								EmptyPlace("all");
								// caclule de beta
								result = LdSinusCote(gammaValue,segcValue,segbValue);
								betaResult = result;
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								checkResult(result,betaValue,beta);
								// Calcule de alpha
								Euclide(gammaValue,betaResult);
								alphaResult = result;
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								checkResult(result,alphaValue,alpha);
								// Calcule de sega
								result = LdSinusAngle(gammaValue,segcValue,alphaResult);
								segaResult = result;
								RoundResult = arrondir(result);
								sega.placeholder = RoundResult;
								checkResult(result,segaValue,sega);
							}
							else {
								segc.className = 'incorrect';
								tooltipStyle.display = 'inline-block';
							}
						}
					}
					// sega existe
					else if (!isNaN(segaValue) && segaValue != "") {
						// && segc existe
						if (!isNaN(segcValue) && segcValue != "") {
							if(Number(segbValue) < Number(segaValue) + Number(segcValue) && Number(segbValue) > Math.abs(Number(segaValue) - Number(segcValue))) {
								sega.className = 'correct';
								sega.nextSibling.nextSibling.nextSibling.nextSibling.style.display='none';
								segb.className = 'correct';
								segc.className = 'correct';
								segc.nextSibling.nextSibling.nextSibling.nextSibling.style.display='none';
								// Calcule de alpha
								result = LdCosinusCote(segbValue,segcValue,segaValue);
								alphaResult = result;
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								checkResult(result,alphaValue,alpha);
								// Calcule de beta
								result = LdCosinusCote(segaValue,segcValue,segbValue);
								betaResult = result;
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								checkResult(result,betaValue,beta);
								// Calcule de gamma
								result = LdCosinusCote(segaValue,segbValue,segcValue);
								gammaResult = result;
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								checkResult(result,gammaValue,gamma);
							}
							else {
								segb.className = 'incorrect';
								tooltipStyle.display = 'inline-block';
								sega.className = 'incorrect';
								segc.className = 'incorrect';
								EmptyPlace("all");
							}
						}
					}

				}
				else if (segbValue == String("")) {
					segb.className = '';
					tooltipStyle.display = 'none';
					EmptyPlace("b");
					inputs = document.getElementsByTagName('input'),
					inputsLength = inputs.length;
					for(var i = 0; i < inputsLength; i++) {
						if (inputs[i].value != String("")) {
							id = inputs[i].id;
							check[id]();
							break;
						}
					}
				}
				else {
					segb.className = 'incorrect';
					tooltipStyle.display = 'inline-block';
				}
			};
		
			check['segc'] = function() {

				var sega = document.getElementById('sega'),
					sega2 = document.getElementById('sega2'),
					segb = document.getElementById('segb'),
					segb2 = document.getElementById('segb2'),
					segc = document.getElementById('segc'),
					segc2 = document.getElementById('segc2'),
					alpha = document.getElementById('alpha'),
					alpha2 = document.getElementById('alpha2'),
					beta = document.getElementById('beta'),
					beta2 = document.getElementById('beta2'),
					gamma = document.getElementById('gamma'),
					gamma2 = document.getElementById('gamma2'),
					tooltipStyle = getTooltip(segc).style,
					segaValue = sega.value,
					segbValue = segb.value,
					segcValue = segc.value,
					alphaValue = alpha.value,
					betaValue = beta.value,
					gammaValue = gamma.value;
				
				// segc existe
				if (!isNaN(segcValue) && Number(segcValue) >= 0 && segcValue != String("")) {
					segc.className = 'correct';
					tooltipStyle.display = 'none';

					// alpha existe
					if (!isNaN(alphaValue) && alphaValue != "") {
						// && beta existe
						if (!isNaN(betaValue) && betaValue != "") {
							// Calcule de sega
							result = LdSinusAngle(gammaResult,segcValue,alphaValue)
							segaResult = result;
							RoundResult = arrondir(result);
							sega.placeholder = RoundResult;
							checkResult(result,segaValue,sega);
							// Calcule de segb
							result = LdSinusAngle(gammaResult,segcValue,betaValue)
							segbResult = result;
							RoundResult = arrondir(result);
							segb.placeholder = RoundResult;
							checkResult(result,segbValue,segb);
						}
						// && gamma existe
						else if (!isNaN(gammaValue) && gammaValue != "") {
							// Calcule de sega
							result = LdSinusAngle(gammaValue,segcValue,alphaValue);
							segaResult = result;
							RoundResult = arrondir(result);
							sega.placeholder = RoundResult;
							checkResult(result,segaValue,sega);
							// Calcule de segb
							result = LdSinusAngle(gammaValue,segcValue,betaResult);
							segbResult = result;
							RoundResult = arrondir(result);
							segb.placeholder = RoundResult;
							checkResult(result,segbValue,segb);
						}
						// && sega existe
						else if (!isNaN(segaValue) && segaValue != "") {
							if (Number(segcValue) > Number(segaValue) && Number(segaValue) > Number(pythagoreHypParSin(alphaValue,segcValue))) {
								//Calcule de discriminant
								discrim = Math.pow((-2*segcValue*Math.cos(Math.radians(alphaValue))),2)-4*(Math.pow(segcValue,2)-Math.pow(segaValue,2))
								//Calcule des deux valeurs de segb possible et inscription
								result = -((-2*segcValue*Math.cos(Math.radians(alphaValue)))+Math.sqrt(discrim))/2;
								segbResult = result;
								RoundResult = arrondir(result);
								segb.placeholder = RoundResult;
								result = -((-2*segcValue*Math.cos(Math.radians(alphaValue)))-Math.sqrt(discrim))/2;
								segb2Result = result;
								RoundResult = arrondir(result);
								segb2.placeholder = RoundResult;
								// Calcule deux valeurs de gamma possibles
								result = LdCosinusCote(segbResult,segaValue,segcValue);
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								result = LdCosinusCote(segb2Result,segaValue,segcValue);
								RoundResult = arrondir(result);
								gamma2.placeholder = RoundResult;
								// Calcule deux valeurs de beta possibles
								result = LdCosinusCote(segcValue,segaValue,segbResult);
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								result = LdCosinusCote(segcValue,segaValue,segb2Result);
								RoundResult = arrondir(result);
								beta2.placeholder = RoundResult;
								//Ajout d'une zone
								segb2.style.display = 'inline-block';
								beta2.style.display = 'inline-block';
								gamma2.style.display = 'inline-block';
							}
							else if (Number(segcValue) <= Number(segaValue) || Number(segaValue) == Number(pythagoreHypParSin(alphaValue,segcValue))) {
								EmptyPlace("all");
								// caclule de gamma
								result = LdSinusCote(alphaValue,segaValue,segcValue);
								gammaResult = result;
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								checkResult(result,gammaValue,gamma);
								// Calcule de beta
								Euclide(alphaValue,gammaResult);
								betaResult = result;
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								checkResult(result,betaValue,beta);
								// Calcule de segb
								result = LdSinusAngle(alphaValue,segaValue,betaResult);
								segbResult = result;
								RoundResult = arrondir(result);
								segb.placeholder = RoundResult;
								checkResult(result,segbValue,segb);
							}
							else {
								sega.className = 'incorrect';
								tooltipStyle.display = 'inline-block';
							}
						}
						// && segb existe
						else if (!isNaN(segbValue) && segbValue != "") {
							alpha.className = 'correct';
							segb.className = 'correct';
							// Calcule de sega
							result = LdCosinus(segcValue,segbValue,alphaValue);
							segaResult = result;
							RoundResult = arrondir(result);
							sega.placeholder = RoundResult;
							checkResult(result,segaValue,sega);
							// caclule de beta
							result = LdCosinusCote(segcValue,segaResult,segbValue);
							betaResult = result;
							RoundResult = arrondir(result);
							beta.placeholder = RoundResult;
							checkResult(result,betaValue,beta);
							// caclule de gamma
							result = Euclide(betaResult,alphaValue)
							gammaResult = result;
							RoundResult = arrondir(result);
							gamma.placeholder = RoundResult;
							checkResult(result,gammaValue,gamma);
						}
					}
					// beta existe
					else if (!isNaN(betaValue) && betaValue != "") {
						// && gamma existe
						if (!isNaN(gammaValue) && gammaValue != "") {
							// Calcule de segb
							result = LdSinusAngle(gammaValue,segcValue,betaValue);
							segbResult = result;
							RoundResult = arrondir(result);
							segb.placeholder = RoundResult;
							checkResult(result,segbValue,segb);
							// Calcule de sega
							result = LdSinusAngle(gammaValue,segcValue,alphaResult);
							segaResult = result;
							RoundResult = arrondir(result);
							sega.placeholder = RoundResult;
							checkResult(result,segaValue,sega);
						}
						// && sega existe
						else if (!isNaN(segaValue) && segaValue != "") {
							beta.className = 'correct';
							sega.className = 'correct';
							// Calcule de segb
							result = LdCosinus(segaValue,segcValue,betaValue);
							segbResult = result;
							RoundResult = arrondir(result);
							segb.placeholder = RoundResult;
							checkResult(result,segbValue,segb);
							// caclule de alpha
							result = LdCosinusCote(segcValue,segbResult,segaValue);
							alphaResult = result;
							RoundResult = arrondir(result);
							alpha.placeholder = RoundResult;
							checkResult(result,alphaValue,alpha);
							// caclule de gamma
							result = Euclide(alphaResult,betaValue)
							gammaResult = result;
							RoundResult = arrondir(result);
							gamma.placeholder = RoundResult;
							checkResult(result,gammaValue,gamma);
						}
						// && segb existe
						else if (!isNaN(segbValue) && segbValue != "") {
							if (Number(segcValue) > Number(segbValue) && Number(segbValue) > Number(pythagoreHypParSin(betaValue,segcValue))) {
								//Calcule de discriminant
								discrim = Math.pow((-2*segcValue*Math.cos(Math.radians(betaValue))),2)-4*(Math.pow(segcValue,2)-Math.pow(segbValue,2))
								//Calcule des deux valeurs de sega possible et inscription
								result = -((-2*segcValue*Math.cos(Math.radians(betaValue)))+Math.sqrt(discrim))/2;
								segaResult = result;
								RoundResult = arrondir(result);
								sega.placeholder = RoundResult;
								result = -((-2*segcValue*Math.cos(Math.radians(betaValue)))-Math.sqrt(discrim))/2;
								sega2Result = result;
								RoundResult = arrondir(result);
								sega2.placeholder = RoundResult;
								// Calcule deux valeurs de gamma possibles
								result = LdCosinusCote(segaResult,segbValue,segcValue);
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								result = LdCosinusCote(sega2Result,segbValue,segcValue);
								RoundResult = arrondir(result);
								gamma2.placeholder = RoundResult;
								// Calcule deux valeurs de alpha possibles
								result = LdCosinusCote(segcValue,segbValue,segaResult);
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								result = LdCosinusCote(segcValue,segbValue,sega2Result);
								RoundResult = arrondir(result);
								alpha2.placeholder = RoundResult;
								//Ajout d'une zone
								sega2.style.display = 'inline-block';
								alpha2.style.display = 'inline-block';
								gamma2.style.display = 'inline-block';
							}
							else if (Number(segcValue) <= Number(segbValue) || Number(segbValue) == Number(pythagoreHypParSin(betaValue,segcValue))) {
								EmptyPlace("all");
								// caclule de gamma
								result = LdSinusCote(betaValue,segbValue,segcValue);
								gammaResult = result;
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								checkResult(result,gammaValue,gamma);
								// Calcule de alpha
								Euclide(betaValue,gammaResult);
								alphaResult = result;
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								checkResult(result,alphaValue,alpha);
								// Calcule de sega
								result = LdSinusAngle(betaValue,segbValue,alphaResult);
								segaResult = result;
								RoundResult = arrondir(result);
								sega.placeholder = RoundResult;
								checkResult(result,segaValue,sega);
							}
							else {
								segb.className = 'incorrect';
								tooltipStyle.display = 'inline-block';
							}
						}
					}
					// gamma existe
					else if (!isNaN(gammaValue) && gammaValue != "") {
						// && sega existe
						if (!isNaN(segaValue) && segaValue != "") {
							if (Number(segaValue) > Number(segcValue) && Number(segcValue) > Number(pythagoreHypParSin(gammaValue,segaValue))) {
								// Calcule de discriminant
								discrim = Math.pow((-2*segaValue*Math.cos(Math.radians(gammaValue))),2)-4*(Math.pow(segaValue,2)-Math.pow(segcValue,2))
								// Calcule des deux valeurs de sega possible et inscription
								result = -((-2*segaValue*Math.cos(Math.radians(gammaValue)))+Math.sqrt(discrim))/2;
								segbResult = result;
								RoundResult = arrondir(result);
								segb.placeholder = RoundResult;
								result = -((-2*segaValue*Math.cos(Math.radians(gammaValue)))-Math.sqrt(discrim))/2;
								segb2Result = result;
								RoundResult = arrondir(result);
								segb2.placeholder = RoundResult;
								// Calcule deux valeurs de alpha possibles
								result = LdCosinusCote(segbResult,segcValue,segaValue);
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								result = LdCosinusCote(segb2Result,segcValue,segaValue);
								RoundResult = arrondir(result);
								alpha2.placeholder = RoundResult;
								// Calcule deux valeurs de beta possibles
								result = LdCosinusCote(segaValue,segcValue,segbResult);
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								result = LdCosinusCote(segaValue,segcValue,segb2Result);
								RoundResult = arrondir(result);
								beta2.placeholder = RoundResult;
								// Ajout d'une zone
								segb2.style.display = 'inline-block';
								alpha2.style.display = 'inline-block';
								beta2.style.display = 'inline-block';
							}
							else if (Number(segaValue) <= Number(segcValue) || Number(segcValue) == Number(pythagoreHypParSin(gammaValue,segaValue))) {
								EmptyPlace("all");
								// Caclule de alpha
								result = LdSinusCote(gammaValue,segcValue,segaValue);
								alphaResult = result;
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								checkResult(result,alphaValue,alpha);
								// Calcule de beta
								Euclide(gammaValue,alphaResult);
								betaResult = result;
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								checkResult(result,betaValue,beta);
								// Calcule de segb
								result = LdSinusAngle(gammaValue,segcValue,betaResult);
								segbResult = result;
								RoundResult = arrondir(result);
								segb.placeholder = RoundResult;
								checkResult(result,segbValue,segb);
							}
							else {
								segc.className = 'incorrect';
								tooltipStyle.display = 'inline-block';
							}
						}
						// && segb existe
						if (!isNaN(segbValue) && segbValue != "") {
							if (Number(segbValue) > Number(segcValue) && Number(segcValue) > Number(pythagoreHypParSin(gammaValue,segbValue))) {
								//Calcule de discriminant
								discrim = Math.pow((-2*segbValue*Math.cos(Math.radians(gammaValue))),2)-4*(Math.pow(segbValue,2)-Math.pow(segcValue,2))
								//Calcule des deux valeurs de sega possible et inscription
								result = -((-2*segbValue*Math.cos(Math.radians(gammaValue)))+Math.sqrt(discrim))/2;
								segaResult = result;
								RoundResult = arrondir(result);
								sega.placeholder = RoundResult;
								result = -((-2*segbValue*Math.cos(Math.radians(gammaValue)))-Math.sqrt(discrim))/2;
								sega2Result = result;
								RoundResult = arrondir(result);
								sega2.placeholder = RoundResult;
								// Calcule deux valeurs de beta possibles
								result = LdCosinusCote(segaResult,segcValue,segbValue);
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								result = LdCosinusCote(sega2Result,segcValue,segbValue);
								RoundResult = arrondir(result);
								beta2.placeholder = RoundResult;
								// Calcule deux valeurs de alpha possibles
								result = LdCosinusCote(segbValue,segcValue,segaResult);
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								result = LdCosinusCote(segbValue,segcValue,sega2Result);
								RoundResult = arrondir(result);
								alpha2.placeholder = RoundResult;
								//Ajout d'une zone
								sega2.style.display = 'inline-block';
								alpha2.style.display = 'inline-block';
								beta2.style.display = 'inline-block';
							}
							else if (Number(segbValue) <= Number(segcValue) || Number(segcValue) == Number(pythagoreHypParSin(gammaValue,segbValue))) {
								EmptyPlace("all");
								// caclule de beta
								result = LdSinusCote(gammaValue,segcValue,segbValue);
								betaResult = result;
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								checkResult(result,betaValue,beta);
								// Calcule de alpha
								Euclide(gammaValue,betaResult);
								alphaResult = result;
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								checkResult(result,alphaValue,alpha);
								// Calcule de sega
								result = LdSinusAngle(gammaValue,segcValue,alphaResult);
								segaResult = result;
								RoundResult = arrondir(result);
								sega.placeholder = RoundResult;
								checkResult(result,segaValue,sega);
							}
							else {
								segc.className = 'incorrect';
								tooltipStyle.display = 'inline-block';
							}
						}
					}
					// sega existe
					else if (!isNaN(segaValue) && segaValue != "") {
						// && segb existe
						if (!isNaN(segbValue) && segbValue != "") {
							if(Number(segcValue) < Number(segaValue) + Number(segbValue) && Number(segcValue) > Math.abs(Number(segaValue) - Number(segbValue))) {
								sega.className = 'correct';
								sega.nextSibling.nextSibling.nextSibling.nextSibling.style.display='none';
								segb.className = 'correct';
								segb.nextSibling.nextSibling.nextSibling.nextSibling.style.display='none';
								segc.className = 'correct';
								// Calcule de alpha
								result = LdCosinusCote(segbValue,segcValue,segaValue);
								alphaResult = result;
								RoundResult = arrondir(result);
								alpha.placeholder = RoundResult;
								checkResult(result,alphaValue,alpha);
								// Calcule de beta
								result = LdCosinusCote(segaValue,segcValue,segbValue);
								betaResult = result;
								RoundResult = arrondir(result);
								beta.placeholder = RoundResult;
								checkResult(result,betaValue,beta);
								// Calcule de gamma
								result = LdCosinusCote(segaValue,segbValue,segcValue);
								gammaResult = result;
								RoundResult = arrondir(result);
								gamma.placeholder = RoundResult;
								checkResult(result,gammaValue,gamma);
							}
							else {
								segc.className = 'incorrect';
								tooltipStyle.display = 'inline-block';
								sega.className = 'incorrect';
								segb.className = 'incorrect';
								EmptyPlace("all");
							}
						}
					}

				}
				else if (segcValue == String("")) {
					segc.className = '';
					tooltipStyle.display = 'none';
					EmptyPlace("c");
					inputs = document.getElementsByTagName('input'),
					inputsLength = inputs.length;
					for(var i = 0; i < inputsLength; i++) {
						if (inputs[i].value != String("")) {
							id = inputs[i].id;
							check[id]();
							break;
						}
					}
				}
				else {
					segc.className = 'incorrect';
					tooltipStyle.display = 'inline-block';
				}

			};

			// Mise en place des événements
			(function() {
			
				var Form = document.getElementById('Form'),
					inputs = document.getElementsByTagName('input'),
					inputsLength = inputs.length;
			
				for (var i = 0 ; i < inputsLength ; i++) {
					if (inputs[i].type == 'number') {
			
						inputs[i].addEventListener('keyup', function(e) {
							check[e.target.id](e.target.id);
						}, false);
			
					}
				}
			
				Form.addEventListener('reset', function() {
			
					for (var i = 0 ; i < inputsLength ; i++) {
						if (inputs[i].type == 'number') {
							inputs[i].className = '';
							inputs[i].placeholder = '';
					}
					EmptyPlace("all");
					}
			
					deactivateTooltips();
			
				}, false);
			})();

			// Désactiver tooltips
			deactivateTooltips();

    	</script>

		<div id="footer">
			<p>
				Faire un don <strong><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=593KJEYR3GQ2S" title="PayPal">Paypal</a></strong> - 
				<a href="https://framagit.org/Frederiiic/Pythagone">Code Source</a>	GPLv3 <img id="copyleft" src="img/Copyleft.svg" alt="Copyleft"> <a href="https://www.fredericpavageau.net">Frederic Pavageau</a> 2025.
			</p>
		</div>

	</body>
</html>